window.addAdviceTypeToModalFrom = function (adviceType) {

    let resultInput = document.getElementById('adviceModalType');
    resultInput.value = adviceType;
}

window.submitAdviceModal = function () {

    let csrf = document.getElementById('adviceCsrf').value;
    let submitAdviceModal = document.getElementById('submitAdviceModal');
    let showAdviceResult = document.getElementById('showAdviceResult');
    let AdviceResultError = document.getElementById('AdviceResultError');

    let adviceModalFullName = document.getElementById('adviceModalFullName')
    let adviceModalPhoneNumber = document.getElementById('adviceModalPhoneNumber')
    let adviceModalType = document.getElementById('adviceModalType')

    const data = {
        full_name: adviceModalFullName.value,
        phone_number: adviceModalPhoneNumber.value,
        advice_type: adviceModalType.value
    }

    submitAdviceModal.disabled = true;

    fetch('request-advice', {
        method: 'POST', headers: {
            'Content-Type': 'application/json',
            'X-CSRF-TOKEN': csrf
        }, body: JSON.stringify(data)
    })
        .then(response => {

            if (!response.ok) {
                throw new Error(`خطا: ${response.statusText}`);
            }
            return response.json(); // یا response.text() برای متن عادی
        })
        .then(data => {
            adviceModalFullName.value = "";
            adviceModalPhoneNumber.value = "";
            adviceModalType.value = "";

            showAdviceResult.textContent = 'عملیات موفقیت آمیز بود. مشاوران رایان ویزا در اولین فرصت با شما تماس برقرار می نمایند.';

        })
        .catch(error => {
            AdviceResultError.textContent = 'عملیات با خطا مواجه شد. لطفا مجددا تلاش نمایید.'
        });

}
