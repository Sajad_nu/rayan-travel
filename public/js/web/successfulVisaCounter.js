const items = document.querySelector('.items');
const counters = document.querySelectorAll('.number');
const speed = 500;

// Function to check if an element is in the viewport
const isInViewport = (element) => {
    const rect = element.getBoundingClientRect();
    return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
        rect.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
};

// The code to start the animation is now wrapped in a function
const startCounters = () => {
    counters.forEach((counter) => {
        const updateCount = () => {
            const target = parseInt(counter.getAttribute("data-bs-target"));
            const count = parseInt(counter.innerText);
            const increment = Math.ceil(target / speed);
            if (count < target) {
                counter.innerText = count + increment;
                setTimeout(updateCount, 1);
            } else {
                counter.innerText = target;
            }
        };
        updateCount();
    });
}

const scrollHandler = () => {
    if (isInViewport(items)) {
        startCounters();
    }
};

// On the first scroll in this window, call the function to start the counters
window.addEventListener('scroll', scrollHandler);

