window.submitContactUsForm = function () {

    let sendContactUsFormButton = document.getElementById('sendContactUsFormButton');
    let showResult = document.getElementById('showResult');
    let showError = document.getElementById('showError');
    let csrf = document.getElementById('csrf').value;

    const data = {
        title: document.getElementById('title').value,
        text: document.getElementById('text').value,
        full_name: document.getElementById('fullName').value,
        phone_number: document.getElementById('phoneNumber').value
    }

    sendContactUsFormButton.disabled = true;

    fetch('new-task', {
        method: 'POST', headers: {
            'Content-Type': 'application/json',
            'X-CSRF-TOKEN': csrf
        }, body: JSON.stringify(data)
    })
        .then(response => {

            if (!response.ok) {
                throw new Error(`خطا: ${response.statusText}`);
            }
            return response.json(); // یا response.text() برای متن عادی
        })
        .then(data => {
            showResult.textContent = 'عملیات موفقیت آمیز بود. مشاوران رایان ویزا در اولین فرصت با شما تماس برقرار می نمایند.'

        })
        .catch(error => {
            showError.textContent = 'عملیات با خطا مواجه شد. لطفا مجددا تلاش نمایید.'
            sendContactUsFormButton.disabled = false;
        });

}
