import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';
import vue from '@vitejs/plugin-vue';

export default defineConfig({
    resolve: {
        mainFields: [
            'browser',
            'module',
            'main',
            'jsnext:main',
            'jsnext'
        ]
    },
    plugins: [
        vue(),
        laravel([
            'resources/scss/main.scss',
            'resources/js/web/vue/app.js',
        ]),
    ],
});



