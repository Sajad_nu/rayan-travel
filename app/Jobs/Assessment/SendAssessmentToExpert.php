<?php

namespace App\Jobs\Assessment;

use App\Channels\NotificationChannels\NotificationManager;
use App\Models\Repositories\AnswersAssessment\AnswersAssessmentRepository;
use App\Models\Repositories\AssessmentQuestion\AssessmentQuestionRepository;
use App\Models\Repositories\UsersAssessment\UsersAssessmentRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendAssessmentToExpert implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected string $_table = 'users_assessment';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function handle(): void
    {
        $users = (new UsersAssessmentRepository())->getAllNotSendAssessmentToExpert();

        $questions = (new AssessmentQuestionRepository())->getAllByVisaId(1)->keyBy('id');

        foreach ($users as $user) {

            $answers = (new AnswersAssessmentRepository())
                ->getAllByUserAssessmentId($user->getId())->groupBy('questionId');

            $result = (new NotificationManager())->sendAssessmentToExpert($user, $questions, $answers);

            if ($result == 0) {
                $user->setSendAt(now());
                (new UsersAssessmentRepository())->update($user);
            }

        }
    }
}
