<?php

namespace App\Jobs\Assessment;

use App\Jobs\SendAssessmentResultToUser;
use App\Models\Entities\UsersAssessment;
use App\Models\Factories\UsersAssessmentFactory;
use App\Models\Repositories\AnswersAssessment\AnswersAssessmentRepository;
use App\Models\Repositories\AssessmentQuestion\AssessmentQuestionRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Query\Builder;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class SendResultToUser implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected string $_table = 'users_assessment';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function handle(): void
    {
        $users = DB::table($this->_table)
            ->whereNotNull('rank')
            ->where(function (Builder $query) {
                $query->whereNull('send_result_at')
                    ->orWhereNull('send_confirm_at');
            })
            ->where('created_at', '<', Carbon::now()->subMinutes(3)->format('Y-m-d H:i:s'))
            ->get();

        $users = (new UsersAssessmentFactory())->makeFromCollection($users);

        $usersNameInfo = (new AnswersAssessmentRepository())
            ->getUserNameByUserIds($users->pluck('id')->toArray())->keyBy('userAssessmentId');

        $questions = (new AssessmentQuestionRepository())->getAllByVisaId(1)->keyBy('id');

        foreach ($users as $user) {

            $answers = (new AnswersAssessmentRepository())
                ->getAllByUserAssessmentId($user->getId())->groupBy('questionId');

            /**@var UsersAssessment $user */
            $userNameInfo = $usersNameInfo[$user->getId()];
            $user->setUserName($userNameInfo->getDescription());

            dispatch_sync(new SendAssessmentResultToUser($user, $questions, $answers));
        }
    }
}
