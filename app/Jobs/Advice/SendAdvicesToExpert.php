<?php

namespace App\Jobs\Advice;

use App\Channels\NotificationChannels\NotificationManager;
use App\Models\Entities\RequestsAdvice;
use App\Models\Repositories\RequestsAdvice\RequestsAdviceRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendAdvicesToExpert implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected string $_table = 'users_assessment';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function handle(): void
    {
        $requests = (new RequestsAdviceRepository())->getAllNotSendToExpert();

        foreach ($requests as $request) {

            $result = (new NotificationManager())->sendAdviceToExperts($request);

            if ($result == 0) {
                /** @var  RequestsAdvice $request */
                $request->setSendToExpertAt(now());
                (new RequestsAdviceRepository())->update($request);
            }

        }
    }
}
