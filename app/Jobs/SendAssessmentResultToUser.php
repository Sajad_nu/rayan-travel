<?php

namespace App\Jobs;

use App\Channels\NotificationChannels\NotificationManager;
use App\Models\Entities\UsersAssessment;
use App\Models\Repositories\UsersAssessment\UsersAssessmentRepository;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;

class SendAssessmentResultToUser implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private UsersAssessment $userInfo;
    private Collection $questions;
    private Collection $answers;

    /**
     * SendCommentChanged constructor.
     * @param UsersAssessment $user
     * @param Collection $questions
     * @param Collection $answers
     */
    public function __construct(UsersAssessment $user, Collection $questions, Collection $answers)
    {
        $this->userInfo = $user;
        $this->answers = $answers;
        $this->questions = $questions;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws Exception
     */
    public function handle(): void
    {
        $notificationManager = new NotificationManager();

        if ($this->userInfo->getSendResultAt() == null) {
            $sendAssessment = $notificationManager->sendAssessmentToUser($this->userInfo, $this->questions, $this->answers);

            if ($sendAssessment == 0) {
                $sendResult = $notificationManager->sendAssessmentResultToUser($this->userInfo);
                $this->userInfo->setSendResultAt(now());
            }
        }

        if ($this->userInfo->getSendResultAt() != null && $this->userInfo->getSendConfirmAt() == null) {

            $message = __('messages.we-sender-assessment.' . 0, [
                'userName' => $this->userInfo->getUserName(),
                'phoneNumber' => 'https://wa.me/' . $this->userInfo->getCountryCode() . (int)$this->userInfo->getPhoneNumber(),
            ]);

            $confirmResult = $notificationManager->confirmSendAssessmentResultUserToExpert($message);
            $this->userInfo->setSendConfirmAt(now());
        }

        if ((isset($sendResult) && $sendResult == 0) || (isset($confirmResult) && $confirmResult == 0)) {
            (new UsersAssessmentRepository())->update($this->userInfo);
        }
    }

}
