<?php

namespace App\Jobs;

use App\Channels\NotificationChannels\NotificationManager;
use App\Models\Entities\Task;
use App\Models\Repositories\Task\TaskRepository;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendTaskToExperts implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Execute the job.
     *
     * @return void
     * @throws Exception
     */
    public function handle(): void
    {
        $taskRepository = new TaskRepository();
        $tasks = $taskRepository->getAllNotSendToExpert();

        foreach ($tasks as $task) {

            $result = (new NotificationManager())->sendTaskToExperts($task);

            if ($result == 0) {
                /** @var  Task $tasks */
                $task->setSendToExpertAt(now());
                $taskRepository->update($task);
            }

        }
    }

}
