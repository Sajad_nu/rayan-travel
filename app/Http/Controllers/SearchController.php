<?php

namespace App\Http\Controllers;

use App\Models\Repositories\Visa\VisaRepository;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class SearchController extends Controller
{

    public function index(Request $request)
    {
        $request->validate([
            'categoryId' => ['required', Rule::exists('visa', 'id')],
            'locationId' => ['required', Rule::exists('locations', 'id')]
        ]);

        $visa = (new VisaRepository())->getOneCategoryAndLocationById($request->get('categoryId'), $request->get('locationId'));
        if ($visa === null) {
            return redirect(url());
        }

        return redirect(url($visa->getUri()));
    }

}
