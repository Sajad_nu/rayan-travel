<?php

namespace App\Http\Controllers;

use App\Models\Repositories\MetaData\MetaDataRepository;
use Illuminate\Http\Request;

class AboutUsController extends Controller
{

    public function index(Request $request)
    {
        $metaData = (new MetaDataRepository())->getOneByUri($request->getPathInfo());

        return view('web.about-us.index', [
            'metaData' => $metaData
        ]);
    }

}
