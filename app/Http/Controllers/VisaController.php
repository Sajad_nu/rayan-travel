<?php

namespace App\Http\Controllers;

use App\Models\Entities\VisaDocument;
use App\Models\Entities\VisaProcedure;
use App\Models\Repositories\Document\DocumentRepository;
use App\Models\Repositories\ExtraContent\ExtraContentRepository;
use App\Models\Repositories\File\FileRepository;
use App\Models\Repositories\MetaData\MetaDataRepository;
use App\Models\Repositories\Procedure\ProcedureRepository;
use App\Models\Repositories\Visa\VisaRepository;
use App\Models\Repositories\VisaDocument\VisaDocumentRepository;
use App\Models\Repositories\VisaProcedure\VisaProcedureRepository;
use Illuminate\Http\Request;

class VisaController extends Controller
{

    public function index(Request $request)
    {
        $visa = (new VisaRepository())->getOneByUri($request->getRequestUri());
        if ($visa === null) {
            return redirect(url());
        }

        $visaDocuments = (new VisaDocumentRepository())->getAllByVisaId($visa->getId());
        $documents = (new DocumentRepository())->getAllByIds($visaDocuments->pluck('documentId')->toArray())->keyBy('id');
        $countryFile = (new FileRepository())->getOneByLocationId($visa->getLocationId());
        $visa->setFileUri($countryFile ? $countryFile->getPath() . $countryFile->getName() : null);

        foreach ($visaDocuments as $visaDocument) {
            /**@var VisaDocument $visaDocument */
            $documentsInfo = $documents[$visaDocument->getDocumentId()];
            $visaDocument->setTitle($documentsInfo->getTitle());
        }

        $visaProcedures = (new VisaProcedureRepository())->getAllByVisaId($visa->getId());
        $procedures = (new ProcedureRepository)->getAllByIds($visaProcedures->pluck('proceduresId')->toArray())->keyBy('id');
        $extraContents = (new ExtraContentRepository())->getAllByUri($visa->getUri());
        $tags = (new VisaRepository())->getAllByCategoryOrLocationId($visa->getCategoryId(), $visa->getLocationId());

        foreach ($visaProcedures as $visaProcedure) {
            /**@var VisaProcedure $visaProcedure */
            $proceduresInfo = $procedures[$visaProcedure->getProceduresId()];
            $visaProcedure->setTitle($proceduresInfo->getTitle());
        }

        $metaData = (new MetaDataRepository())->getOneByUri($visa->getUri());

        return view('web.visa.index', [
            'visa' => $visa,
            'documents' => $visaDocuments,
            'procedures' => $visaProcedures,
            'tags' => $tags,
            'extraContents' => $extraContents,
            'metaData' => $metaData
        ]);
    }

}
