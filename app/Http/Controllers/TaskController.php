<?php

namespace App\Http\Controllers;

use App\Models\Entities\Task;
use App\Models\Repositories\Task\TaskRepository;
use Illuminate\Http\Request;

class TaskController extends Controller
{

    public function store(Request $request)
    {
        $request->validate([
            'title' => ['required', 'string'],
            'full_name' => ['required', 'string'],
            'phone_number' => ['required', 'string'],
            'text' => 'required|string'
        ]);

        $task = new Task();
        $task->setTitle(htmlspecialchars($request->get('title')));
        $task->setFullName(htmlspecialchars($request->get('full_name')));
        $task->setPhoneNumber(htmlspecialchars($request->get('phone_number')));
        $task->setTitle(LatinNumber(htmlspecialchars($request->get('title'))));
        $task->setDescription(htmlspecialchars($request->get('text')));

        (new TaskRepository())->create($task);

        $this->response->setMessage('success');
        return $this->response->json();
    }

}
