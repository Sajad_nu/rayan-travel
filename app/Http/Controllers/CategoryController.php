<?php

namespace App\Http\Controllers;

use App\Models\Entities\File;
use App\Models\Entities\Visa;
use App\Models\Repositories\Category\CategoryRepository;
use App\Models\Repositories\ExtraContent\ExtraContentRepository;
use App\Models\Repositories\File\FileRepository;
use App\Models\Repositories\MetaData\MetaDataRepository;
use App\Models\Repositories\Visa\VisaRepository;

class CategoryController extends Controller
{

    public function index($category)
    {
        $category = (new CategoryRepository())->getOneActiveByType($category);
        if ($category == null) {
            return redirect(url());
        }

        $visas = (new VisaRepository())->getAllByCategoryId($category->getId());

        $visaFiles = (new FileRepository())->getAllByVisaIds($visas->pluck('id')->toArray())->keyBy('visaId');

        $extraContents = (new ExtraContentRepository())->getAllByUri($category->getUri());


        foreach ($visas as $visa) {
            /**@var File $fileInfo */
            $fileInfo = $visaFiles[$visa->getId()] ?? null;

            /**@var Visa $visa */
            $fileUri = $fileInfo ? $fileInfo->getPath() . $fileInfo->getName() : null;
            $visa->setFileUri($fileUri);
        }

        $metaData = (new MetaDataRepository())->getOneByUri($category->getUri());

        return view('web.category.index', [
            'category' => $category,
            'visas' => $visas,
            'extraContents' => $extraContents,
            'metaData' => $metaData,
        ]);
    }

}
