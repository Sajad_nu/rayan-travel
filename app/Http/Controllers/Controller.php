<?php

namespace App\Http\Controllers;

use App\Http\Response\Response;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    protected Response $response;


    public function __construct()
    {
        $this->response = new Response();
    }
}
