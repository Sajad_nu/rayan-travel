<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Entities\UsersAssessment;
use App\Models\Enums\BooleanTypesEnum;
use App\Models\Enums\InputTypesEnum;
use App\Models\Repositories\AnswersAssessment\AnswersAssessmentRepository;
use App\Models\Repositories\AssessmentQuestion\AssessmentQuestionRepository;
use App\Models\Repositories\UsersAssessment\UsersAssessmentRepository;
use Hashids\Hashids;
use Illuminate\Http\Request;

class AnswersAssessmentController extends Controller
{

    public function store(Request $request)
    {
        $data = $request->all();
        $hashId = (new Hashids(__METHOD__, 8))->encode(time());
        $questions = (new AssessmentQuestionRepository())->getAllByVisaId(1)->keyBy('id');


        $user = new UsersAssessment();
        $user->setHashId($hashId);
        $user->setTrackingCode((string)'1000' . time());
        $user = (new UsersAssessmentRepository())->create($user);


        ksort($data, CRYPT_MD5);
        foreach ($data as $key => $value) {

            $paths = explode('-', $key);
            $questionInfo = $questions[$paths[0]];

            if (count($paths) == 1) {
                $paths[] = $value;
                $value = null;
            }

            if ($questionInfo->getInputType() == InputTypesEnum::CHECKBOX->value) {

                if ($value == BooleanTypesEnum::FALSE->value) {
                    continue;
                }
                $value = null;
            }

            $answers[] = [
                'user_assessment_id' => $user->getId(),
                'question_id' => $paths[0],
                'param' => $paths[1] ?? null,
                'item' => $paths[2] ?? null,
                'description' => $value,
            ];
        }

        (new AnswersAssessmentRepository())->insert($answers ?? []);

        $this->response->value->add('hash_id', $hashId);
        return $this->response->json();
    }

}
