<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Repositories\AssessmentQuestion\AssessmentQuestionRepository;
use App\Models\Resources\Web\AssessmentQuestionResource;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class AssessmentQuestionController extends Controller
{
    public function listByVisaId(Request $request)
    {
        $request->validate([
            'visa_id' => 'required|' . Rule::exists('assessment_questions', 'visa_id')
        ]);

        $questions = (new AssessmentQuestionRepository())->getAllByVisaId($request->get('visa_id'));

        $this->response->value->add('questions',
            (new AssessmentQuestionResource())->collectionToArray($questions));

        return $this->response->json();

    }

}
