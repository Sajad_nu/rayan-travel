<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Jobs\SendAssessmentResultToUser;
use App\Jobs\SendAssessmentToExpert;
use App\Models\Repositories\AnswersAssessment\AnswersAssessmentRepository;
use App\Models\Repositories\AssessmentQuestion\AssessmentQuestionRepository;
use App\Models\Repositories\UsersAssessment\UsersAssessmentRepository;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UserAssessmentController extends Controller
{

    public function userCallInfo(Request $request)
    {
        $request->validate([
            'hash_id' => 'required|' . Rule::exists('users_assessment', 'hash_id'),
            'send_by' => 'required',
            'country_code' => 'required',
            'phone_number' => 'required|min:11'
        ]);

        $usersAssessmentRepository = new UsersAssessmentRepository();
        $user = $usersAssessmentRepository->getOneByHashId($request->get('hash_id'));

        if ($user->getPhoneNumber() == null) {

            $user->setCountryCode(LatinNumber($request->get('country_code')));
            $user->setPhoneNumber((int)LatinNumber($request->get('phone_number')));
            $user->setSendResultTo($request->get('send_by'));
            $usersAssessmentRepository->update($user);
        }

        $this->response->setMessage('success');
        return $this->response->json();
    }

    public function setRank(Request $request)
    {
        $request->validate([
            'hash_id' => 'required|' . Rule::exists('users_assessment', 'hash_id'),
        ]);


        $user = (new UsersAssessmentRepository())->getOneByHashId($request->get('hash_id'));

        $questions = (new AssessmentQuestionRepository())->getAllByVisaId(1)->keyBy('id');

        $answers = (new AnswersAssessmentRepository())->getAllByUserAssessmentId($user->getId())->groupBy('questionId');

        $bodyParameters = [];

        $data = $answers->toArray();
        ksort($data, CRYPT_MD5);

        foreach ($data as $key => $answersParams) {

            $questionInfo = $questions[$key];
            $bodyParameters[$key]['title'] = $questionInfo->getTitle();

            $counter = 0;
            foreach ($answersParams as $index => $answer) {

                $param = $answer->getParam();
                $paramsTitle = $questionInfo->getParams()->$param->value;

                if ($questionInfo->getTitle() == null) {

                    unset($bodyParameters[$key]);
                    $bodyParameters[$key . '-' . $counter]['title'] = $paramsTitle;
                    $bodyParameters[$key . '-' . $counter]['values'][] = $answer->getDescription();

                    $counter++;
                } else {
                    $bodyParameters[$key]['values'][$param] = $paramsTitle;
                }

                $itemValue = $answer->getItem();

                if ($itemValue == null) {
                    continue;
                }

                $bodyParameters[$key]['items'][$index]['itemKey'] = $questionInfo->getParams()->$param->items->$itemValue->placeholder;
                $bodyParameters[$key]['items'][$index]['itemValue'] = $answer->getDescription();
            }
        }

        return view('assessment.send-to-admin', [
                'paramsValues' => $bodyParameters,
                'hashId' => $request->get('hash_id')
            ]
        );
    }

    public function setUserFeedback(Request $request)
    {
        $request->validate([
            'hash_id' => 'required|' . Rule::exists('users_assessment', 'hash_id'),
            'rank' => 'required'
        ]);

        $usersAssessmentRepository = (new UsersAssessmentRepository());
        $userInfo = $usersAssessmentRepository->getOneByHashId($request->get('hash_id'));


        if ($userInfo->getRank() == null) {

            $questions = (new AssessmentQuestionRepository())->getAllByVisaId(1)->keyBy('id');

            $answers = (new AnswersAssessmentRepository())
                ->getAllByUserAssessmentId($userInfo->getId())->groupBy('questionId');

            $userInfo->setRank($request->get('rank'));
            $userInfo->setRankAt(now());
            $usersAssessmentRepository->update($userInfo);

            $userNameInfo = (new AnswersAssessmentRepository())->getUserNameByUserId($userInfo->getId());
            $userInfo->setUserName($userNameInfo->getDescription());

            dispatch_sync(new SendAssessmentResultToUser($userInfo, $questions, $answers));
        }
        return redirect()->to('https://chat.whatsapp.com/IDiOvuoJ1JO9hynBCRn5le');
    }


}
