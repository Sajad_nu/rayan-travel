<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Repositories\AssessmentCategory\AssessmentCategoryRepository;
use App\Models\Resources\Web\AssessmentCategoryResource;

class AssessmentCategoryController extends Controller
{
    public function getList()
    {
        $categories = (new AssessmentCategoryRepository())->getAll();

        $this->response->value->add('categories',
            (new AssessmentCategoryResource())->collectionToArray($categories));

        return $this->response->json();
    }
}
