<?php

namespace App\Http\Controllers;

use App\Models\Entities\Category;
use App\Models\Entities\File;
use App\Models\Repositories\Category\CategoryRepository;
use App\Models\Repositories\ExtraContent\ExtraContentRepository;
use App\Models\Repositories\File\FileRepository;
use App\Models\Repositories\Location\LocationRepository;
use App\Models\Repositories\MetaData\MetaDataRepository;

class HomeController extends Controller
{

    public function index()
    {
        $categories = (new CategoryRepository())->getAllActives();
        $categoryFiles = (new FileRepository())
            ->getAllByCategoryIds($categories->pluck('id')->toArray())->keyBy('categoryId');
        $locations = (new LocationRepository())->getAll();

        foreach ($categories as $category) {
            /**@var File $fileInfo */
            $fileInfo = $categoryFiles[$category->getId()] ?? null;

            /**@var Category $category */
            $fileUri = $fileInfo ? $fileInfo->getPath() . $fileInfo->getName() : null;
            $category->setFileUri($fileUri);
        }

        $extraContents = (new ExtraContentRepository())->getAllByUri('/');

        $metaData = (new MetaDataRepository())->getOneByUri('/');

        return view('web.home.index', [
            'categories' => $categories,
            'locations' => $locations,
            'extraContents' => $extraContents,
            'metaData' => $metaData
        ]);
    }

}
