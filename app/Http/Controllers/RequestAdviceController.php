<?php

namespace App\Http\Controllers;

use App\Models\Entities\RequestsAdvice;
use App\Models\Enums\RequestsAdviceTypeEnum;
use App\Models\Repositories\RequestsAdvice\RequestsAdviceRepository;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class RequestAdviceController extends Controller
{

    public function store(Request $request)
    {
        $request->validate([
            'full_name' => ['required', 'string'],
            'advice_type' => ['required', Rule::in(RequestsAdviceTypeEnum::getList())],
            'phone_number' => 'required|min:8'
        ]);

        $requestsAdvice = new RequestsAdvice();
        $requestsAdvice->setTrackingCode((string)'1000' . time());
        $requestsAdvice->setFullName(htmlspecialchars($request->get('full_name')));
        $requestsAdvice->setPhoneNumber(LatinNumber(htmlspecialchars($request->get('phone_number'))));
        $requestsAdvice->setType($request->get('advice_type'));

        (new RequestsAdviceRepository())->create($requestsAdvice);

        $this->response->setMessage('success');
        return $this->response->json();
    }

}
