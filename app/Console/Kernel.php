<?php

namespace App\Console;

use App\Jobs\Advice\SendAdvicesToExpert;
use App\Jobs\Assessment\SendAssessmentToExpert;
use App\Jobs\Assessment\SendResultToUser;
use App\Jobs\SendTaskToExperts;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     */
    protected function schedule(Schedule $schedule): void
    {
        ini_set('memory_limit', '512M');

        try {
            dispatch_sync(new SendTaskToExperts());
        } catch (\Throwable $e) {
            logger('*Error SendTaskToExperts *' . date('H:i') . ' *' . PHP_EOL . $e->getMessage());
        }

        try {
            dispatch_sync(new SendAdvicesToExpert());
        } catch (\Throwable $e) {
            logger('*Error SendAdvicesToExpert *' . date('H:i') . ' *' . PHP_EOL . $e->getMessage());
        }

        try {
            dispatch_sync(new SendAssessmentToExpert());
        } catch (\Throwable $e) {
            logger('*Error SendAssessmentToExpert *' . date('H:i') . ' *' . PHP_EOL . $e->getMessage());
        }

        try {
            dispatch_sync(new SendResultToUser());
        } catch (\Throwable $e) {
            logger('*Error SendResultToUser *' . date('H:i') . ' *' . PHP_EOL . $e->getMessage());
        }
    }

    /**
     * Register the commands for the application.
     */
    protected function commands(): void
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
