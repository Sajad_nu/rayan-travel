<?php

namespace App\Channels\NotificationChannels;

use App\Channels\NotificationChannels\Whatsapp\Whatsapp;
use App\Models\Entities\RequestsAdvice;
use App\Models\Entities\Task;
use App\Models\Entities\UsersAssessment;
use Illuminate\Contracts\Container\Container;
use Illuminate\Support\Collection;
use Illuminate\Support\Manager;

class NotificationManager extends Manager implements INotification
{

    public function __construct(Container $container = null)
    {
        parent::__construct($container == null ? app() : $container);
    }

    /**
     * @return Whatsapp
     */
    public function createWhatsappDriver(): Whatsapp
    {
        return new Whatsapp();
    }

    public function sendAssessmentToExpert(UsersAssessment $user, Collection $questions, Collection $answers)
    {
        return $this->driver()->sendAssessmentToExpert($user, $questions, $answers);
    }

    public function confirmSendAssessmentResultUserToExpert(string $message)
    {
        return $this->driver()->confirmSendAssessmentResultUserToExpert($message);
    }

    public function sendAssessmentResultToUser(UsersAssessment $user)
    {
        return $this->driver()->sendAssessmentResultToUser($user);
    }

    public function sendAssessmentToUser(UsersAssessment $user, Collection $questions, Collection $answers)
    {
        return $this->driver()->sendAssessmentToUser($user, $questions, $answers);
    }

    public function sendAdviceToExperts(RequestsAdvice $request)
    {
        return $this->driver()->sendAdviceToExperts($request);
    }

    public function sendTaskToExperts(Task $task)
    {
        return $this->driver()->sendTaskToExperts($task);
    }

    public function getDefaultDriver(): string
    {
        return 'whatsapp';
    }
}
