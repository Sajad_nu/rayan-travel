<?php

namespace App\Channels\NotificationChannels\Whatsapp;

use App\Models\Entities\RequestsAdvice;
use App\Models\Entities\Task;
use App\Models\Entities\UsersAssessment;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Collection;

class Api
{
    private string $apiUrl;
    private string $token;

    public function __construct($apiUrl, $token)
    {
        $this->apiUrl = $apiUrl;
        $this->token = $token;
    }

    /**
     * @param UsersAssessment $user
     * @param Collection $questions
     * @param Collection $answers
     * @return mixed
     * @throws GuzzleException
     * @throws \HttpResponseException
     */
    public function sendAssessmentToExpert(UsersAssessment $user, Collection $questions, Collection $answers): mixed
    {
        $bodyParameters = self::createBodyParams($questions, $answers);

        $bodyParameters[] = "\n\n\n\n\n\n\nثبت نظر";
        $bodyParameters[] = url('assessment/set-rank?hash_id=' . $user->getHashId()) . "\n";


        $headers = [
            'sender' => '14372339769',
            'key' => $this->token,
            'groupId' => 'IDiOvuoJ1JO9hynBCRn5le'
        ];

        $body = json_encode(["message" => implode("\n", $bodyParameters)]);
        return $this->_call($headers, $body);
    }

    /**
     * @param string $message
     * @return mixed
     * @throws GuzzleException
     * @throws \HttpResponseException
     */
    public function confirmSendAssessmentResultUserToExpert(string $message): mixed
    {
        $headers = [
            'sender' => '14372339769',
            'key' => $this->token,
            'groupId' => 'IDiOvuoJ1JO9hynBCRn5le'
        ];

        $body = json_encode(["message" => $message]);
        return $this->_call($headers, $body);

    }

    public function sendAdviceToExperts(RequestsAdvice $request)
    {
        $message = ' آقا/ خانم ' . $request->getFullName() . PHP_EOL;
        $message .= ' با شماره همراه ' . $request->getPhoneNumber() . PHP_EOL;
        $message .= 'درخواست جدیدی جهت تعیین مشاوره به صورت ' . __('attributes.advice-type.' . $request->getType()) . PHP_EOL;
        $message .= 'از طریق سایت ثبت نموده است.';

        $headers = [
            'sender' => '14372339769',
            'key' => $this->token,
            'groupId' => 'E8tIW84mDMP0t1QlEX7xdA'
        ];

        $body = json_encode(["message" => $message]);
        return $this->_call($headers, $body);

    }

    /**
     * @throws \HttpResponseException
     * @throws GuzzleException
     */
    public function sendTaskToExperts(Task $task)
    {
        $message = 'عنوان : ' . $task->getTitle() . PHP_EOL;
        $message .= ' آقا/ خانم ' . $task->getFullName() . PHP_EOL;
        $message .= ' با شماره همراه ' . $task->getPhoneNumber() . PHP_EOL;
        $message .= 'متن پیام : ' . $task->getDescription() . PHP_EOL . PHP_EOL;
        $message .= '#تماسـ‌باـ‌ما';

        $headers = [
            'sender' => '14372339769',
            'key' => $this->token,
            'groupId' => 'E8tIW84mDMP0t1QlEX7xdA'
        ];

        $body = json_encode(["message" => $message]);
        return $this->_call($headers, $body);

    }

    /**
     * @param UsersAssessment $user
     * @return mixed
     * @throws GuzzleException
     * @throws \HttpResponseException
     */
    public function sendAssessmentResultToUser(UsersAssessment $user): mixed
    {
        $headers = [
            'sender' => '14372339769',
            'key' => $this->token,
            'reciverCountryCode' => $user->getCountryCode(),
            'reciverNumber' => (int)$user->getPhoneNumber()
        ];

        $body = json_encode([
            "message" => "ارزیابی ارسال شده بالا صرفا با محوریت پاسخ شما تنظیم گردیده است. خواهشمند است برای نهایی شدن نتیجه در همین صفحه پیام دهید",
            "imgBase64" => "data:image/jpg;base64," . base64_encode(file_get_contents('https://ryan-visa.com/images/assessmentTemplates/' . $user->getRank() . '.jpg'))
        ], JSON_UNESCAPED_UNICODE);

        return $this->_call($headers, $body);
    }

    /**
     * @param UsersAssessment $user
     * @param Collection $questions
     * @param Collection $answers
     * @return mixed
     * @throws GuzzleException
     * @throws \HttpResponseException
     */
    public function sendAssessmentToUser(UsersAssessment $user, Collection $questions, Collection $answers): mixed
    {
        $headers = [
            'sender' => '14372339769',
            'key' => $this->token,
            'reciverCountryCode' => $user->getCountryCode(),
            'reciverNumber' => (int)$user->getPhoneNumber()
        ];

        $bodyParameters = self::createBodyParams($questions, $answers);

        $body = json_encode(["message" => "این فرم به صورت سیستمی و جهت تسهیل در روند بررسی درخواست شما توسط کارشناس، ارسال شده است. \n\n" . implode("\n", $bodyParameters)]);
        return $this->_call($headers, $body);
    }

    /**
     * @param array $headers
     * @param string $body
     * @return mixed
     * @throws GuzzleException
     * @throws \HttpResponseException
     */
    private function _call(array $headers, string $body): mixed
    {
        try {

            $client = new Client();
            $request = new Request('post', $this->apiUrl, $headers, $body);
            $response = $client->send($request, ['timeout' => 120]);

        } catch (RequestException $e) {

            $response = $e->getResponse();
            if ($response) {
                $result = json_decode((string)$response->getBody());
                throw new \HttpResponseException(response()->json($result->message ?? '', $result->code ?? ''));
            }
        }

        $response = json_decode($response->getBody());

        if ($response != 0) {
            logger($response);
        }
        return $response;
    }

    /**
     * @param Collection $answers
     * @param Collection $questions
     * @return array
     */
    public function createBodyParams(Collection $questions, Collection $answers): array
    {
        $bodyParameters = [];

        $data = $answers->toArray();
        ksort($data, CRYPT_MD5);

        foreach ($data as $key => $answersParams) {

            $questionInfo = $questions[$key];

            foreach ($answersParams as $index => $answer) {

                $param = $answer->getParam();
                $paramsTitle = $questionInfo->getParams()->$param->value;

                if (!isset($bodyParameters[$key . '-' . $answer->getParam()])) {
                    if ($questionInfo->getTitle() == null) {

                        $bodyParameters[$key . '-' . $answer->getParam()] = "\n" . $paramsTitle . ' : ' . $answer->getDescription();

                    } else {
                        $bodyParameters[$key . '-' . $answer->getParam()] = "\n" . $questionInfo->getTitle() . ' : ' . $paramsTitle;
                    }
                }

                $itemValue = $answer->getItem();

                if ($itemValue == null) {
                    continue;
                }

                $bodyParameters[$key . '-' . $index] = $questionInfo->getParams()->$param->items->$itemValue->placeholder . ' : ' . $answer->getDescription();
            }
        }

        return $bodyParameters;
    }

}
