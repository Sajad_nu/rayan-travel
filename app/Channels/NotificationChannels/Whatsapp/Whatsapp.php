<?php

namespace App\Channels\NotificationChannels\Whatsapp;

use App\Channels\NotificationChannels\INotification;
use App\Models\Entities\RequestsAdvice;
use App\Models\Entities\Task;
use App\Models\Entities\UsersAssessment;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Collection;

class Whatsapp implements INotification
{

    /**@var Api $api */
    private Api $api;

    public function __construct()
    {
        $url = config('services.whatsapp.weSender.base_url');
        $token = config('services.whatsapp.weSender.client_token');
        $this->api = new Api($url, $token);
    }

    /**
     * @param UsersAssessment $user
     * @param Collection $questions
     * @param Collection $answers
     * @return mixed
     * @throws GuzzleException
     * @throws \HttpResponseException
     */
    public function sendAssessmentToExpert(UsersAssessment $user, Collection $questions, Collection $answers): mixed
    {
        return $this->api->sendAssessmentToExpert($user, $questions, $answers);
    }

    /**
     * @param UsersAssessment $user
     * @return mixed
     * @throws GuzzleException
     * @throws \HttpResponseException
     */
    public function sendAssessmentResultToUser(UsersAssessment $user): mixed
    {
        return $this->api->sendAssessmentResultToUser($user);
    }

    /**
     * @param UsersAssessment $user
     * @param Collection $questions
     * @param Collection $answers
     * @return mixed
     * @throws GuzzleException
     * @throws \HttpResponseException
     */
    public function sendAssessmentToUser(UsersAssessment $user, Collection $questions, Collection $answers): mixed
    {
        return $this->api->sendAssessmentToUser($user, $questions, $answers);
    }

    /**
     * @param string $message
     * @return mixed
     * @throws GuzzleException
     * @throws \HttpResponseException
     */
    public function confirmSendAssessmentResultUserToExpert(string $message): mixed
    {
        return $this->api->confirmSendAssessmentResultUserToExpert($message);
    }


    public function sendAdviceToExperts(RequestsAdvice $request)
    {
        return $this->api->sendAdviceToExperts($request);
    }


    public function sendTaskToExperts(Task $task)
    {
        return $this->api->sendTaskToExperts($task);
    }

}
