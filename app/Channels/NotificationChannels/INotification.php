<?php

namespace App\Channels\NotificationChannels;

use App\Models\Entities\RequestsAdvice;
use App\Models\Entities\Task;
use App\Models\Entities\UsersAssessment;
use Illuminate\Support\Collection;

interface INotification
{
    public function sendAssessmentToExpert(UsersAssessment $user, Collection $questions, Collection $answers);

    public function sendAssessmentToUser(UsersAssessment $user, Collection $questions, Collection $answers);

    public function sendTaskToExperts(Task $task);

    public function sendAdviceToExperts(RequestsAdvice $request);

}
