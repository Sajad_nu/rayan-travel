<?php

namespace App\Models\Resources;

class MetaDataResource extends Resource
{
    public function toArray($metaData): array
    {
        return [
            'id' => $metaData->getId(),
            'uri' => $metaData->getUri(),
            'title' => $metaData->getTitle(),
            'description' => $metaData->getDescription(),
            'locale' => $metaData->getLocale(),
            'canonical_url' => $metaData->getCanonicalUrl(),
            'index' => $metaData->getIndex(),
            'robots' => $metaData->getRobots(),
            'type' => $metaData->getType(),
            'author' => $metaData->getAuthor(),
            'keywords' => $metaData->getKeywords(),
            'image' => $metaData->getImage(),
            'disabled' => $metaData->getDisabled(),
            'published_time' => $metaData->getPublishedTime(),
            'modified_time' => $metaData->getModifiedTime(),
            'created_at' => $metaData->getCreatedAt(),
            'updated_at' => $metaData->getUpdatedAt(),
            'deleted_at' => $metaData->getDeletedAt(),

        ];
    }

}
