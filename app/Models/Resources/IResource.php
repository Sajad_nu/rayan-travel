<?php

namespace App\Models\Resources;

use App\Models\Entities\Entity;

interface IResource
{
    /**
     * @param Entity $entity
     * @return mixed
     */
    public function toArray($entity);
}
