<?php

namespace App\Models\Resources;


class FileResource extends Resource
{
    public function toArray($file): array
    {
        return [
            'id' => $file->getId(),
            'category_id' => $file->getCategoryId(),
            'visa_id' => $file->getVisaId(),
            'name' => $file->getName(),
            'description' => $file->getDescription(),
            'hash' => $file->getHash(),
            'type' => $file->getType(),
            'path' => $file->getPath(),
            'priority' => $file->getPriority(),
            'published' => $file->getPublished(),
            'created_at' => $file->getCreatedAt(),
            'updated_at' => $file->getUpdatedAt(),
            'deleted_at' => $file->getDeletedAt(),

        ];
    }
}
