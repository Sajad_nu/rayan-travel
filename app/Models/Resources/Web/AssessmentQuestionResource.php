<?php

namespace App\Models\Resources\Web;


use App\Models\Resources\Resource;

class AssessmentQuestionResource extends Resource
{
    public function toArray($assessmentQuestion): array
    {
        return [
            'id' => $assessmentQuestion->getId(),
            'category_id' => $assessmentQuestion->getCategoryId(),
            'parent_id' => $assessmentQuestion->getParentId(),
            'title' => $assessmentQuestion->getTitle(),
            'parent_params' => $assessmentQuestion->getParentParams(),
            'params' => $assessmentQuestion->getParams(),
            'priority' => $assessmentQuestion->getPriority(),

        ];
    }

    public function toArrayWithForeignKeys($assessmentQuestion): array
    {
        return $this->toArray($assessmentQuestion) + [

            ];
    }
}
