<?php

namespace App\Models\Resources\Web;

use App\Models\Resources\Resource;

class AnswersAssessmentResource extends Resource
{
    public function toArray($answersAssessment): array
    {
        return [
            'id' => $answersAssessment->getId(),
            'question_id' => $answersAssessment->getQuestionId(),
            'hash_id' => $answersAssessment->getHashId(),
            'param' => $answersAssessment->getParam(),
            'item' => $answersAssessment->getItem(),
            'description' => $answersAssessment->getDescription(),
            'phone_number' => $answersAssessment->getPhoneNumber(),
            'send_result_to' => $answersAssessment->getSendResultTo(),
            'created_at' => $answersAssessment->getCreatedAt(),
            'updated_at' => $answersAssessment->getUpdatedAt(),

        ];
    }

    public function toArrayWithForeignKeys($answersAssessment): array
    {
        return $this->toArray($answersAssessment) + [

            ];
    }
}
