<?php

namespace App\Models\Resources\Web;

use App\Models\Resources\Resource;

class UsersAssessmentResource extends Resource
{
    public function toArray($usersAssessment): array
    {
        return [
            'id' => $usersAssessment->getId(),
            'question_id' => $usersAssessment->getQuestionId(),
            'params' => $usersAssessment->getParams(),
            'items' => $usersAssessment->getItems(),
            'phone_number' => $usersAssessment->getPhoneNumber(),
            'send_result_to' => $usersAssessment->getSendResultTo(),
            'created_at' => $usersAssessment->getCreatedAt(),
            'updated_at' => $usersAssessment->getUpdatedAt(),

        ];
    }

    public function toArrayWithForeignKeys($usersAssessment): array
    {
        return $this->toArray($usersAssessment) + [

            ];
    }
}
