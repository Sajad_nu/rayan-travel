<?php

namespace App\Models\Resources\Web;

use App\Models\Resources\Resource;

class AssessmentCategoryResource extends Resource
{
    public function toArray($entity): array
    {
        return [
            'id' => $entity->getId(),
            'title' => $entity->getTitle(),
            'priority' => $entity->getPriority(),

        ];
    }

    public function toArrayWithForeignKeys($assessmentCategory): array
    {
        return $this->toArray($assessmentCategory) + [

            ];
    }
}
