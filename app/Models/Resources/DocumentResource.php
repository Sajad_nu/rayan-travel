<?php

namespace App\Models\Resources;


class DocumentResource extends Resource
{
    public function toArray($document): array
    {
        return [
            'id' => $document->getId(),
            'title' => $document->getTitle(),
            'title_en' => $document->getTitleEn(),
            'disabled' => $document->getDisabled(),
            'created_at' => $document->getCreatedAt(),
            'updated_at' => $document->getUpdatedAt(),

        ];
    }
}
