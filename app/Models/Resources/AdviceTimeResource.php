<?php

namespace App\Models\Resources;

class AdviceTimeResource extends Resource
{
    public function toArray($adviceTime): array
    {
        return [
            'id' => $adviceTime->getId(),
            'datetime' => $adviceTime->getDatetime(),
            'is_reserved' => $adviceTime->getIsReserved(),
            'disabled' => $adviceTime->getDisabled(),
            'created_at' => $adviceTime->getCreatedAt(),
            'updated_at' => $adviceTime->getUpdatedAt(),

        ];
    }
}
