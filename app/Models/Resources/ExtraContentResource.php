<?php

namespace App\Models\Resources;

class ExtraContentResource extends Resource
{

    public function toArray($extraContent): array
    {
        return [
            'id' => $extraContent->getId(),
            'uri' => $extraContent->getUri(),
            'type' => $extraContent->getType(),
            'title' => $extraContent->getTitle(),
            'description' => $extraContent->getDescription(),
            'priority' => $extraContent->getPriority(),
            'published' => $extraContent->getPublished(),
            'created_at' => $extraContent->getCreatedAt(),
            'updated_at' => $extraContent->getUpdatedAt(),

        ];
    }

}
