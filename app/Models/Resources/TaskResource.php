<?php

namespace App\Models\Resources;

class TaskResource extends Resource
{
    public function toArray($task): array
    {
        return [
            'id' => $task->getId(),
            'full_name' => $task->getFullName(),
            'phone_number' => $task->getPhoneNumber(),
            'title' => $task->getTitle(),
            'description' => $task->getDescription(),
            'result' => $task->getResult(),
            'assigned_at' => $task->getAssignedAt(),
            'done_at' => $task->getDoneAt(),
            'expired_at' => $task->getExpiredAt(),
            'created_at' => $task->getCreatedAt(),
            'updated_at' => $task->getUpdatedAt(),

        ];
    }
}
