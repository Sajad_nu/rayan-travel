<?php

namespace App\Models\Resources;

class RequestsAdviceResource extends Resource
{
    public function toArray($requestsAdvice): array
    {
        return [
            'id' => $requestsAdvice->getId(),
            'advice_time_id' => $requestsAdvice->getAdviceTimeId(),
            'phone_number' => $requestsAdvice->getPhoneNumber(),
            'full_name' => $requestsAdvice->getFullName(),
            'type' => $requestsAdvice->getType(),
            'tracking_code' => $requestsAdvice->getTrackingCode(),
            'send_to_expert_at' => $requestsAdvice->getSendToExpertAt(),
            'created_at' => $requestsAdvice->getCreatedAt(),
            'updated_at' => $requestsAdvice->getUpdatedAt(),

        ];
    }
}
