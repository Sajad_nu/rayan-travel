<?php

namespace App\Models\Resources;

use App\Models\Entities\Entity;
use Illuminate\Support\Collection;

class Resource
{
    /**
     * @param Collection | Entity[] $entities
     * @return array
     */
    public function collectionToArray(array|Collection $entities): array
    {
        $entityArray = [];

        foreach ($entities as $client) {
            $entityArray[] = $this->toArray($client);
        }

        return $entityArray;
    }
}
