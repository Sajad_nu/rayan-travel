<?php

namespace App\Models\Resources;

class ProcedureResource extends Resource
{
    public function toArray($procedure): array
    {
        return [
            'id' => $procedure->getId(),
            'title' => $procedure->getTitle(),
            'title_en' => $procedure->getTitleEn(),
            'disabled' => $procedure->getDisabled(),
            'priority' => $procedure->getPriority(),
            'created_at' => $procedure->getCreatedAt(),
            'updated_at' => $procedure->getUpdatedAt(),

        ];
    }
}
