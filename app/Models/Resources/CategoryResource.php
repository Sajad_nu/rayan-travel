<?php

namespace App\Models\Resources;

class CategoryResource extends Resource
{
    public function toArray($category): array
    {
        return [
            'id' => $category->getId(),
            'title' => $category->getTitle(),
            'type' => $category->getType(),
            'uri' => $category->getUri(),
            'description' => $category->getDescription(),
            'disabled' => $category->getDisabled(),
            'created_at' => $category->getCreatedAt(),
            'updated_at' => $category->getUpdatedAt(),
            'deleted_at' => $category->getDeletedAt(),

        ];
    }
}
