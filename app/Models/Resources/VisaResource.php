<?php

namespace App\Models\Resources;

class VisaResource extends Resource
{
    public function toArray($visa): array
    {
        return [
            'id' => $visa->getId(),
            'location_id' => $visa->getLocationId(),
            'category_id' => $visa->getCategoryId(),
            'title' => $visa->getTitle(),
            'uri' => $visa->getUri(),
            'priority' => $visa->getPriority(),
            'description' => $visa->getDescription(),
            'documents' => $visa->getDocuments(),
            'fee' => $visa->getFee(),
            'procedures' => $visa->getProcedures(),
            'created_at' => $visa->getCreatedAt(),
            'updated_at' => $visa->getUpdatedAt(),

        ];
    }
}
