<?php

namespace App\Models\Resources;

class VisaDocumentResource extends Resource
{
    public function toArray($visaDocument): array
    {
        return [
            'id' => $visaDocument->getId(),
            'visa_id' => $visaDocument->getVisaId(),
            'document_id' => $visaDocument->getDocumentId(),
            'description' => $visaDocument->getDescription(),
            'created_at' => $visaDocument->getCreatedAt(),
            'updated_at' => $visaDocument->getUpdatedAt(),
            'deleted_at' => $visaDocument->getDeletedAt(),

        ];
    }
}
