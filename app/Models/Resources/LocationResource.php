<?php

namespace App\Models\Resources;

class LocationResource extends Resource
{
    public function toArray($location): array
    {
        return [
            'id' => $location->getId(),
            'name' => $location->getName(),
            'name_en' => $location->getNameEn(),
            'uri' => $location->getUri(),
            'created_at' => $location->getCreatedAt(),
            'updated_at' => $location->getUpdatedAt(),
            'deleted_at' => $location->getDeletedAt(),

        ];
    }
}
