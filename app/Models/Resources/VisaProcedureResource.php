<?php

namespace App\Models\Resources;

class VisaProcedureResource extends Resource
{
    public function toArray($visaProcedure): array
    {
        return [
            'id' => $visaProcedure->getId(),
            'visa_id' => $visaProcedure->getVisaId(),
            'procedures_id' => $visaProcedure->getProceduresId(),
            'description' => $visaProcedure->getDescription(),
            'created_at' => $visaProcedure->getCreatedAt(),
            'updated_at' => $visaProcedure->getUpdatedAt(),
            'deleted_at' => $visaProcedure->getDeletedAt(),

        ];
    }
}
