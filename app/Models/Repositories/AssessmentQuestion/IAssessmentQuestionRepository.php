<?php

namespace App\Models\Repositories\AssessmentQuestion;

use App\Models\Entities\AssessmentQuestion;
use Illuminate\Support\Collection;

interface IAssessmentQuestionRepository
{
    public function getOneById(int $id): null|AssessmentQuestion;

    public function getAllByVisaId(int $visaId): Collection;

    public function getAllByIds(array $ids): Collection;

    public function create(AssessmentQuestion $assessmentQuestion): AssessmentQuestion;

    public function update(AssessmentQuestion $assessmentQuestion): int;
}
