<?php

namespace App\Models\Repositories\AssessmentQuestion;

use App\Models\Entities\AssessmentQuestion;
use Illuminate\Support\Collection;

class AssessmentQuestionRepository implements IAssessmentQuestionRepository
{
    private IAssessmentQuestionRepository $repository;
	private RedisAssessmentQuestionRepository $redisRepository;

	public function __construct()
    {
        $this->repository = new MySqlAssessmentQuestionRepository();
		$this->redisRepository = new RedisAssessmentQuestionRepository();
    }

    public function getOneById(int $id): null|AssessmentQuestion
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => 'getOneById',
            'id' => $id,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getOneById($id);
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $this->repository->getOneById($id);
    }

    public function getAllByIds(array $ids): Collection
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => 'getAllByIds',
            'id' => $ids,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getAllByIds($ids);
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $this->repository->getAllByIds($ids);
    }

    public function getAllByVisaId(int $visaId): Collection
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => 'getAllByVisaId',
            'id' => $visaId,
        ]);

        $entities = $this->redisRepository->get($cacheKey);

        if ($entities == null) {
            $entities = $this->repository->getAllByVisaId($visaId);
            $this->redisRepository->put($cacheKey, $entities);
        }

        return $entities;
    }

    public function create(AssessmentQuestion $assessmentQuestion): AssessmentQuestion
    {
        $this->redisRepository->clear();

        return $this->repository->create($assessmentQuestion);
    }

    public function update(AssessmentQuestion $assessmentQuestion): int
    {
        $this->redisRepository->clear();

        return $this->repository->update($assessmentQuestion);
    }
}
