<?php

namespace App\Models\Repositories\AssessmentQuestion;

use App\Models\Repositories\RedisRepository;
use App\Models\Repositories\Redis\QueryCacheStrategy;

class RedisAssessmentQuestionRepository extends RedisRepository
{
     use QueryCacheStrategy;

    public function __construct()
    {
        $this->cacheTag = 'assessment_questions';
        parent::__construct();
    }
}
