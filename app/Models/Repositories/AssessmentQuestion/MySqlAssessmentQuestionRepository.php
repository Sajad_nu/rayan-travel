<?php

namespace App\Models\Repositories\AssessmentQuestion;

use App\Models\Entities\AssessmentQuestion;
use App\Models\Factories\AssessmentQuestionFactory;
use App\Models\Repositories\MySqlRepository;
use Illuminate\Support\Collection;

class MySqlAssessmentQuestionRepository extends MySqlRepository implements IAssessmentQuestionRepository
{
    public function __construct()
    {
        $this->table = 'assessment_questions';
        $this->primaryKey = 'id';
        $this->softDelete = false;
    }

    public function getOneById(int $id): null|AssessmentQuestion
    {
        $assessmentQuestion = $this->newQuery()
            ->where('id', $id)
            ->first();

        return $assessmentQuestion ? (new AssessmentQuestionFactory())->make($assessmentQuestion) : null;
    }

    public function getAllByIds(array $ids): Collection
    {
        $assessmentQuestion = $this->newQuery()
            ->whereIn('id', $ids)
            ->get();

        return (new AssessmentQuestionFactory())->makeFromCollection($assessmentQuestion);
    }

    public function getAllByVisaId(int $visaId): Collection
    {
        $assessmentQuestion = $this->newQuery()
            ->where('visa_id', $visaId)
            ->get();

        return (new AssessmentQuestionFactory())->makeFromCollection($assessmentQuestion);
    }

    public function create(AssessmentQuestion $assessmentQuestion): AssessmentQuestion
    {
        $assessmentQuestion->setCreatedAt(date('Y-m-d H:i:s'));
        $assessmentQuestion->setUpdatedAt(date('Y-m-d H:i:s'));

        $id = $this->newQuery()
            ->insertGetId([
                'visa_id' => $assessmentQuestion->getVisaId(),
                'category_id' => $assessmentQuestion->getCategoryId(),
                'parent_id' => $assessmentQuestion->getParentId(),
                'title' => $assessmentQuestion->getTitle(),
                'parent_params' => $assessmentQuestion->getParentParams(),
                'params' => $assessmentQuestion->getParams(),
                'priority' => $assessmentQuestion->getPriority(),
                'created_at' => $assessmentQuestion->getCreatedAt(),
                'updated_at' => $assessmentQuestion->getUpdatedAt(),
            ]);

        $assessmentQuestion->setId($id);

        return $assessmentQuestion;
    }

    public function update(AssessmentQuestion $assessmentQuestion): int
    {
        $assessmentQuestion->setUpdatedAt(date('Y-m-d H:i:s'));

        return $this->newQuery()
            ->where($this->primaryKey, $assessmentQuestion->getPrimaryKey())
            ->update([
                'visa_id' => $assessmentQuestion->getVisaId(),
                'category_id' => $assessmentQuestion->getCategoryId(),
                'title' => $assessmentQuestion->getTitle(),
                'parent_id' => $assessmentQuestion->getParentId(),
                'parent_params' => $assessmentQuestion->getParentParams(),
                'params' => $assessmentQuestion->getParams(),
                'priority' => $assessmentQuestion->getPriority(),
                'updated_at' => $assessmentQuestion->getUpdatedAt(),
            ]);
    }
}
