<?php

namespace App\Models\Repositories\Visa;

use App\Models\Repositories\Redis\QueryCacheStrategy;
use App\Models\Repositories\RedisRepository;

class RedisVisaRepository extends RedisRepository
{
     use QueryCacheStrategy;

    public function __construct()
    {
        $this->cacheTag = 'visa';
        parent::__construct();
    }
}
