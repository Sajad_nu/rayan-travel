<?php

namespace App\Models\Repositories\Visa;

use App\Models\Entities\Visa;
use Illuminate\Support\Collection;

interface IVisaRepository
{
    public function getOneById(int $id): null|Visa;

    public function getOneCategoryAndLocationById(int $categoryId,int $locationId): null|Visa;

    public function getOneByUri(string $uri): null|Visa;

    public function getAllByIds(array $ids): Collection;

    public function getAllByCategoryId(int $categoryId): Collection;

    public function getAllByCategoryOrLocationId(int $categoryId, int $locationId): Collection;

    public function create(Visa $visa): Visa;

    public function update(Visa $visa): int;
}
