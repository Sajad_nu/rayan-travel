<?php

namespace App\Models\Repositories\Visa;

use App\Models\Entities\Visa;
use Illuminate\Support\Collection;

class VisaRepository implements IVisaRepository
{
    private IVisaRepository $repository;
    private RedisVisaRepository $redisRepository;

    public function __construct()
    {
        $this->repository = new MySqlVisaRepository();
        $this->redisRepository = new RedisVisaRepository();
    }

    public function getOneById(int $id): null|Visa
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'id' => $id,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getOneById($id);
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $entity;
    }

    public function getOneCategoryAndLocationById(int $categoryId, int $locationId): null|Visa
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'category_id' => $categoryId,
            'location_id' => $locationId,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getOneCategoryAndLocationById($categoryId, $locationId);
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $entity;
    }

    public function getOneByUri(string $uri): null|Visa
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'uri' => $uri,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getOneByUri($uri);
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $entity;
    }

    public function getAllByIds(array $ids): Collection
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'id' => $ids,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getAllByIds($ids);
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $entity;
    }

    public function getAllByCategoryId(int $categoryId): Collection
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'category_id' => $categoryId,
        ]);

        $entities = $this->redisRepository->get($cacheKey);

        if (is_null($entities)) {
            $entities = $this->repository->getAllByCategoryId($categoryId);
            $this->redisRepository->put($cacheKey, $entities);
        }

        return $entities;
    }

    public function getAllByCategoryOrLocationId(int $categoryId, int $locationId): Collection
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'category_id' => $categoryId,
            'location_id' => $locationId,
        ]);

        $entities = $this->redisRepository->get($cacheKey);

        if (is_null($entities)) {
            $entities = $this->repository->getAllByCategoryOrLocationId($categoryId, $locationId);
            $this->redisRepository->put($cacheKey, $entities);
        }

        return $entities;
    }

    public function create(Visa $visa): Visa
    {
        $this->redisRepository->clear();

        return $this->repository->create($visa);
    }

    public function update(Visa $visa): int
    {
        $this->redisRepository->clear();

        return $this->repository->update($visa);
    }
}
