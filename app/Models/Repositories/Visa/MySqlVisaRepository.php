<?php

namespace App\Models\Repositories\Visa;

use App\Models\Entities\Visa;
use App\Models\Factories\VisaFactory;
use App\Models\Repositories\MySqlRepository;
use Illuminate\Support\Collection;

class MySqlVisaRepository extends MySqlRepository implements IVisaRepository
{
    public function __construct()
    {
        $this->table = 'visa';
        $this->primaryKey = 'id';
        $this->softDelete = false;
    }

    public function getOneById(int $id): null|Visa
    {
        $visa = $this->newQuery()
            ->where('id', $id)
            ->first();

        return $visa ? (new VisaFactory())->make($visa) : null;
    }

    public function getOneCategoryAndLocationById(int $categoryId,int $locationId): null|Visa
    {
        $visa = $this->newQuery()
            ->where('category_id', $categoryId)
            ->where('location_id', $locationId)
            ->first();

        return $visa ? (new VisaFactory())->make($visa) : null;
    }

    public function getOneByUri(string $uri): null|Visa
    {
        $visa = $this->newQuery()
            ->where('uri', $uri)
            ->first();

        return $visa ? (new VisaFactory())->make($visa) : null;
    }

    public function getAllByIds(array $ids): Collection
    {
        $visa = $this->newQuery()
            ->whereIn('id', $ids)
            ->get();

        return (new VisaFactory())->makeFromCollection($visa);
    }

    public function getAllByCategoryId(int $categoryId): Collection
    {
        $visa = $this->newQuery()
            ->where('category_id', $categoryId)
            ->get();

        return (new VisaFactory())->makeFromCollection($visa);
    }

    public function getAllByCategoryOrLocationId(int $categoryId, int $locationId): Collection
    {
        $visa = $this->newQuery()
            ->where('category_id', $categoryId)
            ->orWhere('location_id', $locationId)
            ->get();

        return (new VisaFactory())->makeFromCollection($visa);
    }

    public function create(Visa $visa): Visa
    {
        $visa->setCreatedAt(date('Y-m-d H:i:s'));
        $visa->setUpdatedAt(date('Y-m-d H:i:s'));

        $id = $this->newQuery()
            ->insertGetId([
                'location_id' => $visa->getLocationId(),
                'category_id' => $visa->getCategoryId(),
                'title' => $visa->getTitle(),
                'uri' => $visa->getUri(),
                'priority' => $visa->getPriority(),
                'description' => $visa->getDescription(),
                'documents' => $visa->getDocuments(),
                'fee' => $visa->getFee(),
                'procedures' => $visa->getProcedures(),
                'created_at' => $visa->getCreatedAt(),
                'updated_at' => $visa->getUpdatedAt(),
            ]);

        $visa->setId($id);

        return $visa;
    }

    public function update(Visa $visa): int
    {
        $visa->setUpdatedAt(date('Y-m-d H:i:s'));

        return $this->newQuery()
            ->where($this->primaryKey, $visa->getPrimaryKey())
            ->update([
                'location_id' => $visa->getLocationId(),
                'category_id' => $visa->getCategoryId(),
                'title' => $visa->getTitle(),
                'uri' => $visa->getUri(),
                'priority' => $visa->getPriority(),
                'description' => $visa->getDescription(),
                'documents' => $visa->getDocuments(),
                'fee' => $visa->getFee(),
                'procedures' => $visa->getProcedures(),
                'updated_at' => $visa->getUpdatedAt(),
            ]);
    }
}
