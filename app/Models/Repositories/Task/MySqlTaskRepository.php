<?php

namespace App\Models\Repositories\Task;

use App\Models\Entities\Task;
use App\Models\Factories\TaskFactory;
use App\Models\Repositories\MySqlRepository;
use Illuminate\Support\Collection;

class MySqlTaskRepository extends MySqlRepository implements ITaskRepository
{
    private TaskFactory $factory;

    public function __construct()
    {
        $this->table = 'tasks';
        $this->primaryKey = 'id';
        $this->softDelete = false;
        $this->factory = new TaskFactory();
    }

    public function getOneById(int $id): null|Task
    {
        $task = $this->newQuery()
            ->where('id', $id)
            ->first();

        return $task ? $this->factory->make($task) : null;
    }

    public function getAllByIds(array $ids): Collection
    {
        $task = $this->newQuery()
            ->whereIn('id', $ids)
            ->get();

        return $this->factory->makeFromCollection($task);
    }

    public function getAllNotSendToExpert(): Collection
    {
        $usersAssessment = $this->newQuery()
            ->whereNotNull('phone_number')
            ->whereNull('send_to_expert_at')
            ->get();

        return $this->factory->makeFromCollection($usersAssessment);
    }

    public function create(Task $task): Task
    {
        $task->setCreatedAt(date('Y-m-d H:i:s'));
        $task->setUpdatedAt(date('Y-m-d H:i:s'));

        $id = $this->newQuery()
            ->insertGetId([
                'full_name' => $task->getFullName(),
                'phone_number' => $task->getPhoneNumber(),
                'title' => $task->getTitle(),
                'description' => $task->getDescription(),
                'result' => $task->getResult(),
                'assigned_at' => $task->getAssignedAt(),
                'done_at' => $task->getDoneAt(),
                'send_to_expert_at' => $task->getSendToExpertAt(),
                'created_at' => $task->getCreatedAt(),
                'updated_at' => $task->getUpdatedAt(),
            ]);

        $task->setId($id);

        return $task;
    }

    public function update(Task $task): int
    {
        $task->setUpdatedAt(date('Y-m-d H:i:s'));

        return $this->newQuery()
            ->where($this->primaryKey, $task->getPrimaryKey())
            ->update([
                'full_name' => $task->getFullName(),
                'phone_number' => $task->getPhoneNumber(),
                'title' => $task->getTitle(),
                'description' => $task->getDescription(),
                'result' => $task->getResult(),
                'assigned_at' => $task->getAssignedAt(),
                'done_at' => $task->getDoneAt(),
                'send_to_expert_at' => $task->getSendToExpertAt(),
                'updated_at' => $task->getUpdatedAt(),
            ]);
    }
}
