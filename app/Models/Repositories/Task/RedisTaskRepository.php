<?php

namespace App\Models\Repositories\Task;


use App\Models\Repositories\Redis\QueryCacheStrategy;
use App\Models\Repositories\RedisRepository;

class RedisTaskRepository extends RedisRepository
{
    use QueryCacheStrategy;

    public function __construct()
    {
        $this->cacheTag = 'tasks';
        parent::__construct();
    }
}
