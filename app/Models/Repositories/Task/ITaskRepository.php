<?php

namespace App\Models\Repositories\Task;

use App\Models\Entities\Task;
use Illuminate\Support\Collection;

interface ITaskRepository
{
    public function getOneById(int $id): null|Task;

    public function getAllByIds(array $ids): Collection;

    public function getAllNotSendToExpert(): Collection;

    public function create(Task $task): Task;

    public function update(Task $task): int;

}
