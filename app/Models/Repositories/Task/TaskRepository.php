<?php

namespace App\Models\Repositories\Task;

use App\Models\Entities\Task;
use Illuminate\Support\Collection;

class TaskRepository implements ITaskRepository
{
    private ITaskRepository $repository;
    private RedisTaskRepository $redisRepository;

    public function __construct()
    {
        $this->repository = new MySqlTaskRepository();
        $this->redisRepository = new RedisTaskRepository();
    }

    public function getOneById(int $id): null|Task
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'id' => $id,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getOneById($id);
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $entity;
    }

    public function getAllByIds(array $ids): Collection
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'id' => $ids,
        ]);

        $entities = $this->redisRepository->get($cacheKey);

        if (is_null($entities)) {
            $entities = $this->repository->getAllByIds($ids);
            $this->redisRepository->put($cacheKey, $entities);
        }

        return $entities;
    }

    public function getAllNotSendToExpert(): Collection
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getAllNotSendToExpert();
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $entity;
    }

    public function create(Task $task): Task
    {
        $this->redisRepository->clear();

        return $this->repository->create($task);
    }

    public function update(Task $task): int
    {
        $this->redisRepository->clear();

        return $this->repository->update($task);
    }
}
