<?php

namespace App\Models\Repositories\ExtraContent;

use App\Models\Entities\ExtraContent;
use App\Models\Factories\ExtraContentFactory;
use App\Models\Repositories\MySqlRepository;
use Illuminate\Support\Collection;

class MySqlExtraContentRepository extends MySqlRepository implements IExtraContentRepository
{
    private ExtraContentFactory $factory;

    public function __construct()
    {
        $this->table = 'extra_contents';
        $this->primaryKey = 'id';
        $this->softDelete = false;
        $this->factory = new ExtraContentFactory();
    }

    public function getOneById(int $id): null|ExtraContent
    {
        $extraContent = $this->newQuery()
            ->where('id', $id)
            ->first();

        return $extraContent ? $this->factory->make($extraContent) : null;
    }

    public function getAllByIds(array $ids): Collection
    {
        $extraContent = $this->newQuery()
            ->whereIn('id', $ids)
            ->get();

        return $this->factory->makeFromCollection($extraContent);
    }

    public function getAllByUri(string $uri): Collection
    {
        $extraContent = $this->newQuery()
            ->where('uri', $uri)
            ->where('published', true)
            ->orderBy('priority')
            ->get();

        return $this->factory->makeFromCollection($extraContent);
    }

    public function create(ExtraContent $extraContent): ExtraContent
    {
    	$extraContent->setCreatedAt(date('Y-m-d H:i:s'));
		$extraContent->setUpdatedAt(date('Y-m-d H:i:s'));

        $id = $this->newQuery()
            ->insertGetId([
                'uri' => $extraContent->getUri(),
				'type' => $extraContent->getType(),
				'title' => $extraContent->getTitle(),
				'description' => $extraContent->getDescription(),
				'priority' => $extraContent->getPriority(),
				'published' => $extraContent->getPublished(),
				'created_at' => $extraContent->getCreatedAt(),
				'updated_at' => $extraContent->getUpdatedAt(),
            ]);

        $extraContent->setId($id);

        return $extraContent;
    }

    public function update(ExtraContent $extraContent): int
    {
    	$extraContent->setUpdatedAt(date('Y-m-d H:i:s'));

        return $this->newQuery()
           ->where($this->primaryKey, $extraContent->getPrimaryKey())
            ->update([
                'uri' => $extraContent->getUri(),
				'type' => $extraContent->getType(),
				'title' => $extraContent->getTitle(),
				'description' => $extraContent->getDescription(),
				'priority' => $extraContent->getPriority(),
				'published' => $extraContent->getPublished(),
				'updated_at' => $extraContent->getUpdatedAt(),
            ]);
    }
}
