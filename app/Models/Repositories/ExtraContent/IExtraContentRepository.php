<?php

namespace App\Models\Repositories\ExtraContent;

use App\Models\Entities\ExtraContent;
use Illuminate\Support\Collection;

interface IExtraContentRepository
{
    public function getOneById(int $id): null|ExtraContent;

    public function getAllByIds(array $ids): Collection;

    public function getAllByUri(string $uri): Collection;

    public function create(ExtraContent $extraContent): ExtraContent;

    public function update(ExtraContent $extraContent): int;

}
