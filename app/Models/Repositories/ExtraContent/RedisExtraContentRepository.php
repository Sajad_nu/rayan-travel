<?php

namespace App\Models\Repositories\ExtraContent;

use App\Models\Repositories\Redis\QueryCacheStrategy;
use App\Models\Repositories\RedisRepository;

class RedisExtraContentRepository extends RedisRepository
{
    use QueryCacheStrategy;

    public function __construct()
    {
        $this->cacheTag = 'extra_contents';
        parent::__construct();
    }
}
