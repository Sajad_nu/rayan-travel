<?php

namespace App\Models\Repositories\ExtraContent;

use App\Models\Entities\ExtraContent;
use Illuminate\Support\Collection;

class ExtraContentRepository implements IExtraContentRepository
{
    private IExtraContentRepository $repository;
    private RedisExtraContentRepository $redisRepository;

    public function __construct()
    {
        $this->repository = new MySqlExtraContentRepository();
        $this->redisRepository = new RedisExtraContentRepository();
    }

    public function getOneById(int $id): null|ExtraContent
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'id' => $id,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getOneById($id);
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $entity;
    }

    public function getAllByIds(array $ids): Collection
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'id' => $ids,
        ]);

        $entities = $this->redisRepository->get($cacheKey);

        if (is_null($entities)) {
            $entities = $this->repository->getAllByIds($ids);
            $this->redisRepository->put($cacheKey, $entities);
        }

        return $entities;
    }

    public function getAllByUri(string $uri): Collection
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'uri' => $uri,
        ]);

        $entities = $this->redisRepository->get($cacheKey);

        if (is_null($entities)) {
            $entities = $this->repository->getAllByUri($uri);
            $this->redisRepository->put($cacheKey, $entities);
        }

        return $entities;
    }

    public function create(ExtraContent $extraContent): ExtraContent
    {
        $this->redisRepository->clear();

        return $this->repository->create($extraContent);
    }

    public function update(ExtraContent $extraContent): int
    {
        $this->redisRepository->clear();

        return $this->repository->update($extraContent);
    }
}
