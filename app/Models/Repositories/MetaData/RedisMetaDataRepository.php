<?php

namespace App\Models\Repositories\MetaData;

use App\Models\Repositories\Redis\QueryCacheStrategy;
use App\Models\Repositories\RedisRepository;

class RedisMetaDataRepository extends RedisRepository
{
     use QueryCacheStrategy;

    public function __construct()
    {
        $this->cacheTag = 'meta_data';
        parent::__construct();
    }
}
