<?php

namespace App\Models\Repositories\MetaData;

use App\Models\Entities\MetaData;
use Illuminate\Support\Collection;

class MetaDataRepository implements IMetaDataRepository
{
    private IMetaDataRepository $repository;
	private RedisMetaDataRepository $redisRepository;

	public function __construct()
    {
        $this->repository = new MySqlMetaDataRepository();
		$this->redisRepository = new RedisMetaDataRepository();
    }

    public function getOneById(int $id): null|MetaData
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'id' => $id,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getOneById($id);
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $entity;
    }

    public function getOneByUri(string $uri): null|MetaData
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'uri' => $uri,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getOneByUri($uri);
            $this->redisRepository->put($cacheKey, $entity);
        }
        return $entity;
    }

    public function getAllByIds(array $ids): Collection
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'id' => $ids,
        ]);

        $entities = $this->redisRepository->get($cacheKey);

        if (is_null($entities)) {
            $entities = $this->repository->getAllByIds($ids);
            $this->redisRepository->put($cacheKey, $entities);
        }

        return $entities;
    }

    public function create(MetaData $metaData): MetaData
    {
        $this->redisRepository->clear();

        return $this->repository->create($metaData);
    }

    public function update(MetaData $metaData): int
    {
        $this->redisRepository->clear();

        return $this->repository->update($metaData);
    }

    public function remove(MetaData $metaData): int
    {
        $this->redisRepository->clear();

        return $this->repository->remove($metaData);
    }

    public function restore(MetaData $metaData): int
    {
        $this->redisRepository->clear();

        return $this->repository->restore($metaData);
    }
}
