<?php

namespace App\Models\Repositories\MetaData;

use App\Models\Entities\MetaData;
use App\Models\Factories\MetaDataFactory;
use App\Models\Repositories\MySqlRepository;
use Illuminate\Support\Collection;

class MySqlMetaDataRepository extends MySqlRepository implements IMetaDataRepository
{
    public function __construct()
    {
        $this->table = 'meta_data';
        $this->primaryKey = 'id';
        $this->softDelete = true;
    }

    public function getOneById(int $id): null|MetaData
    {
        $metaData = $this->newQuery()
            ->where('id', $id)
            ->first();

        return $metaData ? (new MetaDataFactory())->make($metaData) : null;
    }

    public function getOneByUri(string $uri): null|MetaData
    {
        $metaData = $this->newQuery()
            ->where('uri', $uri)
            ->first();

        return $metaData ? (new MetaDataFactory())->make($metaData) : null;
    }

    public function getAllByIds(array $ids): Collection
    {
        $metaData = $this->newQuery()
            ->whereIn('id', $ids)
            ->get();

        return (new MetaDataFactory())->makeFromCollection($metaData);
    }

    public function create(MetaData $metaData): MetaData
    {
        $metaData->setCreatedAt(date('Y-m-d H:i:s'));
        $metaData->setUpdatedAt(date('Y-m-d H:i:s'));

        $id = $this->newQuery()
            ->insertGetId([
                'uri' => $metaData->getUri(),
                'title' => $metaData->getTitle(),
                'description' => $metaData->getDescription(),
                'locale' => $metaData->getLocale(),
                'canonical_url' => $metaData->getCanonicalUrl(),
                'index' => $metaData->getIndex(),
                'robots' => $metaData->getRobots(),
                'type' => $metaData->getType(),
                'author' => $metaData->getAuthor(),
                'keywords' => $metaData->getKeywords(),
                'image' => $metaData->getImage(),
                'disabled' => $metaData->getDisabled(),
                'published_time' => $metaData->getPublishedTime(),
                'modified_time' => $metaData->getModifiedTime(),
                'created_at' => $metaData->getCreatedAt(),
                'updated_at' => $metaData->getUpdatedAt(),
            ]);

        $metaData->setId($id);

        return $metaData;
    }

    public function update(MetaData $metaData): int
    {
        $metaData->setUpdatedAt(date('Y-m-d H:i:s'));

        return $this->newQuery()
            ->where($this->primaryKey, $metaData->getPrimaryKey())
            ->update([
                'uri' => $metaData->getUri(),
                'title' => $metaData->getTitle(),
                'description' => $metaData->getDescription(),
                'locale' => $metaData->getLocale(),
                'canonical_url' => $metaData->getCanonicalUrl(),
                'index' => $metaData->getIndex(),
                'robots' => $metaData->getRobots(),
                'type' => $metaData->getType(),
                'author' => $metaData->getAuthor(),
                'keywords' => $metaData->getKeywords(),
                'image' => $metaData->getImage(),
                'disabled' => $metaData->getDisabled(),
                'published_time' => $metaData->getPublishedTime(),
                'modified_time' => $metaData->getModifiedTime(),
                'updated_at' => $metaData->getUpdatedAt(),
            ]);
    }

    public function remove(MetaData $metaData): int
    {
        return $this->newQuery()
            ->where($this->primaryKey, $metaData->getPrimaryKey())
            ->update([
                'deleted_at' => date('Y-m-d H:i:s'),
            ]);
    }

    public function restore(MetaData $metaData): int
    {
        return $this->newQuery()
            ->where($this->primaryKey, $metaData->getPrimaryKey())
            ->update([
                'deleted_at' => null,
            ]);
    }
}
