<?php

namespace App\Models\Repositories\MetaData;

use App\Models\Entities\MetaData;
use Illuminate\Support\Collection;

interface IMetaDataRepository
{
    public function getOneById(int $id): null|MetaData;

    public function getOneByUri(string $uri): null|MetaData;

    public function getAllByIds(array $ids): Collection;

    public function create(MetaData $metaData): MetaData;

    public function update(MetaData $metaData): int;

    public function remove(MetaData $metaData): int;

    public function restore(MetaData $metaData): int;
}
