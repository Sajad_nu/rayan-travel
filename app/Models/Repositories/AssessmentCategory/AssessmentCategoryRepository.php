<?php

namespace App\Models\Repositories\AssessmentCategory;

use App\Models\Entities\AssessmentCategory;
use Illuminate\Support\Collection;

class AssessmentCategoryRepository implements IAssessmentCategoryRepository
{
    private IAssessmentCategoryRepository $repository;
	private RedisAssessmentCategoryRepository $redisRepository;

	public function __construct()
    {
        $this->repository = new MySqlAssessmentCategoryRepository();
		$this->redisRepository = new RedisAssessmentCategoryRepository();
    }

    public function getAll(): Collection
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => 'getAll',
        ]);

        $entities = $this->redisRepository->get($cacheKey);

        if ($entities == null) {
            $entities = $this->repository->getAll();
            $this->redisRepository->put($cacheKey, $entities);
        }

        return $entities;
    }

    public function getOneById(int $id): null|AssessmentCategory
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => 'getOneById',
            'id' => $id,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getOneById($id);
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $this->repository->getOneById($id);
    }

    public function getAllByIds(array $ids): Collection
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => 'getAllByIds',
            'id' => $ids,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getAllByIds($ids);
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $this->repository->getAllByIds($ids);
    }

    public function create(AssessmentCategory $assessmentCategory): AssessmentCategory
    {
        $this->redisRepository->clear();

        return $this->repository->create($assessmentCategory);
    }

    public function update(AssessmentCategory $assessmentCategory): int
    {
        $this->redisRepository->clear();

        return $this->repository->update($assessmentCategory);
    }
}
