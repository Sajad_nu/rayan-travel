<?php

namespace App\Models\Repositories\AssessmentCategory;

use App\Models\Entities\AssessmentCategory;
use App\Models\Factories\AssessmentCategoryFactory;
use App\Models\Repositories\MySqlRepository;
use Illuminate\Support\Collection;

class MySqlAssessmentCategoryRepository extends MySqlRepository implements IAssessmentCategoryRepository
{
    public function __construct()
    {
        $this->table = 'assessment_categories';
        $this->primaryKey = 'id';
        $this->softDelete = false;
    }

    public function getAll(): Collection
    {
        $assessmentCategories = $this->newQuery()
            ->get();

        return (new AssessmentCategoryFactory())->makeFromCollection($assessmentCategories);
    }

    public function getOneById(int $id): null|AssessmentCategory
    {
        $assessmentCategory = $this->newQuery()
            ->where('id', $id)
            ->first();

        return $assessmentCategory ? (new AssessmentCategoryFactory())->make($assessmentCategory) : null;
    }

    public function getAllByIds(array $ids): Collection
    {
        $assessmentCategory = $this->newQuery()
            ->whereIn('id', $ids)
            ->get();

        return (new AssessmentCategoryFactory())->makeFromCollection($assessmentCategory);
    }

    public function create(AssessmentCategory $assessmentCategory): AssessmentCategory
    {
        $assessmentCategory->setCreatedAt(date('Y-m-d H:i:s'));
        $assessmentCategory->setUpdatedAt(date('Y-m-d H:i:s'));

        $id = $this->newQuery()
            ->insertGetId([
                'title' => $assessmentCategory->getTitle(),
                'priority' => $assessmentCategory->getPriority(),
                'created_at' => $assessmentCategory->getCreatedAt(),
                'updated_at' => $assessmentCategory->getUpdatedAt(),
            ]);

        $assessmentCategory->setId($id);

        return $assessmentCategory;
    }

    public function update(AssessmentCategory $assessmentCategory): int
    {
        $assessmentCategory->setUpdatedAt(date('Y-m-d H:i:s'));

        return $this->newQuery()
            ->where($this->primaryKey, $assessmentCategory->getPrimaryKey())
            ->update([
                'title' => $assessmentCategory->getTitle(),
                'priority' => $assessmentCategory->getPriority(),
                'updated_at' => $assessmentCategory->getUpdatedAt(),
            ]);
    }
}
