<?php

namespace App\Models\Repositories\AssessmentCategory;

use App\Models\Entities\AssessmentCategory;
use Illuminate\Support\Collection;

interface IAssessmentCategoryRepository
{
    public function getAll(): Collection;

    public function getOneById(int $id): null|AssessmentCategory;

    public function getAllByIds(array $ids): Collection;

    public function create(AssessmentCategory $assessmentCategory): AssessmentCategory;

    public function update(AssessmentCategory $assessmentCategory): int;
}
