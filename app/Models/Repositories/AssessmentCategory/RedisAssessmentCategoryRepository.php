<?php

namespace App\Models\Repositories\AssessmentCategory;

use App\Models\Repositories\Redis\QueryCacheStrategy;
use App\Models\Repositories\RedisRepository;

class RedisAssessmentCategoryRepository extends RedisRepository
{
    use QueryCacheStrategy;

    public function __construct()
    {
        $this->cacheTag = 'assessment_categories';
        parent::__construct();
    }
}
