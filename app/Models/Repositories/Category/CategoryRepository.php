<?php

namespace App\Models\Repositories\Category;

use App\Models\Entities\Category;
use Illuminate\Support\Collection;

class CategoryRepository implements ICategoryRepository
{
    private ICategoryRepository $repository;
    private RedisCategoryRepository $redisRepository;

    public function __construct()
    {
        $this->repository = new MySqlCategoryRepository();
        $this->redisRepository = new RedisCategoryRepository();
    }

    public function getOneById(int $id): null|Category
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'id' => $id,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getOneById($id);
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $this->repository->getOneById($id);
    }

    public function getOneActiveByType(string $type,bool $withTrashed = false): null|Category
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'type' => $type,
            'with_trashed' => $withTrashed,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getOneActiveByType($type, $withTrashed);
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $entity;
    }

    public function getAllByIds(array $ids): Collection
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'id' => $ids,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getAllByIds($ids);
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $this->repository->getAllByIds($ids);
    }

    public function getAllActives(bool $withTrashed = false): Collection
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'with_trashed' => $withTrashed,
        ]);

        $entities = $this->redisRepository->get($cacheKey);

        if ($entities == null) {
            $entities = $this->repository->getAllActives($withTrashed);
            $this->redisRepository->put($cacheKey, $entities);
        }

        return $entities;
    }

    public function create(Category $category): Category
    {
        $this->redisRepository->clear();

        return $this->repository->create($category);
    }

    public function update(Category $category): int
    {
        $this->redisRepository->clear();

        return $this->repository->update($category);
    }

    public function remove(Category $category): int
    {
        $this->redisRepository->clear();

        return $this->repository->remove($category);
    }

    public function restore(Category $category): int
    {
        $this->redisRepository->clear();

        return $this->repository->restore($category);
    }
}
