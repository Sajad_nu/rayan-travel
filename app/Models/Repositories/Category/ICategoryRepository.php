<?php

namespace App\Models\Repositories\Category;

use App\Models\Entities\Category;
use Illuminate\Support\Collection;

interface ICategoryRepository
{
    public function getOneById(int $id): null|Category;

    public function getOneActiveByType(string $type, bool $withTrashed = false): null|Category;

    public function getAllByIds(array $ids): Collection;

    public function getAllActives(bool $withTrashed): Collection;

    public function create(Category $category): Category;

    public function update(Category $category): int;

    public function remove(Category $category): int;

    public function restore(Category $category): int;
}
