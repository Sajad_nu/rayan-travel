<?php

namespace App\Models\Repositories\Category;

use App\Models\Entities\Category;
use App\Models\Factories\CategoryFactory;
use App\Models\Repositories\MySqlRepository;
use Illuminate\Support\Collection;

class MySqlCategoryRepository extends MySqlRepository implements ICategoryRepository
{
    public function __construct()
    {
        $this->table = 'categories';
        $this->primaryKey = 'id';
        $this->softDelete = true;
    }

    public function getOneById(int $id): null|Category
    {
        $category = $this->newQuery()
            ->where('id', $id)
            ->first();

        return $category ? (new CategoryFactory())->make($category) : null;
    }

    public function getOneActiveByType(string $type, bool $withTrashed = false): null|Category
    {
        $query = $this->newQuery()
            ->where('type', $type)
            ->where('disabled', false);

        if ($withTrashed === false) {
            $query->whereNull('deleted_at');
        }

        $category = $query->first();

        return (new CategoryFactory())->make($category);
    }

    public function getAllByIds(array $ids): Collection
    {
        $category = $this->newQuery()
            ->whereIn('id', $ids)
            ->get();

        return (new CategoryFactory())->makeFromCollection($category);
    }

    public function getAllActives(bool $withTrashed = false): Collection
    {
        $query = $this->newQuery()
            ->where('disabled', false);

        if ($withTrashed === false) {
            $query->whereNull('deleted_at');
        }

        $categories = $query->get();

        return (new CategoryFactory())->makeFromCollection($categories);
    }

    public function create(Category $category): Category
    {
        $category->setCreatedAt(date('Y-m-d H:i:s'));
        $category->setUpdatedAt(date('Y-m-d H:i:s'));

        $id = $this->newQuery()
            ->insertGetId([
                'title' => $category->getTitle(),
                'type' => $category->getType(),
                'uri' => $category->getUri(),
                'description' => $category->getDescription(),
                'disabled' => $category->getDisabled(),
                'created_at' => $category->getCreatedAt(),
                'updated_at' => $category->getUpdatedAt(),
            ]);

        $category->setId($id);

        return $category;
    }

    public function update(Category $category): int
    {
        $category->setUpdatedAt(date('Y-m-d H:i:s'));

        return $this->newQuery()
            ->where($this->primaryKey, $category->getPrimaryKey())
            ->update([
                'title' => $category->getTitle(),
                'type' => $category->getType(),
                'uri' => $category->getUri(),
                'description' => $category->getDescription(),
                'disabled' => $category->getDisabled(),
                'updated_at' => $category->getUpdatedAt(),
            ]);
    }

    public function remove(Category $category): int
    {
        return $this->newQuery()
            ->where($this->primaryKey, $category->getPrimaryKey())
            ->update([
                'deleted_at' => date('Y-m-d H:i:s'),
            ]);
    }

    public function restore(Category $category): int
    {
        return $this->newQuery()
            ->where($this->primaryKey, $category->getPrimaryKey())
            ->update([
                'deleted_at' => null,
            ]);
    }
}
