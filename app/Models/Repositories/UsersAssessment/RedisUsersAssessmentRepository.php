<?php

namespace App\Models\Repositories\UsersAssessment;

use App\Models\Repositories\RedisRepository;
use App\Models\Repositories\Redis\QueryCacheStrategy;

class RedisUsersAssessmentRepository extends RedisRepository
{
     use QueryCacheStrategy;

    public function __construct()
    {
        $this->cacheTag = 'users_assessment';
        parent::__construct();
    }
}
