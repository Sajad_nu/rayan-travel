<?php

namespace App\Models\Repositories\UsersAssessment;

use App\Models\Entities\UsersAssessment;
use Illuminate\Support\Collection;

interface IUsersAssessmentRepository
{
    public function getOneById(int $id): null|UsersAssessment;

    public function getOneByHashId(string $hashId): null|UsersAssessment;

    public function getAllByIds(array $ids): Collection;

    public function getAllNotSendAssessmentToExpert(): Collection;

    public function create(UsersAssessment $usersAssessment): UsersAssessment;

    public function update(UsersAssessment $usersAssessment): int;
}
