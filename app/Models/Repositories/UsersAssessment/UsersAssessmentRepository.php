<?php

namespace App\Models\Repositories\UsersAssessment;

use App\Models\Entities\UsersAssessment;
use Illuminate\Support\Collection;

class UsersAssessmentRepository implements IUsersAssessmentRepository
{
    private IUsersAssessmentRepository $repository;
    private RedisUsersAssessmentRepository $redisRepository;

    public function __construct()
    {
        $this->repository = new MySqlUsersAssessmentRepository();
        $this->redisRepository = new RedisUsersAssessmentRepository();
    }

    public function getOneById(int $id): null|UsersAssessment
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => 'getOneById',
            'id' => $id,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getOneById($id);
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $this->repository->getOneById($id);
    }

    public function getOneByHashId(string $hashId): null|UsersAssessment
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => 'getOneByHashId',
            'hashId' => $hashId,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getOneByHashId($hashId);
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $entity;
    }

    public function getAllByIds(array $ids): Collection
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => 'getAllByIds',
            'id' => $ids,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getAllByIds($ids);
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $this->repository->getAllByIds($ids);
    }

    public function getAllNotSendAssessmentToExpert(): Collection
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => 'getAllNotSendAssessmentToExpert',
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getAllNotSendAssessmentToExpert();
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $entity;
    }

    public function create(UsersAssessment $usersAssessment): UsersAssessment
    {
        $this->redisRepository->clear();

        return $this->repository->create($usersAssessment);
    }

    public function update(UsersAssessment $usersAssessment): int
    {
        $this->redisRepository->clear();

        return $this->repository->update($usersAssessment);
    }
}
