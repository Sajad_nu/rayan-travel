<?php

namespace App\Models\Repositories\UsersAssessment;

use App\Models\Entities\UsersAssessment;
use App\Models\Factories\UsersAssessmentFactory;
use App\Models\Repositories\MySqlRepository;
use Illuminate\Support\Collection;

class MySqlUsersAssessmentRepository extends MySqlRepository implements IUsersAssessmentRepository
{
    public function __construct()
    {
        $this->table = 'users_assessment';
        $this->primaryKey = 'id';
        $this->softDelete = false;
    }

    public function getOneById(int $id): null|UsersAssessment
    {
        $usersAssessment = $this->newQuery()
            ->where('id', $id)
            ->first();

        return $usersAssessment ? (new UsersAssessmentFactory())->make($usersAssessment) : null;
    }

    public function getOneByHashId(string $hashId): null|UsersAssessment
    {
        $usersAssessment = $this->newQuery()
            ->where('hash_id', $hashId)
            ->first();

        return $usersAssessment ? (new UsersAssessmentFactory())->make($usersAssessment) : null;
    }

    public function getAllByIds(array $ids): Collection
    {
        $usersAssessment = $this->newQuery()
            ->whereIn('id', $ids)
            ->get();

        return (new UsersAssessmentFactory())->makeFromCollection($usersAssessment);
    }

    public function getAllNotSendAssessmentToExpert(): Collection
    {
        $usersAssessment = $this->newQuery()
            ->whereNotNull('phone_number')
            ->whereNull('send_at')
            ->get();

        return (new UsersAssessmentFactory())->makeFromCollection($usersAssessment);
    }

    public function create(UsersAssessment $usersAssessment): UsersAssessment
    {
        $usersAssessment->setCreatedAt(date('Y-m-d H:i:s'));
        $usersAssessment->setUpdatedAt(date('Y-m-d H:i:s'));

        $id = $this->newQuery()
            ->insertGetId([
                'hash_id' => $usersAssessment->getHashId(),
                'tracking_code' => $usersAssessment->getTrackingCode(),
                'country_code' => $usersAssessment->getCountryCode(),
                'phone_number' => $usersAssessment->getPhoneNumber(),
                'send_result_to' => $usersAssessment->getSendResultTo(),
                'rank' => $usersAssessment->getRank(),
                'rank_at' => $usersAssessment->getRankAt(),
                'created_at' => $usersAssessment->getCreatedAt(),
                'updated_at' => $usersAssessment->getUpdatedAt(),
            ]);

        $usersAssessment->setId($id);

        return $usersAssessment;
    }

    public function update(UsersAssessment $usersAssessment): int
    {
        $usersAssessment->setUpdatedAt(date('Y-m-d H:i:s'));

        return $this->newQuery()
            ->where($this->primaryKey, $usersAssessment->getPrimaryKey())
            ->update([
                'hash_id' => $usersAssessment->getHashId(),
                'tracking_code' => $usersAssessment->getTrackingCode(),
                'country_code' => $usersAssessment->getCountryCode(),
                'phone_number' => $usersAssessment->getPhoneNumber(),
                'send_result_to' => $usersAssessment->getSendResultTo(),
                'rank' => $usersAssessment->getRank(),
                'rank_at' => $usersAssessment->getRankAt(),
                'send_at' => $usersAssessment->getSendAt(),
                'send_result_at' => $usersAssessment->getSendResultAt(),
                'send_confirm_at' => $usersAssessment->getSendConfirmAt(),
                'updated_at' => $usersAssessment->getUpdatedAt(),
            ]);
    }
}
