<?php

namespace App\Models\Repositories\RequestsAdvice;

use App\Models\Repositories\Redis\QueryCacheStrategy;
use App\Models\Repositories\RedisRepository;

class RedisRequestsAdviceRepository extends RedisRepository
{
    use QueryCacheStrategy;

    public function __construct()
    {
        $this->cacheTag = 'requests_advice';
        parent::__construct();
    }
}
