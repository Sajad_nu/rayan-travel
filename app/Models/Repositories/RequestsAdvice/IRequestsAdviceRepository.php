<?php

namespace App\Models\Repositories\RequestsAdvice;

use App\Models\Entities\RequestsAdvice;
use Illuminate\Support\Collection;

interface IRequestsAdviceRepository
{
    public function getOneById(int $id): null|RequestsAdvice;

    public function getAllByIds(array $ids): Collection;

    public function getAllNotSendToExpert(): Collection;

    public function create(RequestsAdvice $requestsAdvice): RequestsAdvice;

    public function update(RequestsAdvice $requestsAdvice): int;

}
