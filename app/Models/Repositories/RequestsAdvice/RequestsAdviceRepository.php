<?php

namespace App\Models\Repositories\RequestsAdvice;

use App\Models\Entities\RequestsAdvice;
use Illuminate\Support\Collection;

class RequestsAdviceRepository implements IRequestsAdviceRepository
{
    private IRequestsAdviceRepository $repository;
    private RedisRequestsAdviceRepository $redisRepository;

    public function __construct()
    {
        $this->repository = new MySqlRequestsAdviceRepository();
        $this->redisRepository = new RedisRequestsAdviceRepository();
    }

    public function getOneById(int $id): null|RequestsAdvice
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'id' => $id,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getOneById($id);
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $entity;
    }

    public function getAllByIds(array $ids): Collection
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'id' => $ids,
        ]);

        $entities = $this->redisRepository->get($cacheKey);

        if (is_null($entities)) {
            $entities = $this->repository->getAllByIds($ids);
            $this->redisRepository->put($cacheKey, $entities);
        }

        return $entities;
    }

    public function getAllNotSendToExpert(): Collection
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getAllNotSendToExpert();
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $entity;
    }

    public function create(RequestsAdvice $requestsAdvice): RequestsAdvice
    {
        $this->redisRepository->clear();

        return $this->repository->create($requestsAdvice);
    }

    public function update(RequestsAdvice $requestsAdvice): int
    {
        $this->redisRepository->clear();

        return $this->repository->update($requestsAdvice);
    }
}
