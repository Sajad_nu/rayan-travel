<?php

namespace App\Models\Repositories\RequestsAdvice;

use App\Models\Entities\RequestsAdvice;
use App\Models\Factories\RequestsAdviceFactory;
use App\Models\Repositories\MySqlRepository;
use Illuminate\Support\Collection;

class MySqlRequestsAdviceRepository extends MySqlRepository implements IRequestsAdviceRepository
{
    private RequestsAdviceFactory $factory;

    public function __construct()
    {
        $this->table = 'requests_advice';
        $this->primaryKey = 'id';
        $this->softDelete = false;
        $this->factory = new RequestsAdviceFactory();
    }

    public function getOneById(int $id): null|RequestsAdvice
    {
        $requestsAdvice = $this->newQuery()
            ->where('id', $id)
            ->first();

        return $requestsAdvice ? $this->factory->make($requestsAdvice) : null;
    }

    public function getAllByIds(array $ids): Collection
    {
        $requestsAdvice = $this->newQuery()
            ->whereIn('id', $ids)
            ->get();

        return $this->factory->makeFromCollection($requestsAdvice);
    }

    public function getAllNotSendToExpert(): Collection
    {
        $usersAssessment = $this->newQuery()
            ->whereNotNull('phone_number')
            ->whereNull('send_to_expert_at')
            ->get();

        return $this->factory->makeFromCollection($usersAssessment);
    }


    public function create(RequestsAdvice $requestsAdvice): RequestsAdvice
    {
        $requestsAdvice->setCreatedAt(date('Y-m-d H:i:s'));
        $requestsAdvice->setUpdatedAt(date('Y-m-d H:i:s'));

        $id = $this->newQuery()
            ->insertGetId([
                'advice_time_id' => $requestsAdvice->getAdviceTimeId(),
                'phone_number' => $requestsAdvice->getPhoneNumber(),
                'full_name' => $requestsAdvice->getFullName(),
                'type' => $requestsAdvice->getType(),
                'tracking_code' => $requestsAdvice->getTrackingCode(),
                'send_to_expert_at' => $requestsAdvice->getSendToExpertAt(),
                'created_at' => $requestsAdvice->getCreatedAt(),
                'updated_at' => $requestsAdvice->getUpdatedAt(),
            ]);

        $requestsAdvice->setId($id);

        return $requestsAdvice;
    }

    public function update(RequestsAdvice $requestsAdvice): int
    {
        $requestsAdvice->setUpdatedAt(date('Y-m-d H:i:s'));

        return $this->newQuery()
            ->where($this->primaryKey, $requestsAdvice->getPrimaryKey())
            ->update([
                'advice_time_id' => $requestsAdvice->getAdviceTimeId(),
                'phone_number' => $requestsAdvice->getPhoneNumber(),
                'full_name' => $requestsAdvice->getFullName(),
                'type' => $requestsAdvice->getType(),
                'tracking_code' => $requestsAdvice->getTrackingCode(),
                'send_to_expert_at' => $requestsAdvice->getSendToExpertAt(),
                'updated_at' => $requestsAdvice->getUpdatedAt(),
            ]);
    }
}
