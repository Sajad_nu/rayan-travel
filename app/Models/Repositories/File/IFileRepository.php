<?php

namespace App\Models\Repositories\File;

use App\Models\Entities\File;
use Illuminate\Support\Collection;

interface IFileRepository
{
    public function getOneById(int $id): null|File;

    public function getOneByLocationId(int $locationId): null|File;

    public function getAllByIds(array $ids): Collection;

    public function getAllByCategoryIds(array $categoryIds): Collection;

    public function getAllByVisaIds(array $visaIds): Collection;

    public function create(File $file): File;

    public function update(File $file): int;

    public function remove(File $file): int;

    public function restore(File $file): int;
}
