<?php

namespace App\Models\Repositories\File;

use App\Models\Entities\File;
use App\Models\Factories\FileFactory;
use App\Models\Repositories\MySqlRepository;
use Illuminate\Support\Collection;

class MySqlFileRepository extends MySqlRepository implements IFileRepository
{
    public function __construct()
    {
        $this->table = 'files';
        $this->primaryKey = 'id';
        $this->softDelete = true;
    }

    public function getOneById(int $id): null|File
    {
        $file = $this->newQuery()
            ->where('id', $id)
            ->first();

        return $file ? (new FileFactory())->make($file) : null;
    }

    public function getOneByLocationId(int $locationId): null|File
    {
        $file = $this->newQuery()
            ->where('location_id', $locationId)
            ->where('published', true)
            ->whereNull('deleted_at')
            ->first();

        return $file ? (new FileFactory())->make($file) : null;
    }

    public function getAllByIds(array $ids): Collection
    {
        $file = $this->newQuery()
            ->whereIn('id', $ids)
            ->get();

        return (new FileFactory())->makeFromCollection($file);
    }

    public function getAllByCategoryIds(array $categoryIds): Collection
    {
        $file = $this->newQuery()
            ->whereIn('category_id', $categoryIds)
            ->where('published', true)
            ->whereNull('deleted_at')
            ->get();

        return (new FileFactory())->makeFromCollection($file);
    }

    public function getAllByVisaIds(array $visaIds): Collection
    {
        $file = $this->newQuery()
            ->whereIn('visa_id', $visaIds)
            ->where('published', true)
            ->whereNull('deleted_at')
            ->get();

        return (new FileFactory())->makeFromCollection($file);
    }

    public function create(File $file): File
    {
        $file->setCreatedAt(date('Y-m-d H:i:s'));
        $file->setUpdatedAt(date('Y-m-d H:i:s'));

        $id = $this->newQuery()
            ->insertGetId([
                'category_id' => $file->getCategoryId(),
                'visa_id' => $file->getVisaId(),
                'name' => $file->getName(),
                'description' => $file->getDescription(),
                'hash' => $file->getHash(),
                'type' => $file->getType(),
                'path' => $file->getPath(),
                'priority' => $file->getPriority(),
                'published' => $file->getPublished(),
                'created_at' => $file->getCreatedAt(),
                'updated_at' => $file->getUpdatedAt(),
            ]);

        $file->setId($id);

        return $file;
    }

    public function update(File $file): int
    {
        $file->setUpdatedAt(date('Y-m-d H:i:s'));

        return $this->newQuery()
            ->where($this->primaryKey, $file->getPrimaryKey())
            ->update([
                'category_id' => $file->getCategoryId(),
                'visa_id' => $file->getVisaId(),
                'name' => $file->getName(),
                'description' => $file->getDescription(),
                'hash' => $file->getHash(),
                'type' => $file->getType(),
                'path' => $file->getPath(),
                'priority' => $file->getPriority(),
                'published' => $file->getPublished(),
                'updated_at' => $file->getUpdatedAt(),
            ]);
    }

    public function remove(File $file): int
    {
        return $this->newQuery()
            ->where($this->primaryKey, $file->getPrimaryKey())
            ->update([
                'deleted_at' => date('Y-m-d H:i:s'),
            ]);
    }

    public function restore(File $file): int
    {
        return $this->newQuery()
            ->where($this->primaryKey, $file->getPrimaryKey())
            ->update([
                'deleted_at' => null,
            ]);
    }
}
