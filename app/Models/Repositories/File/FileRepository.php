<?php

namespace App\Models\Repositories\File;

use App\Models\Entities\File;
use Illuminate\Support\Collection;

class FileRepository implements IFileRepository
{
    private IFileRepository $repository;
    private RedisFileRepository $redisRepository;

    public function __construct()
    {
        $this->repository = new MySqlFileRepository();
        $this->redisRepository = new RedisFileRepository();
    }

    public function getOneById(int $id): null|File
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'id' => $id,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getOneById($id);
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $entity;
    }

    public function getOneByLocationId(int $locationId): null|File
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'location_id' => $locationId,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getOneByLocationId($locationId);
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $entity;
    }

    public function getAllByIds(array $ids): Collection
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'id' => $ids,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getAllByIds($ids);
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $entity;
    }

    public function getAllByCategoryIds(array $categoryIds): Collection
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'category_ids' => $categoryIds,
        ]);

        $entities = $this->redisRepository->get($cacheKey);

        if (is_null($entities)) {
            $entities = $this->repository->getAllByCategoryIds($categoryIds);
            $this->redisRepository->put($cacheKey, $entities);
        }

        return $entities;
    }

    public function getAllByVisaIds(array $visaIds): Collection
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'visa_ids' => $visaIds,
        ]);

        $entities = $this->redisRepository->get($cacheKey);

        if (is_null($entities)) {
            $entities = $this->repository->getAllByVisaIds($visaIds);
            $this->redisRepository->put($cacheKey, $entities);
        }

        return $entities;
    }

    public function create(File $file): File
    {
        $this->redisRepository->clear();

        return $this->repository->create($file);
    }

    public function update(File $file): int
    {
        $this->redisRepository->clear();

        return $this->repository->update($file);
    }

    public function remove(File $file): int
    {
        $this->redisRepository->clear();

        return $this->repository->remove($file);
    }

    public function restore(File $file): int
    {
        $this->redisRepository->clear();

        return $this->repository->restore($file);
    }
}
