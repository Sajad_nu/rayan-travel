<?php

namespace App\Models\Repositories\VisaProcedure;

use App\Models\Entities\VisaProcedure;
use Illuminate\Support\Collection;

class VisaProcedureRepository implements IVisaProcedureRepository
{
    private IVisaProcedureRepository $repository;
    private RedisVisaProcedureRepository $redisRepository;

    public function __construct()
    {
        $this->repository = new MySqlVisaProcedureRepository();
        $this->redisRepository = new RedisVisaProcedureRepository();
    }

    public function getOneById(int $id): null|VisaProcedure
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'id' => $id,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getOneById($id);
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $this->repository->getOneById($id);
    }

    public function getAllByIds(array $ids): Collection
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'id' => $ids,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getAllByIds($ids);
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $this->repository->getAllByIds($ids);
    }

    public function getAllByVisaId(int $visaId, $withTrashed = false): Collection
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'visa_id' => $visaId,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getAllByVisaId($visaId, $withTrashed);
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $entity;
    }

    public function create(VisaProcedure $visaProcedure): VisaProcedure
    {
        $this->redisRepository->clear();

        return $this->repository->create($visaProcedure);
    }

    public function update(VisaProcedure $visaProcedure): int
    {
        $this->redisRepository->clear();

        return $this->repository->update($visaProcedure);
    }

    public function remove(VisaProcedure $visaProcedure): int
    {
        $this->redisRepository->clear();

        return $this->repository->remove($visaProcedure);
    }

    public function restore(VisaProcedure $visaProcedure): int
    {
        $this->redisRepository->clear();

        return $this->repository->restore($visaProcedure);
    }
}
