<?php

namespace App\Models\Repositories\VisaProcedure;

use App\Models\Entities\VisaProcedure;
use App\Models\Factories\VisaProcedureFactory;
use App\Models\Repositories\MySqlRepository;
use Illuminate\Support\Collection;

class MySqlVisaProcedureRepository extends MySqlRepository implements IVisaProcedureRepository
{
    public function __construct()
    {
        $this->table = 'visa_procedures';
        $this->primaryKey = 'id';
        $this->softDelete = true;
    }

    public function getOneById(int $id): null|VisaProcedure
    {
        $visaProcedure = $this->newQuery()
            ->where('id', $id)
            ->first();

        return $visaProcedure ? (new VisaProcedureFactory())->make($visaProcedure) : null;
    }

    public function getAllByIds(array $ids): Collection
    {
        $visaProcedure = $this->newQuery()
            ->whereIn('id', $ids)
            ->get();

        return (new VisaProcedureFactory())->makeFromCollection($visaProcedure);
    }


    public function getAllByVisaId(int $visaId, $withTrashed = false): Collection
    {
        $query = $this->newQuery()
            ->where('visa_id', $visaId);

        if ($withTrashed === false) {
            $query->whereNull('deleted_at');
        }
        $visaDocuments = $query->get();

        return (new VisaProcedureFactory())->makeFromCollection($visaDocuments);
    }

    public function create(VisaProcedure $visaProcedure): VisaProcedure
    {
        $visaProcedure->setCreatedAt(date('Y-m-d H:i:s'));
        $visaProcedure->setUpdatedAt(date('Y-m-d H:i:s'));

        $id = $this->newQuery()
            ->insertGetId([
                'visa_id' => $visaProcedure->getVisaId(),
                'procedures_id' => $visaProcedure->getProceduresId(),
                'description' => $visaProcedure->getDescription(),
                'created_at' => $visaProcedure->getCreatedAt(),
                'updated_at' => $visaProcedure->getUpdatedAt(),
            ]);

        $visaProcedure->setId($id);

        return $visaProcedure;
    }

    public function update(VisaProcedure $visaProcedure): int
    {
        $visaProcedure->setUpdatedAt(date('Y-m-d H:i:s'));

        return $this->newQuery()
            ->where($this->primaryKey, $visaProcedure->getPrimaryKey())
            ->update([
                'visa_id' => $visaProcedure->getVisaId(),
                'procedures_id' => $visaProcedure->getProceduresId(),
                'description' => $visaProcedure->getDescription(),
                'updated_at' => $visaProcedure->getUpdatedAt(),
            ]);
    }

    public function remove(VisaProcedure $visaProcedure): int
    {
        return $this->newQuery()
            ->where($this->primaryKey, $visaProcedure->getPrimaryKey())
            ->update([
                'deleted_at' => date('Y-m-d H:i:s'),
            ]);
    }

    public function restore(VisaProcedure $visaProcedure): int
    {
        return $this->newQuery()
            ->where($this->primaryKey, $visaProcedure->getPrimaryKey())
            ->update([
                'deleted_at' => null,
            ]);
    }
}
