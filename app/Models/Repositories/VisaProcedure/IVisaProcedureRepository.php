<?php

namespace App\Models\Repositories\VisaProcedure;

use App\Models\Entities\VisaProcedure;
use Illuminate\Support\Collection;

interface IVisaProcedureRepository
{
    public function getOneById(int $id): null|VisaProcedure;

    public function getAllByIds(array $ids): Collection;

    public function getAllByVisaId(int $visaId, $withTrashed = false): Collection;

    public function create(VisaProcedure $visaProcedure): VisaProcedure;

    public function update(VisaProcedure $visaProcedure): int;

    public function remove(VisaProcedure $visaProcedure): int;

    public function restore(VisaProcedure $visaProcedure): int;
}
