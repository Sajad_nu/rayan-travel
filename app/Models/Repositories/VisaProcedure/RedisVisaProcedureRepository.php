<?php

namespace App\Models\Repositories\VisaProcedure;

use App\Models\Repositories\Redis\QueryCacheStrategy;
use App\Models\Repositories\RedisRepository;

class RedisVisaProcedureRepository extends RedisRepository
{
     use QueryCacheStrategy;

    public function __construct()
    {
        $this->cacheTag = 'visa_procedures';
        parent::__construct();
    }
}
