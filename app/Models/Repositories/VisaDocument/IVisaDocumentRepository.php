<?php

namespace App\Models\Repositories\VisaDocument;

use App\Models\Entities\VisaDocument;
use Illuminate\Support\Collection;

interface IVisaDocumentRepository
{
    public function getOneById(int $id): null|VisaDocument;

    public function getAllByIds(array $ids): Collection;

    public function getAllByVisaId(int $visaId, $withTrashed = false): Collection;

    public function create(VisaDocument $visaDocument): VisaDocument;

    public function update(VisaDocument $visaDocument): int;

    public function remove(VisaDocument $visaDocument): int;

    public function restore(VisaDocument $visaDocument): int;
}
