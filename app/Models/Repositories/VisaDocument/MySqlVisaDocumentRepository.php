<?php

namespace App\Models\Repositories\VisaDocument;

use App\Models\Entities\VisaDocument;
use App\Models\Factories\VisaDocumentFactory;
use App\Models\Repositories\MySqlRepository;
use Illuminate\Support\Collection;

class MySqlVisaDocumentRepository extends MySqlRepository implements IVisaDocumentRepository
{
    public function __construct()
    {
        $this->table = 'visa_documents';
        $this->primaryKey = 'id';
        $this->softDelete = true;
    }

    public function getOneById(int $id): null|VisaDocument
    {
        $visaDocument = $this->newQuery()
            ->where('id', $id)
            ->first();

        return $visaDocument ? (new VisaDocumentFactory())->make($visaDocument) : null;
    }

    public function getAllByIds(array $ids): Collection
    {
        $visaDocument = $this->newQuery()
            ->whereIn('id', $ids)
            ->get();

        return (new VisaDocumentFactory())->makeFromCollection($visaDocument);
    }

    public function getAllByVisaId(int $visaId, $withTrashed = false): Collection
    {
        $query = $this->newQuery()
            ->where('visa_id', $visaId);

        if ($withTrashed === false) {
            $query->whereNull('deleted_at');
        }
        $visaDocuments = $query->get();

        return (new VisaDocumentFactory())->makeFromCollection($visaDocuments);
    }

    public function create(VisaDocument $visaDocument): VisaDocument
    {
        $visaDocument->setCreatedAt(date('Y-m-d H:i:s'));
        $visaDocument->setUpdatedAt(date('Y-m-d H:i:s'));

        $id = $this->newQuery()
            ->insertGetId([
                'visa_id' => $visaDocument->getVisaId(),
                'document_id' => $visaDocument->getDocumentId(),
                'description' => $visaDocument->getDescription(),
                'created_at' => $visaDocument->getCreatedAt(),
                'updated_at' => $visaDocument->getUpdatedAt(),
            ]);

        $visaDocument->setId($id);

        return $visaDocument;
    }

    public function update(VisaDocument $visaDocument): int
    {
        $visaDocument->setUpdatedAt(date('Y-m-d H:i:s'));

        return $this->newQuery()
            ->where($this->primaryKey, $visaDocument->getPrimaryKey())
            ->update([
                'visa_id' => $visaDocument->getVisaId(),
                'document_id' => $visaDocument->getDocumentId(),
                'description' => $visaDocument->getDescription(),
                'updated_at' => $visaDocument->getUpdatedAt(),
            ]);
    }

    public function remove(VisaDocument $visaDocument): int
    {
        return $this->newQuery()
            ->where($this->primaryKey, $visaDocument->getPrimaryKey())
            ->update([
                'deleted_at' => date('Y-m-d H:i:s'),
            ]);
    }

    public function restore(VisaDocument $visaDocument): int
    {
        return $this->newQuery()
            ->where($this->primaryKey, $visaDocument->getPrimaryKey())
            ->update([
                'deleted_at' => null,
            ]);
    }
}
