<?php

namespace App\Models\Repositories\VisaDocument;

use App\Models\Repositories\Redis\QueryCacheStrategy;
use App\Models\Repositories\RedisRepository;

class RedisVisaDocumentRepository extends RedisRepository
{
    use QueryCacheStrategy;

    public function __construct()
    {
        $this->cacheTag = 'visa_documents';
        parent::__construct();
    }
}
