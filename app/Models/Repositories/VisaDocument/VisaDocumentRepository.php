<?php

namespace App\Models\Repositories\VisaDocument;

use App\Models\Entities\VisaDocument;
use Illuminate\Support\Collection;

class VisaDocumentRepository implements IVisaDocumentRepository
{
    private IVisaDocumentRepository $repository;
    private RedisVisaDocumentRepository $redisRepository;

    public function __construct()
    {
        $this->repository = new MySqlVisaDocumentRepository();
        $this->redisRepository = new RedisVisaDocumentRepository();
    }

    public function getOneById(int $id): null|VisaDocument
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'id' => $id,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getOneById($id);
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $entity;
    }

    public function getAllByIds(array $ids): Collection
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'id' => $ids,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getAllByIds($ids);
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $entity;
    }

    public function getAllByVisaId(int $visaId, $withTrashed = false): Collection
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'visa_id' => $visaId,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getAllByVisaId($visaId, $withTrashed);
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $entity;
    }

    public function create(VisaDocument $visaDocument): VisaDocument
    {
        $this->redisRepository->clear();

        return $this->repository->create($visaDocument);
    }

    public function update(VisaDocument $visaDocument): int
    {
        $this->redisRepository->clear();

        return $this->repository->update($visaDocument);
    }

    public function remove(VisaDocument $visaDocument): int
    {
        $this->redisRepository->clear();

        return $this->repository->remove($visaDocument);
    }

    public function restore(VisaDocument $visaDocument): int
    {
        $this->redisRepository->clear();

        return $this->repository->restore($visaDocument);
    }
}
