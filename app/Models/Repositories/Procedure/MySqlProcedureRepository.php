<?php

namespace App\Models\Repositories\Procedure;

use App\Models\Entities\Procedure;
use App\Models\Factories\ProcedureFactory;
use App\Models\Repositories\MySqlRepository;
use Illuminate\Support\Collection;

class MySqlProcedureRepository extends MySqlRepository implements IProcedureRepository
{
    public function __construct()
    {
        $this->table = 'procedures';
        $this->primaryKey = 'id';
        $this->softDelete = false;
    }

    public function getOneById(int $id): null|Procedure
    {
        $procedure = $this->newQuery()
            ->where('id', $id)
            ->first();

        return $procedure ? (new ProcedureFactory())->make($procedure) : null;
    }

    public function getAllByIds(array $ids): Collection
    {
        $procedure = $this->newQuery()
            ->whereIn('id', $ids)
            ->get();

        return (new ProcedureFactory())->makeFromCollection($procedure);
    }

    public function create(Procedure $procedure): Procedure
    {
        $procedure->setCreatedAt(date('Y-m-d H:i:s'));
        $procedure->setUpdatedAt(date('Y-m-d H:i:s'));

        $id = $this->newQuery()
            ->insertGetId([
                'title' => $procedure->getTitle(),
                'title_en' => $procedure->getTitleEn(),
                'disabled' => $procedure->getDisabled(),
                'priority' => $procedure->getPriority(),
                'created_at' => $procedure->getCreatedAt(),
                'updated_at' => $procedure->getUpdatedAt(),
            ]);

        $procedure->setId($id);

        return $procedure;
    }

    public function update(Procedure $procedure): int
    {
        $procedure->setUpdatedAt(date('Y-m-d H:i:s'));

        return $this->newQuery()
            ->where($this->primaryKey, $procedure->getPrimaryKey())
            ->update([
                'title' => $procedure->getTitle(),
                'title_en' => $procedure->getTitleEn(),
                'disabled' => $procedure->getDisabled(),
                'priority' => $procedure->getPriority(),
                'updated_at' => $procedure->getUpdatedAt(),
            ]);
    }
}
