<?php

namespace App\Models\Repositories\Procedure;

use App\Models\Entities\Procedure;
use Illuminate\Support\Collection;

class ProcedureRepository implements IProcedureRepository
{
    private IProcedureRepository $repository;
	private RedisProcedureRepository $redisRepository;

	public function __construct()
    {
        $this->repository = new MySqlProcedureRepository();
		$this->redisRepository = new RedisProcedureRepository();
    }

    public function getOneById(int $id): null|Procedure
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'id' => $id,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getOneById($id);
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $this->repository->getOneById($id);
    }

    public function getAllByIds(array $ids): Collection
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'id' => $ids,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getAllByIds($ids);
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $this->repository->getAllByIds($ids);
    }

    public function create(Procedure $procedure): Procedure
    {
        $this->redisRepository->clear();

        return $this->repository->create($procedure);
    }

    public function update(Procedure $procedure): int
    {
        $this->redisRepository->clear();

        return $this->repository->update($procedure);
    }
}
