<?php

namespace App\Models\Repositories\Procedure;

use App\Models\Entities\Procedure;
use Illuminate\Support\Collection;

interface IProcedureRepository
{
    public function getOneById(int $id): null|Procedure;

    public function getAllByIds(array $ids): Collection;

    public function create(Procedure $procedure): Procedure;

    public function update(Procedure $procedure): int;
}
