<?php

namespace App\Models\Repositories\Procedure;

use App\Models\Repositories\Redis\QueryCacheStrategy;
use App\Models\Repositories\RedisRepository;

class RedisProcedureRepository extends RedisRepository
{
    use QueryCacheStrategy;

    public function __construct()
    {
        $this->cacheTag = 'procedures';
        parent::__construct();
    }
}
