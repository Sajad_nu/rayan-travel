<?php

namespace App\Models\Repositories\Location;

use App\Models\Repositories\Redis\QueryCacheStrategy;
use App\Models\Repositories\RedisRepository;

class RedisLocationRepository extends RedisRepository
{
    use QueryCacheStrategy;

    public function __construct()
    {
        $this->cacheTag = 'locations';
        parent::__construct();
    }
}
