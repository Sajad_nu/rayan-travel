<?php

namespace App\Models\Repositories\Location;

use App\Models\Entities\Location;
use App\Models\Factories\LocationFactory;
use App\Models\Repositories\MySqlRepository;
use Illuminate\Support\Collection;

class MySqlLocationRepository extends MySqlRepository implements ILocationRepository
{
    public function __construct()
    {
        $this->table = 'locations';
        $this->primaryKey = 'id';
        $this->softDelete = true;
    }


    public function getAll(bool $withTrashed = false): Collection
    {
        $query = $this->newQuery();
        if ($withTrashed === false) {
            $query->whereNull('deleted_at');
        }

        $locations = $query->get();

        return (new LocationFactory())->makeFromCollection($locations);
    }

    public function getOneById(int $id): null|Location
    {
        $location = $this->newQuery()
            ->where('id', $id)
            ->first();

        return $location ? (new LocationFactory())->make($location) : null;
    }

    public function getAllByIds(array $ids): Collection
    {
        $location = $this->newQuery()
            ->whereIn('id', $ids)
            ->get();

        return (new LocationFactory())->makeFromCollection($location);
    }

    public function create(Location $location): Location
    {
        $location->setCreatedAt(date('Y-m-d H:i:s'));
        $location->setUpdatedAt(date('Y-m-d H:i:s'));

        $id = $this->newQuery()
            ->insertGetId([
                'name' => $location->getName(),
                'name_en' => $location->getNameEn(),
                'uri' => $location->getUri(),
                'created_at' => $location->getCreatedAt(),
                'updated_at' => $location->getUpdatedAt(),
            ]);

        $location->setId($id);

        return $location;
    }

    public function update(Location $location): int
    {
        $location->setUpdatedAt(date('Y-m-d H:i:s'));

        return $this->newQuery()
            ->where($this->primaryKey, $location->getPrimaryKey())
            ->update([
                'name' => $location->getName(),
                'name_en' => $location->getNameEn(),
                'uri' => $location->getUri(),
                'updated_at' => $location->getUpdatedAt(),
            ]);
    }

    public function remove(Location $location): int
    {
        return $this->newQuery()
            ->where($this->primaryKey, $location->getPrimaryKey())
            ->update([
                'deleted_at' => date('Y-m-d H:i:s'),
            ]);
    }

    public function restore(Location $location): int
    {
        return $this->newQuery()
            ->where($this->primaryKey, $location->getPrimaryKey())
            ->update([
                'deleted_at' => null,
            ]);
    }
}
