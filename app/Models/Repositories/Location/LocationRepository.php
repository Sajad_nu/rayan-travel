<?php

namespace App\Models\Repositories\Location;

use App\Models\Entities\Location;
use Illuminate\Support\Collection;

class LocationRepository implements ILocationRepository
{
    private ILocationRepository $repository;
    private RedisLocationRepository $redisRepository;

    public function __construct()
    {
        $this->repository = new MySqlLocationRepository();
        $this->redisRepository = new RedisLocationRepository();
    }

    public function getOneById(int $id): null|Location
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'id' => $id,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getOneById($id);
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $this->repository->getOneById($id);
    }

    public function getAllByIds(array $ids): Collection
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'id' => $ids,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getAllByIds($ids);
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $this->repository->getAllByIds($ids);
    }

    public function getAll(bool $withTrashed = false): Collection
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'with_trashed' => $withTrashed,
        ]);

        $entities = $this->redisRepository->get($cacheKey);

        if (is_null($entities)) {
            $entities = $this->repository->getAll($withTrashed);
            $this->redisRepository->put($cacheKey, $entities);
        }

        return $entities;
    }

    public function create(Location $location): Location
    {
        $this->redisRepository->clear();

        return $this->repository->create($location);
    }

    public function update(Location $location): int
    {
        $this->redisRepository->clear();

        return $this->repository->update($location);
    }

    public function remove(Location $location): int
    {
        $this->redisRepository->clear();

        return $this->repository->remove($location);
    }

    public function restore(Location $location): int
    {
        $this->redisRepository->clear();

        return $this->repository->restore($location);
    }
}
