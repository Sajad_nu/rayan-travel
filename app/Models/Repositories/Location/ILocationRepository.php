<?php

namespace App\Models\Repositories\Location;

use App\Models\Entities\Location;
use Illuminate\Support\Collection;

interface ILocationRepository
{
    public function getOneById(int $id): null|Location;

    public function getAllByIds(array $ids): Collection;

    public function getAll(bool $withTrashed): Collection;

    public function create(Location $location): Location;

    public function update(Location $location): int;

    public function remove(Location $location): int;

    public function restore(Location $location): int;
}
