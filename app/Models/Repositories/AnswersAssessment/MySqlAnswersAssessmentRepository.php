<?php

namespace App\Models\Repositories\AnswersAssessment;

use App\Models\Entities\AnswersAssessment;
use App\Models\Factories\AnswersAssessmentFactory;
use App\Models\Repositories\MySqlRepository;
use Illuminate\Support\Collection;

class MySqlAnswersAssessmentRepository extends MySqlRepository implements IAnswersAssessmentRepository
{
    public function __construct()
    {
        $this->table = 'answers_assessment';
        $this->primaryKey = 'id';
        $this->softDelete = false;
    }

    public function getOneById(int $id): null|AnswersAssessment
    {
        $answersAssessment = $this->newQuery()
            ->where('id', $id)
            ->first();

        return $answersAssessment ? (new AnswersAssessmentFactory())->make($answersAssessment) : null;
    }

    public function getUserNameByUserId(int $userId): null|AnswersAssessment
    {
        $answersAssessment = $this->newQuery()
            ->where('user_assessment_id', $userId)
            ->where('param', 'name')
            ->first();

        return $answersAssessment ? (new AnswersAssessmentFactory())->make($answersAssessment) : null;
    }

    public function getUserNameByUserIds(array $userIds): Collection
    {
        $answersAssessments = $this->newQuery()
            ->whereIn('user_assessment_id', $userIds)
            ->where('param', 'name')
            ->get();

        return (new AnswersAssessmentFactory())->makeFromCollection($answersAssessments);
    }

    public function getAllByIds(array $ids): Collection
    {
        $answersAssessment = $this->newQuery()
            ->whereIn('id', $ids)
            ->get();

        return (new AnswersAssessmentFactory())->makeFromCollection($answersAssessment);
    }

    public function getAllByUserAssessmentId(int $userAssessmentId): Collection
    {
        $answersAssessment = $this->newQuery()
            ->where('user_assessment_id', $userAssessmentId)
            ->get();

        return (new AnswersAssessmentFactory())->makeFromCollection($answersAssessment);
    }

    public function create(AnswersAssessment $answersAssessment): AnswersAssessment
    {
        $answersAssessment->setCreatedAt(date('Y-m-d H:i:s'));
        $answersAssessment->setUpdatedAt(date('Y-m-d H:i:s'));

        $id = $this->newQuery()
            ->insertGetId([
                'question_id' => $answersAssessment->getQuestionId(),
                'user_assessment_id' => $answersAssessment->getUserAssessmentId(),
                'param' => $answersAssessment->getParam(),
                'item' => $answersAssessment->getItem(),
                'description' => $answersAssessment->getDescription(),
                'created_at' => $answersAssessment->getCreatedAt(),
                'updated_at' => $answersAssessment->getUpdatedAt(),
            ]);

        $answersAssessment->setId($id);

        return $answersAssessment;
    }

    public function insert(array $answers): bool
    {
        return $this->newQuery()->insert($answers);
    }


    public function update(AnswersAssessment $answersAssessment): int
    {
        $answersAssessment->setUpdatedAt(date('Y-m-d H:i:s'));

        return $this->newQuery()
            ->where($this->primaryKey, $answersAssessment->getPrimaryKey())
            ->update([
                'question_id' => $answersAssessment->getQuestionId(),
                'param' => $answersAssessment->getParam(),
                'item' => $answersAssessment->getItem(),
                'description' => $answersAssessment->getDescription(),
                'updated_at' => $answersAssessment->getUpdatedAt(),
            ]);
    }
}
