<?php

namespace App\Models\Repositories\AnswersAssessment;

use App\Models\Entities\AnswersAssessment;
use Illuminate\Support\Collection;

interface IAnswersAssessmentRepository
{
    public function getOneById(int $id): null|AnswersAssessment;

    public function getUserNameByUserId(int $userId): null|AnswersAssessment;

    public function getUserNameByUserIds(array $userIds): Collection;

    public function getAllByIds(array $ids): Collection;

    public function getAllByUserAssessmentId(int $userAssessmentId): Collection;

    public function insert(array $answers): bool;

    public function create(AnswersAssessment $answersAssessment): AnswersAssessment;

    public function update(AnswersAssessment $answersAssessment): int;
}
