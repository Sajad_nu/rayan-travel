<?php

namespace App\Models\Repositories\AnswersAssessment;

use App\Models\Repositories\Redis\QueryCacheStrategy;
use App\Models\Repositories\RedisRepository;

class RedisAnswersAssessmentRepository extends RedisRepository
{
    use QueryCacheStrategy;

    public function __construct()
    {
        $this->cacheTag = 'answers_assessment';
        parent::__construct();
    }
}
