<?php

namespace App\Models\Repositories\AnswersAssessment;

use App\Models\Entities\AnswersAssessment;
use Illuminate\Support\Collection;

class AnswersAssessmentRepository implements IAnswersAssessmentRepository
{
    private IAnswersAssessmentRepository $repository;
    private RedisAnswersAssessmentRepository $redisRepository;

    public function __construct()
    {
        $this->repository = new MySqlAnswersAssessmentRepository();
        $this->redisRepository = new RedisAnswersAssessmentRepository();
    }

    public function getOneById(int $id): null|AnswersAssessment
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => 'getOneById',
            'id' => $id,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getOneById($id);
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $this->repository->getOneById($id);
    }

    public function getUserNameByUserId(int $userId): null|AnswersAssessment
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => 'getUserNameByUserId',
            'user_id' => $userId,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getUserNameByUserId($userId);
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $entity;
    }

    public function getUserNameByUserIds(array $userIds): Collection
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => 'getUserNameByUserIds',
            'user_ids' => $userIds,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getUserNameByUserIds($userIds);
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $entity;
    }

    public function getAllByIds(array $ids): Collection
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => 'getAllByIds',
            'id' => $ids,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getAllByIds($ids);
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $this->repository->getAllByIds($ids);
    }

    public function getAllByUserAssessmentId(int $userAssessmentId): Collection
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => 'getAllByUserAssessmentId',
            'userAssessmentId' => $userAssessmentId,
        ]);

        $entities = $this->redisRepository->get($cacheKey);

        if (is_null($entities)) {
            $entities = $this->repository->getAllByUserAssessmentId($userAssessmentId);
            $this->redisRepository->put($cacheKey, $entities);
        }

        return $entities;
    }

    public function insert(array $answers): bool
    {
        $result = $this->repository->insert($answers);

        if ($result) {
            $this->redisRepository->clear();
        }
        return $result;
    }

    public function create(AnswersAssessment $answersAssessment): AnswersAssessment
    {
        $this->redisRepository->clear();

        return $this->repository->create($answersAssessment);
    }

    public function update(AnswersAssessment $answersAssessment): int
    {
        $this->redisRepository->clear();

        return $this->repository->update($answersAssessment);
    }
}
