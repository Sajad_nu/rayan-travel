<?php

namespace App\Models\Repositories\AdviceTime;

use App\Models\Entities\AdviceTime;
use Illuminate\Support\Collection;

class AdviceTimeRepository implements IAdviceTimeRepository
{
    private IAdviceTimeRepository $repository;
    private RedisAdviceTimeRepository $redisRepository;

    public function __construct()
    {
        $this->repository = new MySqlAdviceTimeRepository();
        $this->redisRepository = new RedisAdviceTimeRepository();
    }

    public function getOneById(int $id): null|AdviceTime
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'id' => $id,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getOneById($id);
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $entity;
    }

    public function getAllByIds(array $ids): Collection
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'id' => $ids,
        ]);

        $entities = $this->redisRepository->get($cacheKey);

        if (is_null($entities)) {
            $entities = $this->repository->getAllByIds($ids);
            $this->redisRepository->put($cacheKey, $entities);
        }

        return $entities;
    }

    public function create(AdviceTime $adviceTime): AdviceTime
    {
        $this->redisRepository->clear();

        return $this->repository->create($adviceTime);
    }

    public function update(AdviceTime $adviceTime): int
    {
        $this->redisRepository->clear();

        return $this->repository->update($adviceTime);
    }
}
