<?php

namespace App\Models\Repositories\AdviceTime;

use App\Models\Repositories\Redis\QueryCacheStrategy;
use App\Models\Repositories\RedisRepository;

class RedisAdviceTimeRepository extends RedisRepository
{
    use QueryCacheStrategy;

    public function __construct()
    {
        $this->cacheTag = 'advice_times';
        parent::__construct();
    }
}
