<?php

namespace App\Models\Repositories\AdviceTime;

use App\Models\Entities\AdviceTime;
use Illuminate\Support\Collection;

interface IAdviceTimeRepository
{
    public function getOneById(int $id): null|AdviceTime;

    public function getAllByIds(array $ids): Collection;

    public function create(AdviceTime $adviceTime): AdviceTime;

    public function update(AdviceTime $adviceTime): int;

}