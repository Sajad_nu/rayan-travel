<?php

namespace App\Models\Repositories\AdviceTime;

use App\Models\Entities\AdviceTime;
use App\Models\Factories\AdviceTimeFactory;
use App\Models\Repositories\MySqlRepository;
use Illuminate\Support\Collection;

class MySqlAdviceTimeRepository extends MySqlRepository implements IAdviceTimeRepository
{
    private AdviceTimeFactory $factory;

    public function __construct()
    {
        $this->table = 'advice_times';
        $this->primaryKey = 'id';
        $this->softDelete = false;
        $this->factory = new AdviceTimeFactory();
    }

    public function getOneById(int $id): null|AdviceTime
    {
        $adviceTime = $this->newQuery()
            ->where('id', $id)
            ->first();

        return $adviceTime ? $this->factory->make($adviceTime) : null;
    }

    public function getAllByIds(array $ids): Collection
    {
        $adviceTime = $this->newQuery()
            ->whereIn('id', $ids)
            ->get();

        return $this->factory->makeFromCollection($adviceTime);
    }

    public function create(AdviceTime $adviceTime): AdviceTime
    {
        $adviceTime->setCreatedAt(date('Y-m-d H:i:s'));
        $adviceTime->setUpdatedAt(date('Y-m-d H:i:s'));

        $id = $this->newQuery()
            ->insertGetId([
                'datetime' => $adviceTime->getDatetime(),
                'is_reserved' => $adviceTime->getIsReserved(),
                'disabled' => $adviceTime->getDisabled(),
                'created_at' => $adviceTime->getCreatedAt(),
                'updated_at' => $adviceTime->getUpdatedAt(),
            ]);

        $adviceTime->setId($id);

        return $adviceTime;
    }

    public function update(AdviceTime $adviceTime): int
    {
        $adviceTime->setUpdatedAt(date('Y-m-d H:i:s'));

        return $this->newQuery()
            ->where($this->primaryKey, $adviceTime->getPrimaryKey())
            ->update([
                'datetime' => $adviceTime->getDatetime(),
                'is_reserved' => $adviceTime->getIsReserved(),
                'disabled' => $adviceTime->getDisabled(),
                'updated_at' => $adviceTime->getUpdatedAt(),
            ]);
    }
}
