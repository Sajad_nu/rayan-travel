<?php

namespace App\Models\Repositories;

use App\Models\Entities\Entity;
use App\Models\General\Polygon;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Str;

abstract class  MySqlRepository
{
    protected string $primaryKey = 'id';
    /**
     * @var Builder $alternativeDbConnection
     */
    protected $table = '';
    protected bool $softDelete = false;


    /**
     * @return Builder
     */
    public function newQuery(): Builder
    {
        return app('db')->table($this->table);
    }

    public function exists($columnValue, $columnName = null)
    {
        if (is_null($columnName)) {
            $columnName = $this->primaryKey;
        }
        return $this->newQuery()->where($columnName, $columnValue)->exists();
    }

    /**
     * @param int $offset
     * @param int $count
     * @param int|null $total
     * @param array $orders
     * @param array $filters
     * @return Builder
     */
    public function processGridViewQuery(Builder $query, int|null &$total, int $offset = 0, int $count = 0, array $orders = [], array $filters = []): Builder
    {
        if ($orders) {
            $query = $this->processOrder($query, $orders);
        }

        if ($filters) {
            $query = $this->processFilter($query, $filters);
        }

        $total = $query->count();

        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }
        return $query;
    }

    /**
     * @param Entity $model
     */
    public function updateOrCreate($model)
    {
        if ($this->exists($model->getPrimaryKey())) {
            $this->update($model);
        } else {
            $this->create($model);
        }
    }

    /**
     * @param Entity $model
     */
    public function createIfNotExists($model)
    {
        if (!$this->exists($model->getPrimaryKey())) {
            $this->create($model);
        }
    }

    /**
     * It returns maximum row Id
     * @return int|mixed
     */
    public function getMaxId()
    {
        $entity = $this->newQuery()->orderByDesc($this->primaryKey)->first();
        if ($entity) {
            return $entity->id;
        } else {
            return 0;
        }
    }

    /**
     * @param int|null $from
     * @param int|null $to
     * @return int
     */
    public function getCount($from = null, $to = null)
    {
        $query = $this->newQuery();

        if ($this->softDelete) {
            $query->whereNull('deleted_at');
        }

        if (!is_null($from) && !is_null($to)) {
            $query->whereBetween('created_at', [$from, $to]);
        } else if (!is_null($from) && is_null($to)) {
            $query->where('created_at', '>', $from);
        } else if (is_null($from) && !is_null($to)) {
            $query->where('created_at', '<', $to);
        }

        return $query->count();
    }

    /**
     * this is for validation purpose look at AppServiceProvider
     * @param array $value
     * @param mixed $ignoredPrimaryKey
     * @return bool
     */
    public function valueExists($value, $ignoredPrimaryKey = null)
    {
        $query = $this->newQuery();

        if ($this->softDelete) {
            $query->whereNull('deleted_at');
        }

        $query->where($value);

        if (isset($value[$ignoredPrimaryKey])) {
            $query->where($this->primaryKey, '<>', $value[$ignoredPrimaryKey]);
        }

        return $query->exists();
    }

    /**
     * this is for validation purpose look at AppServiceProvider
     * @param $attribute
     * @param $value
     * @return bool
     */
    public function foreignKeyExist($attribute, $value)
    {
        $query = $this->newQuery()->whereNull('deleted_at');
        $ids = $query->whereIn('id', $value)->pluck('id')->toArray();
        if (count(array_diff($value, $ids)) == 0) {
            return true;
        }
        return false;
    }

    protected function processOrder(&$query, $orders, $columnsMapper = [])
    {
        foreach ($orders as $order) {
            $order = is_array($order) ? $order : get_object_vars($order);
            $name = $order['name'];
            if (isset($columnsMapper[$order['name']])) {
                $name = $columnsMapper[$order['name']];
            }
            $query->orderBy($name, $order['type']);
        }
        return $query;
    }

    /**
     * @param Builder $query
     * @param array $filters
     * @return Builder
     */
    protected function processFilter($query, $filters, $columnsMapper = [])
    {
        foreach ($filters as $filter) {
            $name = $filter->name;
            if (isset($columnsMapper[$filter->name])) {
                $name = $columnsMapper[$filter->name];
            }

            switch (strtolower(Str::snake($filter->operator))) {
                case FilterOperator::IS_NULL:
                    $query->whereNull($name);
                    break;
                case FilterOperator::IS_NOT_NULL:
                    $query->whereNotNull($name);
                    break;
                case FilterOperator::IS_EQUAL_TO:
                    if (is_string($filter->operand1) && str_contains($filter->operand1, '|')) {
                        // create in functionality with equal string
                        $arr = array_filter(explode('|', $filter->operand1));
                        $query->whereIn($name, $arr);
                    } else {
                        $query->where($name, '=', $filter->operand1);
                    }
                    break;
                case FilterOperator::IS_NOT_EQUAL_TO:
                    if (is_string($filter->operand1) && str_contains($filter->operand1, '|')) {
                        // create in functionality with equal string
                        $arr = array_filter(explode('|', $filter->operand1));
                        $query->whereNotIn($name, $arr);
                    } else {
                        $query->where($name, '<>', $filter->operand1);
                    }
                    break;
                case FilterOperator::START_WITH:
                    $query->where($name, 'LIKE', $filter->operand1 . '%');
                    break;
                case FilterOperator::DOES_NOT_CONTAINS:
                    $query->where($name, 'NOT LIKE', '%' . $filter->operand1 . '%');
                    break;
                case FilterOperator::CONTAINS:
                    $query->where($name, 'LIKE', '%' . $filter->operand1 . '%');
                    break;
                case FilterOperator::ENDS_WITH:
                    $query->where($name, 'LIKE', '%' . $filter->operand1);
                    break;
                case FilterOperator::IN:
                    $query->whereIn($name, $filter->operand1);
                    break;
                case FilterOperator::NOT_IN:
                    $query->whereNotIn($name, $filter->operand1);
                    break;
                case FilterOperator::BETWEEN:
                    $query->whereBetween($name, array($filter->operand1, $filter->operand2));
                    break;
                case FilterOperator::IS_AFTER_THAN_OR_EQUAL_TO:
                case FilterOperator::IS_GREATER_THAN_OR_EQUAL_TO:
                    $query->where($name, '>=', $filter->operand1);
                    break;
                case FilterOperator::IS_AFTER_THAN:
                case FilterOperator::IS_GREATER_THAN:
                    $query->where($name, '>', $filter->operand1);
                    break;
                case FilterOperator::IS_LESS_THAN_OR_EQUAL_TO:
                case FilterOperator::IS_BEFORE_THAN_OR_EQUAL_TO:
                    $query->where($name, '<=', $filter->operand1);
                    break;
                case FilterOperator::IS_LESS_THAN:
                case FilterOperator::IS_BEFORE_THAN:
                    $query->where($name, '<', $filter->operand1);
                    break;
                case FilterOperator::IS_INSIDE_POLYGON:
                    /** @var Polygon $polygon */
                    $polygon = $filter->operand1;
                    $query->whereRaw("Contains(GeomFromText('{$polygon->toRaw()}'),{$name})");
                    break;
            }
        }

        return $query;
    }
}
