<?php

namespace App\Models\Repositories\Document;

use App\Models\Entities\Document;
use Illuminate\Support\Collection;

class DocumentRepository implements IDocumentRepository
{
    private IDocumentRepository $repository;
	private RedisDocumentRepository $redisRepository;

	public function __construct()
    {
        $this->repository = new MySqlDocumentRepository();
		$this->redisRepository = new RedisDocumentRepository();
    }

    public function getOneById(int $id): null|Document
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'id' => $id,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getOneById($id);
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $entity;
    }

    public function getAllByIds(array $ids): Collection
    {
        $cacheKey = $this->redisRepository->makeKey([
            'function_name' => __FUNCTION__,
            'id' => $ids,
        ]);

        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->repository->getAllByIds($ids);
            $this->redisRepository->put($cacheKey, $entity);
        }
        return $entity;
    }

    public function create(Document $document): Document
    {
        $this->redisRepository->clear();

        return $this->repository->create($document);
    }

    public function update(Document $document): int
    {
        $this->redisRepository->clear();

        return $this->repository->update($document);
    }
}
