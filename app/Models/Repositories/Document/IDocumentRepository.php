<?php

namespace App\Models\Repositories\Document;

use App\Models\Entities\Document;
use Illuminate\Support\Collection;

interface IDocumentRepository
{
    public function getOneById(int $id): null|Document;

    public function getAllByIds(array $ids): Collection;

    public function create(Document $document): Document;

    public function update(Document $document): int;
}
