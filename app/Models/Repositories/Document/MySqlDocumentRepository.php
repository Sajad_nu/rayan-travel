<?php

namespace App\Models\Repositories\Document;

use App\Models\Entities\Document;
use App\Models\Factories\DocumentFactory;
use App\Models\Repositories\MySqlRepository;
use Illuminate\Support\Collection;

class MySqlDocumentRepository extends MySqlRepository implements IDocumentRepository
{
     public function __construct()
    {
        $this->table = 'documents';
        $this->primaryKey = 'id';
        $this->softDelete = false;
    }

    public function getOneById(int $id): null|Document
    {
        $document = $this->newQuery()
            ->where('id', $id)
            ->first();

        return $document ? (new DocumentFactory())->make($document) : null;
    }

    public function getAllByIds(array $ids): Collection
    {
        $document = $this->newQuery()
            ->whereIn('id', $ids)
            ->get();

        return (new DocumentFactory())->makeFromCollection($document);
    }

    public function create(Document $document): Document
    {
    	$document->setCreatedAt(date('Y-m-d H:i:s'));
		$document->setUpdatedAt(date('Y-m-d H:i:s'));

        $id = $this->newQuery()
            ->insertGetId([
                'title' => $document->getTitle(),
				'title_en' => $document->getTitleEn(),
				'disabled' => $document->getDisabled(),
				'created_at' => $document->getCreatedAt(),
				'updated_at' => $document->getUpdatedAt(),
            ]);

        $document->setId($id);

        return $document;
    }

    public function update(Document $document): int
    {
    	$document->setUpdatedAt(date('Y-m-d H:i:s'));

        return $this->newQuery()
           ->where($this->primaryKey, $document->getPrimaryKey())
            ->update([
                'title' => $document->getTitle(),
				'title_en' => $document->getTitleEn(),
				'disabled' => $document->getDisabled(),
				'updated_at' => $document->getUpdatedAt(),
            ]);
    }
}
