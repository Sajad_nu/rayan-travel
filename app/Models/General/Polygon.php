<?php

namespace App\Models\General;

class Polygon
{
    /**
     * @var Point[]
     */
    public $points;

    public function __construct()
    {
        $this->points = [];
    }

    /**
     * @param \stdClass $stdObj
     * @return $this
     */
    public function parseStdObject($stdObj): Polygon
    {
        $this->points = [];
        foreach ($stdObj->points as $point) {
            $this->points[] = (new Point())->parseStdObject($point);
        }
        return $this;
    }

    /**
     * @param Point $point
     */
    public function addPoint(Point $point)
    {
        $this->points [] = $point;
    }

    /**
     * @param array $points
     */
    public function setPoints(array $points)
    {
        $this->points = $points;
    }

    /**
     * @return string
     */
    public function toRaw(): string
    {
        $string = '';
        foreach ($this->points as $index => $point) {
            $string .= ($index > 0 ? ',' : '') . $point->toLatLongString(' ');
        }
        $string .= ' , ' . $this->points[0]->toLatLongString(' ');
        return 'POLYGON((' . $string . '))';
    }

    /**
     * @param Point $points
     * @return $this
     */
    public function parseBinary(Point $points): Polygon
    {
        $this->points = [];
        foreach ($points as $point) {
            $this->points[] = (new Point())->parseBinary($point);
        }
        return $this;
    }
}
