<?php

namespace App\Models\General;

use Illuminate\Support\Facades\DB;

class Point
{
    public $latitude;
    public $longitude;

    /**
     * Point constructor.
     * @param $latitude
     * @param $longitude
     */
    public function __construct($latitude = null, $longitude = null)
    {
        $this->latitude = $latitude;
        $this->longitude = $longitude;
    }

    public function parse($point)
    {
        if ($point) {
            $location = str_replace('point', '', strtolower((string) $point));
            $location = ltrim($location, '(');
            $location = rtrim($location, ')');
            $location = explode(' ', $location);

            $this->latitude = isset($location[0]) ? floatval($location[0]) : null;
            $this->longitude = isset($location[1]) ? floatval($location[1]) : null;
        }
        return $this;
    }

    public function parseBinary($point)
    {
        $coordinates = unpack('x/x/x/x/corder/Ltype/dlat/dlon', (string) $point);
        $this->latitude = isset($coordinates['lat']) ? $coordinates['lat'] : null;
        $this->longitude = isset($coordinates['lon']) ? $coordinates['lon'] : null;
        return $this;
    }

    public function parseStdObject($stdObj)
    {
        $this->latitude = $stdObj->latitude;
        $this->longitude = $stdObj->longitude;
        return $this;
    }

    public function toArray()
    {
        return [
            'latitude' => $this->latitude,
            'longitude' => $this->longitude
        ];
    }

    public function toJson()
    {
        return json_encode($this->toArray());
    }

    public function isEmpty()
    {
        return (empty($this->latitude) || empty($this->longitude));
    }

    public function getSqlFromText()
    {
        return DB::raw("ST_GeomFromText('POINT(" . $this->latitude . " " . $this->longitude . ")')");
    }

    public function __toString()
    {
        if ($this->isEmpty()) {
            return '';
        }
        return "POINT({$this->latitude} {$this->longitude})";
    }

    /**
     * @param string $separator
     * @return string
     */
    public function toLatLongString(string $separator = ' , ')
    {
        return $this->latitude . $separator . $this->longitude;
    }

    public function jsonSerialize()
    {
        return $this->toArray();
    }
}
