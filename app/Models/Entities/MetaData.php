<?php

namespace App\Models\Entities;

class MetaData extends Entity
{
    protected int $id;

	protected string $uri;

	protected null|string $title;

	protected null|string $description;

	protected null|string $locale;

	protected null|string $canonicalUrl;

	protected null|string $index;

	protected null|string $robots;

	protected null|string $type;

	protected null|string $author;

	protected null|string $keywords;

	protected null|string $image;

	protected bool $disabled = false;

	protected null|string $publishedTime;

	protected null|string $modifiedTime;

	protected string $createdAt;

	protected string $updatedAt;

	protected null|string $deletedAt;

	public function getId(): int
    {
        return $this->id;
    }

	public function setId(int $id): void
    {
        $this->id = $id;
    }

	public function getUri(): string
    {
        return $this->uri;
    }

	public function setUri(string $uri): void
    {
        $this->uri = $uri;
    }

	public function getTitle(): null|string
    {
        return $this->title;
    }

	public function setTitle(null|string $title): void
    {
        $this->title = $title;
    }

	public function getDescription(): null|string
    {
        return $this->description;
    }

	public function setDescription(null|string $description): void
    {
        $this->description = $description;
    }

	public function getLocale(): null|string
    {
        return $this->locale;
    }

	public function setLocale(null|string $locale): void
    {
        $this->locale = $locale;
    }

	public function getCanonicalUrl(): null|string
    {
        return $this->canonicalUrl;
    }

	public function setCanonicalUrl(null|string $canonicalUrl): void
    {
        $this->canonicalUrl = $canonicalUrl;
    }

	public function getIndex(): null|string
    {
        return $this->index;
    }

	public function setIndex(null|string $index): void
    {
        $this->index = $index;
    }

	public function getRobots(): null|string
    {
        return $this->robots;
    }

	public function setRobots(null|string $robots): void
    {
        $this->robots = $robots;
    }

	public function getType(): null|string
    {
        return $this->type;
    }

	public function setType(null|string $type): void
    {
        $this->type = $type;
    }

	public function getAuthor(): null|string
    {
        return $this->author;
    }

	public function setAuthor(null|string $author): void
    {
        $this->author = $author;
    }

	public function getKeywords(): null|string
    {
        return $this->keywords;
    }

	public function setKeywords(null|string $keywords): void
    {
        $this->keywords = $keywords;
    }

	public function getImage(): null|string
    {
        return $this->image;
    }

	public function setImage(null|string $image): void
    {
        $this->image = $image;
    }

	public function getDisabled(): bool
    {
        return $this->disabled;
    }

	public function setDisabled(bool $disabled): void
    {
        $this->disabled = $disabled;
    }

	public function getPublishedTime(): null|string
    {
        return $this->publishedTime;
    }

	public function setPublishedTime(null|string $publishedTime): void
    {
        $this->publishedTime = $publishedTime;
    }

	public function getModifiedTime(): null|string
    {
        return $this->modifiedTime;
    }

	public function setModifiedTime(null|string $modifiedTime): void
    {
        $this->modifiedTime = $modifiedTime;
    }

	public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

	public function setCreatedAt(string $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

	public function getUpdatedAt(): string
    {
        return $this->updatedAt;
    }

	public function setUpdatedAt(string $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

	public function getDeletedAt(): null|string
    {
        return $this->deletedAt;
    }

	public function setDeletedAt(null|string $deletedAt): void
    {
        $this->deletedAt = $deletedAt;
    }
}
