<?php

namespace App\Models\Entities;

class RequestsAdvice extends Entity
{
    protected int $id;

    protected null|int $adviceTimeId = null;

    protected string $phoneNumber;

    protected null|string $fullName = null;

    protected string $type;

    protected string $trackingCode;

    protected null|string $sendToExpertAt = null;

    protected string $createdAt;

    protected string $updatedAt;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getAdviceTimeId(): null|int
    {
        return $this->adviceTimeId;
    }

    public function setAdviceTimeId(null|int $adviceTimeId): void
    {
        $this->adviceTimeId = $adviceTimeId;
    }

    public function getPhoneNumber(): string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }

    public function getFullName(): null|string
    {
        return $this->fullName;
    }

    public function setFullName(null|string $fullName): void
    {
        $this->fullName = $fullName;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getTrackingCode(): string
    {
        return $this->trackingCode;
    }

    /**
     * @param string $trackingCode
     */
    public function setTrackingCode(string $trackingCode): void
    {
        $this->trackingCode = $trackingCode;
    }


    public function getSendToExpertAt(): null|string
    {
        return $this->sendToExpertAt;
    }

    public function setSendToExpertAt(null|string $sendToExpertAt): void
    {
        $this->sendToExpertAt = $sendToExpertAt;
    }

    public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

    public function setCreatedAt(string $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getUpdatedAt(): string
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(string $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }
}
