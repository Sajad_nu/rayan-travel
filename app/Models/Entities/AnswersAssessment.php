<?php

namespace App\Models\Entities;

class AnswersAssessment extends Entity
{
    protected int $id;

    protected int $userAssessmentId;

    protected int $questionId;

	protected string $param;

	protected null|string $item;

	protected null|string $description;

	protected string $createdAt;

	protected string $updatedAt;

	public function getId(): int
    {
        return $this->id;
    }

	public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getUserAssessmentId(): int
    {
        return $this->userAssessmentId;
    }

    /**
     * @param int $userAssessmentId
     */
    public function setUserAssessmentId(int $userAssessmentId): void
    {
        $this->userAssessmentId = $userAssessmentId;
    }

	public function getQuestionId(): int
    {
        return $this->questionId;
    }

	public function setQuestionId(int $questionId): void
    {
        $this->questionId = $questionId;
    }

	public function getParam(): string
    {
        return $this->param;
    }

	public function setParam(string $param): void
    {
        $this->param = $param;
    }

	public function getItem(): null|string
    {
        return $this->item;
    }

	public function setItem(null|string $item): void
    {
        $this->item = $item;
    }

	public function getDescription(): null|string
    {
        return $this->description;
    }

	public function setDescription(null|string $description): void
    {
        $this->description = $description;
    }

	public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

	public function setCreatedAt(string $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

	public function getUpdatedAt(): string
    {
        return $this->updatedAt;
    }

	public function setUpdatedAt(string $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }
}
