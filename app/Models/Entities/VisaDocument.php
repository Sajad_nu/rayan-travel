<?php

namespace App\Models\Entities;

class VisaDocument extends Entity
{
    protected int $id;

	protected int $visaId;

	protected int $documentId;

	protected string $title;

	protected null|string $description;

	protected string $createdAt;

	protected string $updatedAt;

	protected null|string $deletedAt;

	public function getId(): int
    {
        return $this->id;
    }

	public function setId(int $id): void
    {
        $this->id = $id;
    }

	public function getVisaId(): int
    {
        return $this->visaId;
    }

	public function setVisaId(int $visaId): void
    {
        $this->visaId = $visaId;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

	public function getDocumentId(): int
    {
        return $this->documentId;
    }

	public function setDocumentId(int $documentId): void
    {
        $this->documentId = $documentId;
    }

	public function getDescription(): null|string
    {
        return $this->description;
    }

	public function setDescription(null|string $description): void
    {
        $this->description = $description;
    }

	public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

	public function setCreatedAt(string $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

	public function getUpdatedAt(): string
    {
        return $this->updatedAt;
    }

	public function setUpdatedAt(string $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

	public function getDeletedAt(): null|string
    {
        return $this->deletedAt;
    }

	public function setDeletedAt(null|string $deletedAt): void
    {
        $this->deletedAt = $deletedAt;
    }
}
