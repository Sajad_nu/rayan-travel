<?php

namespace App\Models\Entities;

class AssessmentCategory extends Entity
{
    protected int $id;

	protected string $title;

	protected int $priority;

	protected string $createdAt;

	protected string $updatedAt;

	public function getId(): int
    {
        return $this->id;
    }

	public function setId(int $id): void
    {
        $this->id = $id;
    }

	public function getTitle(): string
    {
        return $this->title;
    }

	public function setTitle(string $title): void
    {
        $this->title = $title;
    }

	public function getPriority(): int
    {
        return $this->priority;
    }

	public function setPriority(int $priority): void
    {
        $this->priority = $priority;
    }

	public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

	public function setCreatedAt(string $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

	public function getUpdatedAt(): string
    {
        return $this->updatedAt;
    }

	public function setUpdatedAt(string $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }
}
