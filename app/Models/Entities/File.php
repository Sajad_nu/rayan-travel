<?php

namespace App\Models\Entities;

class File extends Entity
{
    protected int $id;

	protected null|int $categoryId;

	protected null|int $visaId;

	protected string $name;

	protected null|string $description;

	protected null|string $hash;

	protected null|string $type;

	protected null|string $path;

	protected int $priority = 0;

	protected bool $published = false;

	protected string $createdAt;

	protected string $updatedAt;

	protected null|string $deletedAt;

	public function getId(): int
    {
        return $this->id;
    }

	public function setId(int $id): void
    {
        $this->id = $id;
    }

	public function getCategoryId(): null|int
    {
        return $this->categoryId;
    }

	public function setCategoryId(null|int $categoryId): void
    {
        $this->categoryId = $categoryId;
    }

	public function getVisaId(): null|int
    {
        return $this->visaId;
    }

	public function setVisaId(null|int $visaId): void
    {
        $this->visaId = $visaId;
    }

	public function getName(): string
    {
        return $this->name;
    }

	public function setName(string $name): void
    {
        $this->name = $name;
    }

	public function getDescription(): null|string
    {
        return $this->description;
    }

	public function setDescription(null|string $description): void
    {
        $this->description = $description;
    }

	public function getHash(): null|string
    {
        return $this->hash;
    }

	public function setHash(null|string $hash): void
    {
        $this->hash = $hash;
    }

	public function getType(): null|string
    {
        return $this->type;
    }

	public function setType(null|string $type): void
    {
        $this->type = $type;
    }

	public function getPath(): null|string
    {
        return $this->path;
    }

	public function setPath(null|string $path): void
    {
        $this->path = $path;
    }

	public function getPriority(): int
    {
        return $this->priority;
    }

	public function setPriority(int $priority): void
    {
        $this->priority = $priority;
    }

	public function getPublished(): bool
    {
        return $this->published;
    }

	public function setPublished(bool $published): void
    {
        $this->published = $published;
    }

	public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

	public function setCreatedAt(string $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

	public function getUpdatedAt(): string
    {
        return $this->updatedAt;
    }

	public function setUpdatedAt(string $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

	public function getDeletedAt(): null|string
    {
        return $this->deletedAt;
    }

	public function setDeletedAt(null|string $deletedAt): void
    {
        $this->deletedAt = $deletedAt;
    }
}
