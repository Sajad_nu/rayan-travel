<?php

namespace App\Models\Entities;

class Task extends Entity
{
    protected int $id;

    protected null|string $fullName = null;

    protected null|string $phoneNumber = null;

    protected string $title;

    protected null|string $description = null;

    protected null|string $result = null;

    protected null|string $assignedAt = null;

    protected null|string $doneAt = null;

    protected null|string $sendToExpertAt = null;

    protected string $createdAt;

    protected string $updatedAt;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getFullName(): null|string
    {
        return $this->fullName;
    }

    public function setFullName(null|string $fullName): void
    {
        $this->fullName = $fullName;
    }

    public function getPhoneNumber(): null|string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(null|string $phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getDescription(): null|string
    {
        return $this->description;
    }

    public function setDescription(null|string $description): void
    {
        $this->description = $description;
    }

    public function getResult(): null|string
    {
        return $this->result;
    }

    public function setResult(null|string $result): void
    {
        $this->result = $result;
    }

    public function getAssignedAt(): null|string
    {
        return $this->assignedAt;
    }

    public function setAssignedAt(null|string $assignedAt): void
    {
        $this->assignedAt = $assignedAt;
    }

    public function getDoneAt(): null|string
    {
        return $this->doneAt;
    }

    public function setDoneAt(null|string $doneAt): void
    {
        $this->doneAt = $doneAt;
    }

    /**
     * @return string|null
     */
    public function getSendToExpertAt(): ?string
    {
        return $this->sendToExpertAt;
    }

    /**
     * @param string|null $sendToExpertAt
     */
    public function setSendToExpertAt(?string $sendToExpertAt): void
    {
        $this->sendToExpertAt = $sendToExpertAt;
    }

    public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

    public function setCreatedAt(string $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getUpdatedAt(): string
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(string $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }
}
