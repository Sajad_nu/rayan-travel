<?php

namespace App\Models\Entities;

class UsersAssessment extends Entity
{
    protected int $id;

    protected string $hashId;

    protected string $trackingCode;

    protected string $userName;

    protected null|string $countryCode = null;

    protected null|string $phoneNumber = null;

    protected null|string $sendResultTo = null;

    protected null|string $rank = null;

    protected null|string $rankAt = null;

    protected null|string $sendAt = null;

    protected null|string $sendResultAt = null;

    protected null|string $sendConfirmAt = null;

    protected string $createdAt;

    protected string $updatedAt;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getHashId(): string
    {
        return $this->hashId;
    }

    /**
     * @param string $hashId
     */
    public function setHashId(string $hashId): void
    {
        $this->hashId = $hashId;
    }

    /**
     * @return string
     */
    public function getTrackingCode(): string
    {
        return $this->trackingCode;
    }

    /**
     * @param string $trackingCode
     */
    public function setTrackingCode(string $trackingCode): void
    {
        $this->trackingCode = $trackingCode;
    }

    /**
     * @return string
     */
    public function getUserName(): string
    {
        return $this->userName;
    }

    /**
     * @param string $userName
     */
    public function setUserName(string $userName): void
    {
        $this->userName = $userName;
    }

    /**
     * @return string|null
     */
    public function getCountryCode(): ?string
    {
        return $this->countryCode;
    }

    /**
     * @param string|null $countryCode
     */
    public function setCountryCode(?string $countryCode): void
    {
        $this->countryCode = $countryCode;
    }

    /**
     * @return string|null
     */
    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    /**
     * @param string|null $phoneNumber
     */
    public function setPhoneNumber(?string $phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return string|null
     */
    public function getSendResultTo(): ?string
    {
        return $this->sendResultTo;
    }

    /**
     * @param string|null $sendResultTo
     */
    public function setSendResultTo(?string $sendResultTo): void
    {
        $this->sendResultTo = $sendResultTo;
    }

    /**
     * @return string|null
     */
    public function getRank(): ?string
    {
        return $this->rank;
    }

    /**
     * @param string|null $rank
     */
    public function setRank(?string $rank): void
    {
        $this->rank = $rank;
    }

    /**
     * @return string|null
     */
    public function getRankAt(): ?string
    {
        return $this->rankAt;
    }

    /**
     * @param string|null $rankAt
     */
    public function setRankAt(?string $rankAt): void
    {
        $this->rankAt = $rankAt;
    }

    /**
     * @return string|null
     */
    public function getSendAt(): ?string
    {
        return $this->sendAt;
    }

    /**
     * @param string|null $sendAt
     */
    public function setSendAt(?string $sendAt): void
    {
        $this->sendAt = $sendAt;
    }

    /**
     * @return string|null
     */
    public function getSendResultAt(): ?string
    {
        return $this->sendResultAt;
    }

    /**
     * @param string|null $sendResultAt
     */
    public function setSendResultAt(?string $sendResultAt): void
    {
        $this->sendResultAt = $sendResultAt;
    }

    /**
     * @return string|null
     */
    public function getSendConfirmAt(): ?string
    {
        return $this->sendConfirmAt;
    }

    /**
     * @param string|null $sendConfirmAt
     */
    public function setSendConfirmAt(?string $sendConfirmAt): void
    {
        $this->sendConfirmAt = $sendConfirmAt;
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

    /**
     * @param string $createdAt
     */
    public function setCreatedAt(string $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return string
     */
    public function getUpdatedAt(): string
    {
        return $this->updatedAt;
    }

    /**
     * @param string $updatedAt
     */
    public function setUpdatedAt(string $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }
}
