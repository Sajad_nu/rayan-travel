<?php

namespace App\Models\Entities;

class ExtraContent extends Entity
{
    protected int $id;

	protected string $uri;

	protected string $type;

	protected string $title;

	protected string $description;

	protected int $priority = 0;

	protected bool $published = false;

	protected string $createdAt;

	protected string $updatedAt;

	public function getId(): int
    {
        return $this->id;
    }

	public function setId(int $id): void
    {
        $this->id = $id;
    }

	public function getUri(): string
    {
        return $this->uri;
    }

	public function setUri(string $uri): void
    {
        $this->uri = $uri;
    }

	public function getType(): string
    {
        return $this->type;
    }

	public function setType(string $type): void
    {
        $this->type = $type;
    }

	public function getTitle(): string
    {
        return $this->title;
    }

	public function setTitle(string $title): void
    {
        $this->title = $title;
    }

	public function getDescription(): string
    {
        return $this->description;
    }

	public function setDescription(string $description): void
    {
        $this->description = $description;
    }

	public function getPriority(): int
    {
        return $this->priority;
    }

	public function setPriority(int $priority): void
    {
        $this->priority = $priority;
    }

	public function getPublished(): bool
    {
        return $this->published;
    }

	public function setPublished(bool $published): void
    {
        $this->published = $published;
    }

	public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

	public function setCreatedAt(string $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

	public function getUpdatedAt(): string
    {
        return $this->updatedAt;
    }

	public function setUpdatedAt(string $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }
}
