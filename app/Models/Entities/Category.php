<?php

namespace App\Models\Entities;

class Category extends Entity
{
    protected int $id;

	protected string $title;

	protected null|string $type;

	protected null|string $uri;

	protected null|string $description;

	protected null|string $fileUri;

	protected bool $disabled = false;

	protected string $createdAt;

	protected string $updatedAt;

	protected null|string $deletedAt;

	public function getId(): int
    {
        return $this->id;
    }

	public function setId(int $id): void
    {
        $this->id = $id;
    }

	public function getTitle(): string
    {
        return $this->title;
    }

	public function setTitle(string $title): void
    {
        $this->title = $title;
    }

	public function getType(): null|string
    {
        return $this->type;
    }

	public function setType(null|string $type): void
    {
        $this->type = $type;
    }

	public function getUri(): null|string
    {
        return $this->uri;
    }

	public function setUri(null|string $uri): void
    {
        $this->uri = $uri;
    }

    /**
     * @return string|null
     */
    public function getFileUri(): ?string
    {
        return $this->fileUri;
    }

    /**
     * @param string|null $fileUri
     */
    public function setFileUri(?string $fileUri): void
    {
        $this->fileUri = $fileUri;
    }

	public function getDescription(): null|string
    {
        return $this->description;
    }

	public function setDescription(null|string $description): void
    {
        $this->description = $description;
    }

	public function getDisabled(): bool
    {
        return $this->disabled;
    }

	public function setDisabled(bool $disabled): void
    {
        $this->disabled = $disabled;
    }

	public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

	public function setCreatedAt(string $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

	public function getUpdatedAt(): string
    {
        return $this->updatedAt;
    }

	public function setUpdatedAt(string $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

	public function getDeletedAt(): null|string
    {
        return $this->deletedAt;
    }

	public function setDeletedAt(null|string $deletedAt): void
    {
        $this->deletedAt = $deletedAt;
    }
}
