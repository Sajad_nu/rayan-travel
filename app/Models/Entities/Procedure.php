<?php

namespace App\Models\Entities;

class Procedure extends Entity
{
    protected int $id;

	protected string $title;

	protected string $titleEn;

	protected bool $disabled;

	protected int $priority = 0;

	protected string $createdAt;

	protected string $updatedAt;

	public function getId(): int
    {
        return $this->id;
    }

	public function setId(int $id): void
    {
        $this->id = $id;
    }

	public function getTitle(): string
    {
        return $this->title;
    }

	public function setTitle(string $title): void
    {
        $this->title = $title;
    }

	public function getTitleEn(): string
    {
        return $this->titleEn;
    }

	public function setTitleEn(string $titleEn): void
    {
        $this->titleEn = $titleEn;
    }

	public function getDisabled(): bool
    {
        return $this->disabled;
    }

	public function setDisabled(bool $disabled): void
    {
        $this->disabled = $disabled;
    }

	public function getPriority(): int
    {
        return $this->priority;
    }

	public function setPriority(int $priority): void
    {
        $this->priority = $priority;
    }

	public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

	public function setCreatedAt(string $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

	public function getUpdatedAt(): string
    {
        return $this->updatedAt;
    }

	public function setUpdatedAt(string $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }
}
