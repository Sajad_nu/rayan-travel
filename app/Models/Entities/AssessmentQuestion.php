<?php

namespace App\Models\Entities;

class AssessmentQuestion extends Entity
{
    protected int $id;

    protected int $visaId;

    protected int $categoryId;

    protected null|int $parentId;

    protected null|string $title;

    protected null|string $parentParams;

    protected null|object $params;

    protected null|string $inputType;

    protected int $priority;

    protected string $createdAt;

    protected string $updatedAt;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getVisaId(): int
    {
        return $this->visaId;
    }

    public function setVisaId(int $visaId): void
    {
        $this->visaId = $visaId;
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return $this->categoryId;
    }

    /**
     * @param int $categoryId
     */
    public function setCategoryId(int $categoryId): void
    {
        $this->categoryId = $categoryId;
    }

    public function getParentId(): null|int
    {
        return $this->parentId;
    }

    public function setParentId(null|int $parentId): void
    {
        $this->parentId = $parentId;
    }

    public function getTitle(): null|string
    {
        return $this->title;
    }

    public function setTitle(null|string $title): void
    {
        $this->title = $title;
    }

    public function getParentParams(): null|string
    {
        return $this->parentParams;
    }

    public function setParentParams(null|string $parentParams): void
    {
        $this->parentParams = $parentParams;
    }

    /**
     * @return object|null
     */
    public function getParams(): ?object
    {
        return $this->params;
    }

    /**
     * @return string|null
     */
    public function getInputType(): ?string
    {
        return $this->inputType;
    }

    /**
     * @param string|null $inputType
     */
    public function setInputType(?string $inputType): void
    {
        $this->inputType = $inputType;
    }

    /**
     * @param object|null $params
     */
    public function setParams(?object $params): void
    {
        $this->params = $params;
    }

    public function getPriority(): int
    {
        return $this->priority;
    }

    public function setPriority(int $priority): void
    {
        $this->priority = $priority;
    }

    public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

    public function setCreatedAt(string $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getUpdatedAt(): string
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(string $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }
}
