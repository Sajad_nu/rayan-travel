<?php

namespace App\Models\Entities;

class Visa extends Entity
{
    protected int $id;

	protected int $locationId;

	protected int $categoryId;

	protected string $title;

	protected null|string $uri;

	protected null|string $fileUri;

	protected int $priority;

	protected null|string $description;

	protected null|string $documents;

	protected string $fee;

	protected null|string $procedures;

	protected string $createdAt;

	protected string $updatedAt;

	public function getId(): int
    {
        return $this->id;
    }

	public function setId(int $id): void
    {
        $this->id = $id;
    }

	public function getLocationId(): int
    {
        return $this->locationId;
    }

	public function setLocationId(int $locationId): void
    {
        $this->locationId = $locationId;
    }

	public function getCategoryId(): int
    {
        return $this->categoryId;
    }

	public function setCategoryId(int $categoryId): void
    {
        $this->categoryId = $categoryId;
    }

	public function getTitle(): string
    {
        return $this->title;
    }

	public function setTitle(string $title): void
    {
        $this->title = $title;
    }

	public function getUri(): null|string
    {
        return $this->uri;
    }

	public function setUri(null|string $uri): void
    {
        $this->uri = $uri;
    }

    /**
     * @return string|null
     */
    public function getFileUri(): ?string
    {
        return $this->fileUri;
    }

    /**
     * @param string|null $fileUri
     */
    public function setFileUri(?string $fileUri): void
    {
        $this->fileUri = $fileUri;
    }

	public function getPriority(): int
    {
        return $this->priority;
    }

	public function setPriority(int $priority): void
    {
        $this->priority = $priority;
    }

	public function getDescription(): null|string
    {
        return $this->description;
    }

	public function setDescription(null|string $description): void
    {
        $this->description = $description;
    }

	public function getDocuments(): null|string
    {
        return $this->documents;
    }

	public function setDocuments(null|string $documents): void
    {
        $this->documents = $documents;
    }

	public function getFee(): string
    {
        return $this->fee;
    }

	public function setFee(string $fee): void
    {
        $this->fee = $fee;
    }

	public function getProcedures(): null|string
    {
        return $this->procedures;
    }

	public function setProcedures(null|string $procedures): void
    {
        $this->procedures = $procedures;
    }

	public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

	public function setCreatedAt(string $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

	public function getUpdatedAt(): string
    {
        return $this->updatedAt;
    }

	public function setUpdatedAt(string $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }
}
