<?php

namespace App\Models\Entities;

use App\Models\General\Point;
use Illuminate\Support\Str;

abstract class Entity implements \JsonSerializable
{
    private array $originals = [];

    /**
     * @return int
     */
    public abstract function getId();

    public function __construct()
    {

    }

    public function __set($name, $value)
    {
        if (property_exists($this, $name)) {
            $function = Str::camel('set_' . Str::snake($name));
            $this->$function($value);
        }
    }

    public function __get($name)
    {
        if (property_exists($this, $name)) {
            $function = Str::camel('get_' . Str::snake($name));
            return $this->$function();
        }
    }

    public function __isset($name)
    {
        return property_exists($this, $name);
    }

    /**
     * Make all variables of the object as null
     * @return $this
     */
    public function clearVariables()
    {
        $attributes = get_object_vars($this);
        foreach ($attributes as $attributeName => $attributeValue) {
            $this->$attributeName = null;
        }
        return $this;
    }

    /**
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Fill the model
     */
    public function fill()
    {

    }

    /**
     * get an Array of current Attributes value
     */
    public function toArray()
    {
        return get_object_vars($this);
    }

    /**
     * store an array of attributes original value
     */
    public function storeOriginals()
    {
        $this->originals = $this->toArray();
    }

    /**
     * @description Get an Array of Changed Attributes
     * @return array
     */
    public function changedAttributesName(): array
    {
        $changedAttributes = [];
        $attributes = $this->toArray();
        foreach ($attributes as $key => $value) {
            if (array_key_exists($key, $this->originals)) {
                if ($value !== $this->originals[$key] && !((is_array($this->originals[$key]) || is_object($this->originals[$key])))) {
                    $changedAttributes[] = $key;
                }

                if ($value instanceof Point) {
                    foreach ($value as $index => $item) {

                        if ($item != $this->originals[$key]->$index) {
                            $changedAttributes[] = $key;
                        }
                    }
                }

            }
        }
        return $changedAttributes;
    }

    /**
     * get an Array of Changed Attributes with new values
     * @return array
     */
    public function getDirty()
    {
        $dirty = [];
        $attributes = $this->toArray();

        foreach ($this->changedAttributesName() as $key) {
            $dirty[$key] = $attributes[$key];
        }

        return $dirty;
    }

    /**
     * get an Array of Changed Attributes with original values
     * @return array
     */
    public function getChanges()
    {
        $changes = [];

        foreach ($this->changedAttributesName() as $key) {
            $changes[$key] = $this->originals[$key];
        }

        return $changes;
    }

    /**
     * @description Is any attribute changed?
     * @param null $key
     * @return bool
     */
    public function isDirty($key = null): bool
    {
        $changedKeys = $this->changedAttributesName();
        return count($changedKeys) > 0 && ((!$key || in_array($key, $changedKeys)));
    }

    public function jsonSerialize()
    {
        return $this->toArray();
    }
}
