<?php

namespace App\Models\Factories;

use App\Models\Entities\VisaProcedure;
use stdClass;

class VisaProcedureFactory extends Factory
{
    public function make(stdClass $entity): VisaProcedure
    {
        $visaProcedure = new VisaProcedure();

        $visaProcedure->setId($entity->id);
        $visaProcedure->setVisaId($entity->visa_id);
        $visaProcedure->setProceduresId($entity->procedures_id);
        $visaProcedure->setDescription($entity->description);
        $visaProcedure->setCreatedAt($entity->created_at);
        $visaProcedure->setUpdatedAt($entity->updated_at);
        $visaProcedure->setDeletedAt($entity->deleted_at);

        return $visaProcedure;
    }
}
