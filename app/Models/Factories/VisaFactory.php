<?php

namespace App\Models\Factories;

use App\Models\Entities\Visa;
use stdClass;

class VisaFactory extends Factory
{
    public function make(stdClass $entity): Visa
    {
        $visa = new Visa();

        $visa->setId($entity->id);
        $visa->setLocationId($entity->location_id);
        $visa->setCategoryId($entity->category_id);
        $visa->setTitle($entity->title);
        $visa->setUri($entity->uri);
        $visa->setPriority($entity->priority);
        $visa->setDescription($entity->description);
        $visa->setDocuments($entity->documents);
        $visa->setFee($entity->fee);
        $visa->setProcedures($entity->procedures);
        $visa->setCreatedAt($entity->created_at);
        $visa->setUpdatedAt($entity->updated_at);

        return $visa;
    }
}
