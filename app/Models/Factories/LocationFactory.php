<?php

namespace App\Models\Factories;

use App\Models\Entities\Location;
use stdClass;

class LocationFactory extends Factory
{
    public function make(stdClass $entity): Location
    {
        $location = new Location();

        $location->setId($entity->id);
        $location->setName($entity->name);
        $location->setNameEn($entity->name_en);
        $location->setUri($entity->uri);
        $location->setCreatedAt($entity->created_at);
        $location->setUpdatedAt($entity->updated_at);
        $location->setDeletedAt($entity->deleted_at);

        return $location;
    }
}
