<?php

namespace App\Models\Factories;

use App\Models\Entities\AssessmentQuestion;
use stdClass;

class AssessmentQuestionFactory extends Factory
{
    public function make(stdClass $entity): AssessmentQuestion
    {
        $assessmentQuestion = new AssessmentQuestion();

        $assessmentQuestion->setId($entity->id);
        $assessmentQuestion->setVisaId($entity->visa_id);
        $assessmentQuestion->setCategoryId($entity->category_id);
        $assessmentQuestion->setParentId($entity->parent_id);
        $assessmentQuestion->setTitle($entity->title);
        $assessmentQuestion->setParentParams($entity->parent_params);
        $assessmentQuestion->setParams(json_decode($entity->params));
        $assessmentQuestion->setInputType($entity->input_type);
        $assessmentQuestion->setPriority($entity->priority);
        $assessmentQuestion->setCreatedAt($entity->created_at);
        $assessmentQuestion->setUpdatedAt($entity->updated_at);

        return $assessmentQuestion;
    }
}
