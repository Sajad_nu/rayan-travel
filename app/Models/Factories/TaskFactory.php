<?php

namespace App\Models\Factories;

use App\Models\Entities\Task;
use stdClass;

class TaskFactory extends Factory
{
    public function make(stdClass $entity): Task
    {
        $task = new Task();

        $task->setId($entity->id);
		$task->setFullName($entity->full_name);
		$task->setPhoneNumber($entity->phone_number);
		$task->setTitle($entity->title);
		$task->setDescription($entity->description);
		$task->setResult($entity->result);
		$task->setAssignedAt($entity->assigned_at);
		$task->setDoneAt($entity->done_at);
		$task->setSendToExpertAt($entity->send_to_expert_at);
		$task->setCreatedAt($entity->created_at);
		$task->setUpdatedAt($entity->updated_at);

        return $task;
    }
}
