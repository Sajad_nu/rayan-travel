<?php

namespace App\Models\Factories;

use App\Models\Entities\UsersAssessment;
use stdClass;

class UsersAssessmentFactory extends Factory
{
    public function make(stdClass $entity): UsersAssessment
    {
        $usersAssessment = new UsersAssessment();

        $usersAssessment->setId($entity->id);
        $usersAssessment->setHashId($entity->hash_id);
        $usersAssessment->setTrackingCode($entity->tracking_code);
        $usersAssessment->setCountryCode($entity->country_code);
        $usersAssessment->setPhoneNumber($entity->phone_number);
        $usersAssessment->setSendResultTo($entity->send_result_to);
        $usersAssessment->setRank($entity->rank);
        $usersAssessment->setRankAt($entity->rank_at);
        $usersAssessment->setSendAt($entity->send_at);
        $usersAssessment->setSendResultAt($entity->send_result_at);
        $usersAssessment->setSendConfirmAt($entity->send_confirm_at);
        $usersAssessment->setCreatedAt($entity->created_at);
        $usersAssessment->setUpdatedAt($entity->updated_at);

        return $usersAssessment;
    }
}
