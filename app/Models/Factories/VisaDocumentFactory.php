<?php

namespace App\Models\Factories;

use App\Models\Entities\VisaDocument;
use stdClass;

class VisaDocumentFactory extends Factory
{
    public function make(stdClass $entity): VisaDocument
    {
        $visaDocument = new VisaDocument();

        $visaDocument->setId($entity->id);
        $visaDocument->setVisaId($entity->visa_id);
        $visaDocument->setDocumentId($entity->document_id);
        $visaDocument->setDescription($entity->description);
        $visaDocument->setCreatedAt($entity->created_at);
        $visaDocument->setUpdatedAt($entity->updated_at);
        $visaDocument->setDeletedAt($entity->deleted_at);

        return $visaDocument;
    }
}
