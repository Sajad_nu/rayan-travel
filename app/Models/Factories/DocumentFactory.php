<?php

namespace App\Models\Factories;

use App\Models\Entities\Document;
use stdClass;

class DocumentFactory extends Factory
{
     public function make(stdClass $entity): Document
    {
        $document = new Document();

        $document->setId($entity->id);
		$document->setTitle($entity->title);
		$document->setTitleEn($entity->title_en);
		$document->setDisabled($entity->disabled);
		$document->setCreatedAt($entity->created_at);
		$document->setUpdatedAt($entity->updated_at);

        return $document;
    }
}
