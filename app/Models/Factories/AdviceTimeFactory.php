<?php

namespace App\Models\Factories;

use App\Models\Entities\AdviceTime;
use stdClass;

class AdviceTimeFactory extends Factory
{
    public function make(stdClass $entity): AdviceTime
    {
        $adviceTime = new AdviceTime();

        $adviceTime->setId($entity->id);
        $adviceTime->setDatetime($entity->datetime);
        $adviceTime->setIsReserved($entity->is_reserved);
        $adviceTime->setDisabled($entity->disabled);
        $adviceTime->setCreatedAt($entity->created_at);
        $adviceTime->setUpdatedAt($entity->updated_at);

        return $adviceTime;
    }
}
