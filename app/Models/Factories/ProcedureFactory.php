<?php

namespace App\Models\Factories;

use App\Models\Entities\Procedure;
use stdClass;

class ProcedureFactory extends Factory
{
    public function make(stdClass $entity): Procedure
    {
        $procedure = new Procedure();

        $procedure->setId($entity->id);
        $procedure->setTitle($entity->title);
        $procedure->setTitleEn($entity->title_en);
        $procedure->setDisabled($entity->disabled);
        $procedure->setPriority($entity->priority);
        $procedure->setCreatedAt($entity->created_at);
        $procedure->setUpdatedAt($entity->updated_at);

        return $procedure;
    }
}
