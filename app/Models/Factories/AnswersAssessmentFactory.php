<?php

namespace App\Models\Factories;

use App\Models\Entities\AnswersAssessment;
use stdClass;

class AnswersAssessmentFactory extends Factory
{
    public function make(stdClass $entity): AnswersAssessment
    {
        $answersAssessment = new AnswersAssessment();

        $answersAssessment->setId($entity->id);
        $answersAssessment->setQuestionId($entity->question_id);
        $answersAssessment->setUserAssessmentId($entity->user_assessment_id);
        $answersAssessment->setParam($entity->param);
        $answersAssessment->setItem($entity->item);
        $answersAssessment->setDescription($entity->description);
        $answersAssessment->setCreatedAt($entity->created_at);
        $answersAssessment->setUpdatedAt($entity->updated_at);

        return $answersAssessment;
    }
}
