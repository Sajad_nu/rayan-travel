<?php

namespace App\Models\Factories;

use App\Models\Entities\AssessmentCategory;
use stdClass;

class AssessmentCategoryFactory extends Factory
{
    public function make(stdClass $entity): AssessmentCategory
    {
        $assessmentCategory = new AssessmentCategory();

        $assessmentCategory->setId($entity->id);
        $assessmentCategory->setTitle($entity->title);
        $assessmentCategory->setPriority($entity->priority);
        $assessmentCategory->setCreatedAt($entity->created_at);
        $assessmentCategory->setUpdatedAt($entity->updated_at);

        return $assessmentCategory;
    }
}
