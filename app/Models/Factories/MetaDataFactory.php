<?php

namespace App\Models\Factories;

use App\Models\Entities\MetaData;
use stdClass;

class MetaDataFactory extends Factory
{
    public function make(stdClass $entity): MetaData
    {
        $metaData = new MetaData();

        $metaData->setId($entity->id);
        $metaData->setUri($entity->uri);
        $metaData->setTitle($entity->title);
        $metaData->setDescription($entity->description);
        $metaData->setLocale($entity->locale);
        $metaData->setCanonicalUrl($entity->canonical_url);
        $metaData->setIndex($entity->index);
        $metaData->setRobots($entity->robots);
        $metaData->setType($entity->type);
        $metaData->setAuthor($entity->author);
        $metaData->setKeywords($entity->keywords);
        $metaData->setImage($entity->image);
        $metaData->setDisabled($entity->disabled);
        $metaData->setPublishedTime($entity->published_time);
        $metaData->setModifiedTime($entity->modified_time);
        $metaData->setCreatedAt($entity->created_at);
        $metaData->setUpdatedAt($entity->updated_at);
        $metaData->setDeletedAt($entity->deleted_at);

        return $metaData;
    }
}
