<?php

namespace App\Models\Factories;

use App\Models\Entities\File;
use stdClass;

class FileFactory extends Factory
{
    public function make(stdClass $entity): File
    {
        $file = new File();

        $file->setId($entity->id);
        $file->setCategoryId($entity->category_id);
        $file->setVisaId($entity->visa_id);
        $file->setName($entity->name);
        $file->setDescription($entity->description);
        $file->setHash($entity->hash);
        $file->setType($entity->type);
        $file->setPath($entity->path);
        $file->setPriority($entity->priority);
        $file->setPublished($entity->published);
        $file->setCreatedAt($entity->created_at);
        $file->setUpdatedAt($entity->updated_at);
        $file->setDeletedAt($entity->deleted_at);

        return $file;
    }
}
