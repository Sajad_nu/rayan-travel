<?php

namespace App\Models\Factories;

use App\Models\Entities\ExtraContent;
use stdClass;

class ExtraContentFactory extends Factory
{
    public function make(stdClass $entity): ExtraContent
    {
        $extraContent = new ExtraContent();

        $extraContent->setId($entity->id);
        $extraContent->setUri($entity->uri);
        $extraContent->setType($entity->type);
        $extraContent->setTitle($entity->title);
        $extraContent->setDescription($entity->description);
        $extraContent->setPriority($entity->priority);
        $extraContent->setPublished($entity->published);
        $extraContent->setCreatedAt($entity->created_at);
        $extraContent->setUpdatedAt($entity->updated_at);

        return $extraContent;
    }
}
