<?php

namespace App\Models\Factories;

use App\Models\Entities\RequestsAdvice;
use stdClass;

class RequestsAdviceFactory extends Factory
{
    public function make(stdClass $entity): RequestsAdvice
    {
        $requestsAdvice = new RequestsAdvice();

        $requestsAdvice->setId($entity->id);
        $requestsAdvice->setAdviceTimeId($entity->advice_time_id);
        $requestsAdvice->setPhoneNumber($entity->phone_number);
        $requestsAdvice->setFullName($entity->full_name);
        $requestsAdvice->setType($entity->type);
        $requestsAdvice->setTrackingCode($entity->tracking_code);
        $requestsAdvice->setSendToExpertAt($entity->send_to_expert_at);
        $requestsAdvice->setCreatedAt($entity->created_at);
        $requestsAdvice->setUpdatedAt($entity->updated_at);

        return $requestsAdvice;
    }
}
