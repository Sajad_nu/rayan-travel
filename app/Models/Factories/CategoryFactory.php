<?php

namespace App\Models\Factories;

use App\Models\Entities\Category;
use stdClass;

class CategoryFactory extends Factory
{
     public function make(stdClass $entity): Category
    {
        $category = new Category();

        $category->setId($entity->id);
		$category->setTitle($entity->title);
		$category->setType($entity->type);
		$category->setUri($entity->uri);
		$category->setDescription($entity->description);
		$category->setDisabled($entity->disabled);
		$category->setCreatedAt($entity->created_at);
		$category->setUpdatedAt($entity->updated_at);
		$category->setDeletedAt($entity->deleted_at);

        return $category;
    }
}
