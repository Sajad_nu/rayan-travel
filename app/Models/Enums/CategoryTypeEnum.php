<?php

namespace App\Models\Enums;

enum CategoryTypeEnum: string
{
    case PILGRIMAGE = 'pilgrimage';

    case DIPLOMATIC = 'diplomatic';

    case TOURIST = 'tourist';

    case EDUCATIONAL = 'educational';

    case BUSINESS = 'business';

    case TRANSIT = 'transit';

    case MEDIA = 'media';

    case INVESTMENT = 'investment';

    case MEDICAL = 'medical';

    case MARRIAGE = 'marriage';

    case ON_ARRIVAL = 'on_arrival';

    case IMMIGRANT = 'immigrant';

    case SPORT = 'sport';
}
