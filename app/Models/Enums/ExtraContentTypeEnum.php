<?php

namespace App\Models\Enums;

enum ExtraContentTypeEnum : string
{
    case FAQ = 'faq';

	case CONTENT = 'content';
}
