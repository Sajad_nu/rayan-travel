<?php

namespace App\Models\Enums;

enum WeSenderApiHttpStatus: int
{
    case SUCCESS = 0;
    case SERVER_ERROR = -1;
    case USER_NOT_FOUND = -2;
    case SENDER_IS_EMPTY = -3;
    case KEY_IS_EMPTY = -4;
    case RECEIVER_IS_EMPTY = -5;
    case RECEIVER_IS_NULL = -6;
    case BODY_IS_EMPTY = -7;
    case METHOD_NOT_ALLOWED = -8;
    case PLAN_IS_EXPIRED = -9;
    case FORMAT_TYPE_IS_INVALID = -10;
    case FILE_TYPE_IS_INVALID = -11;
    case DISCONNECTION = -15;
    case TOO_MANY_NUMBERS = -19;
    case TOO_MANY_MESSAGES = -20;

}
