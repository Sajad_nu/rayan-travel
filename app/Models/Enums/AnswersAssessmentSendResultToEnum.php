<?php

namespace App\Models\Enums;

enum AnswersAssessmentSendResultToEnum
{
    public const SMS = 'sms';

	public const WHATSAPP = 'whatsapp';
}
