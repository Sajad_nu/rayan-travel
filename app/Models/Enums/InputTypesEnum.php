<?php

namespace App\Models\Enums;

enum InputTypesEnum: string
{
    case CHECKBOX = 'checkbox';

    case RADIO = 'radio';

    case TEXT = 'text';

    case DATE = 'date';

    case NUMBER = 'number';
}
