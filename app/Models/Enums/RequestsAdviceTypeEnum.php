<?php

namespace App\Models\Enums;

enum RequestsAdviceTypeEnum: string
{
    use EnumOperationsTrait;

    case IN_PERSON = 'in_person';

    case ONLINE = 'online';

    case PHONE = 'phone';
}
