<?php

namespace App\Models\Enums;

enum UsersAssessmentSendResultToEnum: string
{
    case SMS = 'sms';

	case WHATSAPP = 'whatsapp';
}
