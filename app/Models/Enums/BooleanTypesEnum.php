<?php

namespace App\Models\Enums;

enum BooleanTypesEnum: string
{
    case TRUE = 'true';

    case FALSE = 'false';

}
