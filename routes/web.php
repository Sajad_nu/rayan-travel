<?php

use App\Http\Controllers\AboutUsController;
use App\Http\Controllers\Api\AnswersAssessmentController;
use App\Http\Controllers\Api\AssessmentCategoryController;
use App\Http\Controllers\Api\AssessmentQuestionController;
use App\Http\Controllers\Api\UserAssessmentController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ContactUsController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RequestAdviceController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\VisaController;
use Illuminate\Support\Facades\Route;

Route::get('/', [HomeController::class, 'index']);

Route::get('/{category}Visa', [CategoryController::class, 'index']);

Route::get('/{category}Visa/{country}', [VisaController::class, 'index']);

Route::post('visa-search', [SearchController::class, 'index'])->name('visa-search');

Route::get('/contact-us', [ContactUsController::class, 'index']);

Route::get('/about-us', [AboutUsController::class, 'index']);

Route::get('/received-message/{hashId}', function () {
    return redirect('/');
})->where(['hashId' => '(.+)']);

Route::post('/request-advice', [RequestAdviceController::class, 'store']);

Route::post('/new-task', [TaskController::class, 'store']);

Route::prefix('assessment')->group(function () {

    Route::view('/', 'web.assessment.start-assessment');

    Route::get('/categories', [AssessmentCategoryController::class, 'getList']);
    Route::get('/questions', [AssessmentQuestionController::class, 'listByVisaId']);


    Route::post('/store', [AnswersAssessmentController::class, 'store']);
    Route::get('/set-rank', [UserAssessmentController::class, 'setRank']);
    Route::post('/user-call-info', [UserAssessmentController::class, 'userCallInfo']);
    Route::get('/record-feedback', [UserAssessmentController::class, 'setUserFeedback']);
});





