<?php

return [

    'user-assessment-rank' => [
        'poor' => 'ضعیف',
        'average' => 'متوسط',
        'good' => 'خوب',
        'excellent' => 'عالی'

    ],

    'advice-type' => [
        'online' => 'آنلاین',
        'phone' => 'تلفنی',
        'in_person' => 'حضوری'
    ]
];
