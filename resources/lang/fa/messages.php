<?php


use App\Models\Enums\WeSenderApiHttpStatus;

return [

    'we-sender-assessment' => [
        WeSenderApiHttpStatus::SUCCESS->value => "ارسال نتیجه ارزیابی آقا / خانم :userName در صف ارسال قرار گرفت\n\nچند ثانیه دیگر با کلیک بر روی این لینک:\n :phoneNumber وارد صفحه چت با کاربر شوید \n\nدرصورتی که پس از چند ثانیه امکان ورود به صفحه چت با کاربر میسر نبود این کاربر شماره اشتباهی را برای اعلام نتیجه وارد نموده است. ",
        WeSenderApiHttpStatus::SERVER_ERROR->value => 'اشکال در سرور لطفا بعدا تلاش کنید.',
        WeSenderApiHttpStatus::USER_NOT_FOUND->value => 'اطلاعات ورودی اکانت شما اشتباه وارد شده به پشتیبان سایت اطلاع دهید.',
        WeSenderApiHttpStatus::SENDER_IS_EMPTY->value => 'شماره اکانت شما اشتباه وارد شده به پشتیبان سایت اطلاع دهید.',
        WeSenderApiHttpStatus::KEY_IS_EMPTY->value => 'کلید دسترسی وب سرویس اشتباه وارد شده به پشتیبان سایت اطلاع دهید.',
        WeSenderApiHttpStatus::RECEIVER_IS_EMPTY->value => 'گیرنده یافت نشد.',
        WeSenderApiHttpStatus::RECEIVER_IS_NULL->value => 'گیرنده یافت نشد.',
        WeSenderApiHttpStatus::BODY_IS_EMPTY->value => 'متن پیام نمیتوند خالی باشد به پشتیبان سایت اطلاع دهید.',
        WeSenderApiHttpStatus::METHOD_NOT_ALLOWED->value => 'متود http اشتباه است به پشتیبان سایت اطلاع دهید.',
        WeSenderApiHttpStatus::PLAN_IS_EXPIRED->value => 'طرح شما پایان یافته است به پشتیبان سایت اطلاع دهید.',
        WeSenderApiHttpStatus::FORMAT_TYPE_IS_INVALID->value => ': فرمت فایل ارسایل پشتیبات ن -10 نیم شود. به پشتیبان سایت اطلاع دهید.',
        WeSenderApiHttpStatus::FILE_TYPE_IS_INVALID->value => ' رشته 64base صحیح نیست.  به پشتیبان سایت اطلاع دهید.',
        WeSenderApiHttpStatus::DISCONNECTION->value => 'واتساپ شما ازسامانه قطع شده است.  به پشتیبان سایت اطلاع دهید.',
        WeSenderApiHttpStatus::TOO_MANY_NUMBERS->value => 'عداد شماره بیش از حد مجاز در یک درخواست',
        WeSenderApiHttpStatus::TOO_MANY_MESSAGES->value => 'عداد پیامها بیش از حد مجاز درصف'
    ]
];
