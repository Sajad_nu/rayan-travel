import {fetchData} from './fetchData';

export const getCategoriesByVisaId = () => {

    const getCategories = fetchData('http://localhost:3000/categories');

    return {getCategories}
}
