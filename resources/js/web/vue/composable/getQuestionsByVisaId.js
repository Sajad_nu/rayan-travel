import {fetchData} from './fetchData';

export const getQuestionsByVisaId = () => {

    const getQuestions = fetchData('http://localhost:8000/questions');

    return {getQuestions}
}
