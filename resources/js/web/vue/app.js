import 'bootstrap/dist/css/bootstrap.rtl.min.css';
import 'bootstrap';
import '../../../scss/main.scss';

import {createApp} from 'vue';
import {createPinia} from 'pinia';
import router from './router/index';
import ReceivedMessage from './components/evaluation-form/ReceivedMessage.vue';
import App from './App.vue';

const pinia = createPinia();
const app = createApp(App);

import VueAwesomePaginate from "vue-awesome-paginate";
import "vue-awesome-paginate/dist/style.css";

import Vue3PersianDatetimePicker from 'vue3-persian-datetime-picker'
app.component('DatePicker', Vue3PersianDatetimePicker)

app.use(pinia)
app.use(router)
app.use(ReceivedMessage)
app.use(VueAwesomePaginate)
app.mount('#app')

