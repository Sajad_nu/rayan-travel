import {createRouter, createWebHistory} from 'vue-router';
import StartEvaluation from '../components/evaluation-form/EvaluationQuestionnaire.vue';
import ReceivedMessage from '../components/evaluation-form/ReceivedMessage.vue';


const router = createRouter({
    history: createWebHistory(),
    routes: [
        {path: '/assessment', name: 'start-evaluation', component: StartEvaluation},
        {path: '/received-message/:hashId', name: 'received-message', component: ReceivedMessage},
    ],
})

export default router;
