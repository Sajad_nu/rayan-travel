<div class="main-content container-fluid row px-0 ms-0" id="mainContent">

    <div class="parent position-relative px-0 d-flex align-items-md-center">

        <video autoplay loop muted playsinline class="rotateVideo w-100" poster="{{url("mp4/bg.png")}}">
            <source src="{{url('mp4/bg.avifs')}}" type="video/avifs">
            <source src="{{url('mp4/bg.webp')}}" type="video/webp">
            <source src="{{url('mp4/bg.mp4')}}" type="video/mp4" class="w-100">
        </video>

        <div class="overlay ">
            <div class="row top">
                <p class="title text-center">رایان ویزا</p>
                <p class="description justify-content-center d-none d-md-flex">دفتر خدمات مسافرتی و جهانگردی رایان سیاحت</p>
            </div>

            <div class="searchBox d-flex justify-content-center row">
                <p class="title">درخواست ویزا</p>

                <form action="{{url('visa-search')}}" method="post">
                    @csrf
                    <div class="row d-flex  justify-content-md-between align-items-center">
                        <div class="col-md-5 px-0">
                            <select class="form-select form-select-lg fs-6 fw-medium" name="categoryId" id="visaType"
                                    aria-label="Default select example">
                                <option selected disabled >نوع ویزا</option>
                                @foreach($categories as $category)
                                    <option class="my-5 " value="{{$category->getId()}}">{{$category->getTitle()}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-md-5 px-0">
                            <select class="form-select form-select-lg fs-6 fw-medium" name="locationId" id="visaCountry"
                                    aria-label="Default select example">
                                <option selected disabled>کشور مقصد</option>

                                @foreach($locations as $location)
                                    <option value="{{$location->getId()}}">{{$location->getName()}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="row col-12 ms-0 p-0 col-md-1 d-flex justify-content-center">
                            <button class="btn" type="submit">جستجو</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>


</div>






