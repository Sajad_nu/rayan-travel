<div class="visasTypesInHomePage container-fluid d-flex row mx-auto">
    <p class="title text-center">انواع ویزا</p>
    <div class="row d-flex justify-content-between">

        @foreach($categories as $category)

            <div class="item tourist">
                <a href="{{url($category->getUri())}}" class="card">
                    <img src="{{asset($category->getFileUri())}}" class="img-fluid card-img" alt="انواع ویزا">

                    <P class="card-img-overlay">ویزای {{$category->getTitle()}}</P>
                </a>
            </div>

        @endforeach
    </div>

</div>

