<div class="successfulVisas container-fluid row ms-0">

    <div class="row successfulVisasNumber">
        <p class="title px-0">تعداد ویزاهای موفق</p>
        <p class="description px-0">اخذ ویزای توریستی، تحصیلی، درمانی و ورزشی برای کشور های آمریکا، کانادا، حوزه شینگن،
            انگلستان و استرالیا</p>
        <div class="row p-0 items">
            <div class="col-5 col-md-2 item me-3">
                <label class="number" data-bs-target="10000">0
                    <img src="{{asset('images/icon-plus.png')}}" class="d-inline"/>
                </label>
                <label class="title">تعداد ویزاهای موفق</label>
            </div>

            <div class="col-5 col-md-2 item me-md-3">
                <label class="number" data-bs-target="16000">
                    0
                    <img src="{{asset('images/icon-plus.png')}}" class="d-inline"/>
                </label>
                <label class="title">تعداد کل متقاضیان </label>
            </div>

            <div class="col-5 col-md-2 item me-3">
                <label class="number" data-bs-target="15">0
                    <img src="{{asset('images/icon-plus.png')}}" class="d-inline"/>
                </label>
                <label class="title">سال های فعالیت </label>
            </div>

            <div class="col-5 col-md-2 item ">
                <label class="number" data-bs-target="20">0
                    <img src="{{asset('images/icon-plus.png')}}" class="d-inline"/>
                </label>
                <label class="title">کشورهای حوزه فعالیت</label>
            </div>
        </div>
    </div>

</div>
<div class="d-flex d-md-none justify-content-end wave-top-mobile">
    <svg xmlns="http://www.w3.org/2000/svg" width="363" height="68" viewBox="0 0 425 80" fill="none">
        <path
            d="M0 80H425C408.632 66.5241 381.981 58.1008 351.897 62.6839C334.255 65.3714 326.999 59.0319 318.217 51.3607C309.371 43.632 298.976 34.5517 274.858 31.9871C247.683 29.0971 233.284 37.0254 219.153 44.806C211.661 48.931 204.245 53.0141 195.04 55.4211C171.876 61.4786 143.016 50.3763 109.444 17.6298C80.498 -10.6047 26.6001 -0.706207 0 17.9316V80Z"
            fill="#1A2428FF"/>
    </svg>
</div>

<div class="d-none d-md-flex justify-content-md-end wave-top">
    <svg xmlns="http://www.w3.org/2000/svg" width="940" height="177" viewBox="0 0 940 177" fill="none">
        <path
            d="M0 177H940C903.797 147.185 844.852 128.548 778.313 138.688C739.294 144.634 723.244 130.608 703.822 113.636C684.255 96.5358 661.266 76.4457 607.921 70.7714C547.817 64.3774 515.968 81.9188 484.716 99.1333C468.145 108.26 451.742 117.294 431.383 122.619C380.15 136.021 316.317 111.458 242.064 39.0059C178.042 -23.4628 58.8332 -1.56248 0 39.6737V177Z"
            fill="#1A2428FF"/>
    </svg>
</div>

<script src="{{ asset('js/web/successfulVisaCounter.js') }}"></script>
