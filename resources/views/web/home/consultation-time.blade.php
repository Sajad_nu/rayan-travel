<div class="consultationTime container-fluid row ms-0" id="consultationTime">
    <p class="title px-0">تعیین وقت مشاوره</p>
    <p class="description px-0">شما می توانید پس از تکمیل و ارسال <a href="{{url('assessment')}}" class="text-bold ">فرم
            ارزیابی درخواست ویزا</a> از طریق یکی از روش های زیر
        درخواست وقت مشاوره خود را ثبت نموده و با مشاوران رایان ویزا در ارتباط باشید.</p>

    <div class="conference row pb-0">
        <ul class="row p-0">
            <li class="col-md-4 itemStart" data-bs-toggle="modal" data-bs-target="#staticBackdrop" onclick="addAdviceTypeToModalFrom('in_person')">
                <a>
                    <label class="icon">
                        <img src="{{asset('images/consultationIcons/questions.png')}}"
                             class="img-fluid"
                             alt="مشاوره حضوری برای اخذ ویزا">
                    </label>
                    <label class="conferenceType">مشاوره حضوری</label>
                </a>
            </li>

            <li class="col-md-4 itemCenter"  data-bs-toggle="modal" data-bs-target="#staticBackdrop" onclick="addAdviceTypeToModalFrom('phone')">
                <a>
                    <label class="icon">
                        <img src="{{asset('images/consultationIcons/customer-service.png')}}"
                             class="img-fluid"
                             alt="مشاوره تلفنی برای اخذ ویزا">
                    </label>
                    <label class="conferenceType">مشاوره تلفنی</label>
                </a>
            </li>

            <li class="col-md-4 itemEnd" data-bs-toggle="modal" data-bs-target="#staticBackdrop" onclick="addAdviceTypeToModalFrom('online')">
                <a>
                    <label class="icon">
                        <img src="{{asset('images/consultationIcons/online-consultant.png')}}"
                             class="img-fluid"
                             alt="مشاوره آنلاین برای اخذ ویزا">
                    </label>
                    <label class="conferenceType">مشاوره آنلاین</label>
                </a>
            </li>
        </ul>
    </div>
</div>
