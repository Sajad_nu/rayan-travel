<div class="latestOnBlog container-fluid row ms-0">
    <div class="row weblog">
        {{--        <label class="title">تازه ترین ها</label>--}}
        <label class="description">تازه ترین ها در وبلاگ رایان ویزا
            <a href="{{url('blog')}}" class="text-light">مشاهده همه
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                    <path
                        d="M10.5303 15.4697C10.8232 15.7626 10.8232 16.2374 10.5303 16.5303C10.2374 16.8232 9.76256 16.8232 9.46967 16.5303L5.46967 12.5303C5.17678 12.2374 5.17678 11.7626 5.46967 11.4697L9.46967 7.46967C9.76256 7.17678 10.2374 7.17678 10.5303 7.46967C10.8232 7.76256 10.8232 8.23744 10.5303 8.53033L7.81066 11.25H17.5C17.9142 11.25 18.25 11.5858 18.25 12C18.25 12.4142 17.9142 12.75 17.5 12.75H7.81066L10.5303 15.4697Z"
                        fill="#fff"/>
                </svg>
            </a>
        </label>
    </div>

    <div class="row lastedNews flex-nowrap">

        <div class="right col-md-6 h-auto item mb-0 card">
            <a href="{{url('blog')}}">
                <img src="{{asset('images/weblog/medical.jpg')}}" class="img-fluid card-img"
                     alt="اخبار در وبلاگ رایان ویزا" style="height: 467px">
                <p class="title card-img-overlay">
                 برای دریافت ویزای درمانی آمریکا به چه مدارکی نیاز است؟
                </p>
            </a>
        </div>

        <div class="left col-md-6">

            <div class="row">

                <div class="col-md-6 item card">
                    <a href="{{url('blog')}}">
                        <img src="{{asset('images/weblog/web6.jpeg')}}" class="small img-fluid card-img"
                             alt="اخبار در وبلاگ رایان ویزا">
                        <p class="title card-img-overlay">
                    حداقل نمره آیلتس برای دریافت ویزای تحصیلی چقدر است؟
                        </p>
                    </a>
                </div>

                <div class="col-md-6 item card">
                    <a href="{{url('blog')}}">
                        <img src="{{asset('images/weblog/document.jpg')}}" class="small img-fluid card-img"
                             alt="اخبار در وبلاگ رایان ویزا">

                        <p class="title card-img-overlay">
                         مدارک پایه برای اخذ ویزا
                        </p>
                    </a>
                </div>

            </div>

            <div class="row">
                <div class="col-md-6 item mb-0 card">
                    <a href="{{url('blog')}}">
                        <img src="{{asset('images/weblog/vaccine.jpg')}}" class="small img-fluid card-img"
                             alt="اخبار در وبلاگ رایان ویزا">

                        <p class="title card-img-overlay">
                          قبل از رفتن به سفر چه واکسن هایی بزنیم؟
                        </p>
                    </a>
                </div>
                <div class="col-md-6 item mb-0 card">
                    <a href="{{url('blog')}}">
                        <img src="{{asset('images/weblog/web4.jpg')}}" class="small img-fluid card-img"
                             alt="اخبار در وبلاگ رایان ویزا">

                        <p class="title card-img-overlay">
                            راهنمای جامع دریافت ویزا</p>
                    </a>
                </div>
            </div>

        </div>

    </div>
</div>
