@include('web.components.header')

@include('web.components.navbar-bottom')

@include('web.home.main-content')

@include('web.home.visas-types-In-Home')

@include('web.home.services')

@include('web.home.assessment-in-home')

@include('web.home.consultation-time')

@include('web.home.why-ryan-visa')

@include('web.home.successful-visas')

@include('web.home.comments-travelers')

<div class="d-none d-md-flex">
    @include('web.home.latest-on-blog')
</div>

@include('web.faq')

<div class="d-none d-md-flex">
    @include('web.home.news-letter')
</div>

@include('web.home.contact-form-modal')

@include('web.components.footer')

<script type="module" src="{{asset('js/web/sendAdvicePhoneNumber.js')}}"></script>

