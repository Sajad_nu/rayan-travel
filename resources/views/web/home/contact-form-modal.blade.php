{{-- Modal Body--}}
<div class="contactFormModal modal fade ps-0" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false"
     tabindex="-1"
     aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">

            <div class="modal-body p-4 p-md-5 pt-md-4">

                <div class="row d-flex justify-content-start mb-3">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                برای گرفتن وقت مشاوره، شماره تماس خود را وارد نموده تا مشاوران رایان ویزا در اولین فرصت با شما تماس
                برقرار نمایند.

                <input type="hidden" name="advice_type" id="adviceModalType">
                <input type="hidden" id="adviceCsrf" value="{{csrf_token()}}">

                <div class="input-group input-group-lg mt-4">
                    <input id="adviceModalFullName"
                           type="text"
                           class="form-control text-center"
                           aria-label="Large"
                           aria-describedby="inputGroup-sizing-sm"
                           placeholder="نام و نام خانوادگی خود را وارد کنید."
                    >
                </div>

                <div class="input-group input-group-lg mt-4">
                    <input id="adviceModalPhoneNumber"
                           type="text"
                           class="form-control text-center"
                           aria-label="Large"
                           aria-describedby="inputGroup-sizing-sm"
                           placeholder="شماره همراه خود را وارد کنید."
                    >
                </div>

                <div class="d-flex justify-content-center mt-4">
                    <button type="button" class="btn btn-successfull" id="submitAdviceModal"
                            onclick="submitAdviceModal()">
                        ثبت
                    </button>
                </div>
                <div class="d-flex justify-content-center mt-4">
                    <p id="showAdviceResult" class="text-success text-center"></p>
                    <p id="AdviceResultError" class="text-danger text-center"></p>
                </div>
            </div>
        </div>
    </div>
</div>


