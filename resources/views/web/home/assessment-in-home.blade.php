<div class="assessmentInHome container-fluid row d-flex flex-md-nowrap ms-0" id="assessment">

    <div class="right col-12 col-md-3 d-none d-md-flex"><img src="{{asset('images/assessmentPicture.jpg')}}"
                                                             class="img-fluid rounded" alt="فرم ارزیابی درخواست ویزا">
    </div>

    <div class="left col-12 col-md-9 pe-md-4 row">
        <div class="row d-flex flex-nowrap align-items-center mt-md-4 mt-md-0">
            <label class="assessmentIcon">
                <svg xmlns="http://www.w3.org/2000/svg" width="48" height="49" viewBox="0 0 48 49" fill="none">
                    <path
                        d="M38 6.5H10C7.8 6.5 6 8.3 6 10.5V38.5C6 40.7 7.8 42.5 10 42.5H38C40.2 42.5 42 40.7 42 38.5V10.5C42 8.3 40.2 6.5 38 6.5ZM38 38.5H10V10.5H38V38.5ZM14 20.5H18V34.5H14V20.5ZM22 14.5H26V34.5H22V14.5ZM30 26.5H34V34.5H30V26.5Z"
                        fill="#00A594"/>
                </svg>
            </label>
            <label class="title">ارزیابی درخواست ویزا</label>
        </div>
        <p class="description">شما از این طریق می توانید قبل از هر اقدامی، از میزان شانس خود برای اخذ ویزا آگاه
            شوید.</p>
        <p class="moreDescription mb-0">پس از تکمیل فرم ارزیابی درخواست ویزا، میزان شانس شما توسط کارشناس ارشد رایان
            ویزا مورد تحلیل و بررسی قرار گرفته و در نهایت مناسب ترین پاسخ برای پرونده شما پیشنهاد و ارسال خواهد شد.</p>

        <div class="row d-flex justify-content-end mx-auto">
            <a href="{{url('assessment')}}">
                برای شروع ارزیابی کلیک کنید
                <svg xmlns="http://www.w3.org/2000/svg" width="32" height="33" viewBox="0 0 32 33" fill="none">
                    <path
                        d="M14.0406 21.1263C14.4311 21.5168 14.4311 22.15 14.0406 22.5405C13.6501 22.931 13.0169 22.931 12.6264 22.5405L7.29306 17.2071C6.90253 16.8166 6.90253 16.1835 7.29306 15.7929L12.6264 10.4596C13.0169 10.0691 13.6501 10.0691 14.0406 10.4596C14.4311 10.8501 14.4311 11.4833 14.0406 11.8738L10.4144 15.5L23.3335 15.5C23.8858 15.5 24.3335 15.9478 24.3335 16.5C24.3335 17.0523 23.8858 17.5 23.3335 17.5H10.4144L14.0406 21.1263Z"
                        fill="white"/>
                </svg>
            </a>
        </div>

    </div>

</div>

{{--mobile--}}
<svg xmlns="http://www.w3.org/2000/svg" width="375" height="80" viewBox="0 0 375 80" fill="none"
     class="wave-bottom d-md-none position-absolute">
    <path
        d="M431 -1H0C16.5996 12.6443 43.6262 21.173 74.1353 16.5326C92.0259 13.8115 99.3849 20.2302 108.29 27.9973C117.262 35.8226 127.803 45.0164 152.262 47.6131C179.82 50.5391 194.423 42.5117 208.753 34.6339C216.351 30.4573 223.872 26.3232 233.206 23.8861C256.697 17.7529 285.965 28.994 320.011 62.1498C349.366 90.7372 404.024 80.715 431 61.8442V-1Z"
        fill="#1A2428FF"/>
</svg>
<img src="{{asset('images/benefit.png')}}" class="img-fluid benefitImage d-md-none" alt="تعیین وقت مشاوره"/>


{{--desktop--}}
<div class="row  d-none d-md-flex">
    <div class="wave col-md-6 d-flex flex-nowrap position-relative">

        <svg xmlns="http://www.w3.org/2000/svg" width="940" height="176" viewBox="0 0 940 176" fill="none"
             class="wave-bottom position-absolute">
            <path
                d="M940 -1H0C36.2032 28.8154 95.1476 47.452 161.687 37.3119C200.706 31.3658 216.756 45.3919 236.178 62.3644C255.745 79.4642 278.734 99.5543 332.079 105.229C392.183 111.623 424.032 94.0812 455.284 76.8667C471.855 67.7401 488.258 58.7062 508.617 53.3808C559.85 39.9785 623.683 64.5424 697.936 136.994C761.958 199.463 881.167 177.562 940 136.326V-1Z"
                fill="#1A2428FF"/>
        </svg>
    </div>
    <div class="col-md-6"><img src="{{asset('images/benefit.png')}}" class="img-fluid benefitImage"
                               alt="تعیین وقت مشاوره"/></div>
</div>



