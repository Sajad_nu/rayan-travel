<div class="services container-fluid row ms-0">

    <hr style="color: #eee">

    <div class="servicesTitle text-center mt-5 ">خدمات رایان ویزا</div>

    <div class="row servicesItems d-flex justify-content-center px-4 px-md-0 pt-0">

        <div class="servicesItem col-md-2 clickable">
            <a href="{{url('touristVisa')}}" class="text-decoration-none">
                <div class="servicesIcon">
                    <img src="{{asset('images/servicesIcons/tourist.png')}}" class="img-fluid" alt="اخذ ویزای توریستی">
                </div>
                <p class="description mb-0">
                    اخذ ویزای توریستی کشورهای هدف
                </p>
            </a>
        </div>

        <div class="servicesItem col-md-2">
            <a data-bs-toggle="modal" data-bs-target="#staticBackdrop" onclick="addAdviceTypeToModalFrom('online')">
                <div class="servicesIcon">
                    <img src="{{asset('images/servicesIcons/plan.png')}}" class="img-fluid"
                         alt="مشاوره ویزا و برنامه سفر">
                </div>
                <p class="description mb-0">
                    مشاوره ویزا و برنامه سفر
                </p>
            </a>
        </div>

        <div class="servicesItem col-md-2 comingSoon">
             <span
                 class="badgeSoon position-absolute translate-middle p-2 bg-light  shadow-lg text-danger border border-light rounded blinking-text">
                <span class="">به زودی</span>
            </span>

            <div class="servicesIcon">
                <img src="{{asset('images/servicesIcons/deadline.png')}}" class="img-fluid" alt="تعیین وقت سفارت">
            </div>

            <p class="description mb-0">تعیین وقت سفارت</p>
        </div>
        <div class="servicesItem col-md-2 comingSoon">
 <span
     class="badgeSoon position-absolute translate-middle p-2 bg-light  shadow-lg text-danger border border-light rounded blinking-text">
                <span class="">به زودی</span>
            </span>
            <div class="servicesIcon">
                <img src="{{asset('images/servicesIcons/airplane.png')}}" class="img-fluid"
                     alt="رزرواسیون ایرلاین‌های داخلی و خارجی">
            </div>
            <p class="description mb-0">رزرواسیون ایرلاین‌های داخلی و خارجی</p>
        </div>
    </div>

    <div class="row servicesItems d-flex justify-content-center px-4 px-md-0 pt-0">

        <div class="servicesItem col-md-2 comingSoon">
 <span
     class="badgeSoon position-absolute translate-middle p-2 bg-light  shadow-lg text-danger border border-light rounded blinking-text">
                <span class="">به زودی</span>
            </span>
            <div class="servicesIcon">
                <img src="{{asset('images/servicesIcons/hotel.png')}}" class="img-fluid"
                     alt="رزرواسیون هتل‌های داخلی و خارجی">
            </div>
            <p class="description mb-0">رزرواسیون هتل‌های داخلی و خارجی</p>
        </div>

        <div class="servicesItem col-md-2 comingSoon">
 <span
     class="badgeSoon position-absolute translate-middle p-2 bg-light  shadow-lg text-danger border border-light rounded blinking-text">
                <span class="">به زودی</span>
            </span>
            <div class="servicesIcon">
                <img src="{{asset('images/servicesIcons/train.png')}}" class="img-fluid" alt="رزرواسیون مسیرهای ریلی">
            </div>
            <p class="description mb-0">رزرواسیون مسیرهای ریلی</p>
        </div>

        <div class="servicesItem col-md-2 comingSoon">
 <span
     class="badgeSoon position-absolute translate-middle p-2 bg-light  shadow-lg text-danger border border-light rounded blinking-text">
                <span class="">به زودی</span>
            </span>
            <div class="servicesIcon">
                <img src="{{asset('images/servicesIcons/tour-guide.png')}}" class="img-fluid"
                     alt="تورهای داخلی و خارجی">
            </div>
            <p class="description mb-0">طراحی و برگذاری تورهای داخلی و خارجی</p>
        </div>
    </div>

    <hr style="color: #eee" class="d-none d-md-flex">


</div>

