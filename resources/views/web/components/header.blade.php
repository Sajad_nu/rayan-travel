<!DOCTYPE html>
<html lang="fa-ir" dir="rtl">
<head>

    @include('web.meta-data')

    <link rel="shortcut icon" type="image/png" href="{{ url('images/favicon.ico') }}"/>
    <link rel="canonical" href="{{url($metaData->getCanonicalUrl())}}">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.rtl.min.css"
          integrity="sha384-nU14brUcp6StFntEOOEBvcJm4huWjB0OcIeQ3fltAfSmuZFrkAif0T+UtNGlKKQv" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.css"/>

    @vite(['resources/scss/main.scss', 'resources/js/web/vue/app.js'])
</head>

<body>
<div class="header">
    <nav class="container-fluid navbar navbar-expand-md fixed-top">

        <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasDarkNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="offcanvas offcanvas-start w-75 text-bg-dark text-right" tabindex="-1" id="offcanvasDarkNavbar">
            <div class="offcanvas-header">
                <h5 class="offcanvas-title gx-2" id="offcanvasDarkNavbarLabel">
                    <img class="logo me-2" src="{{asset('images/logo2.png')}}" alt="لوگو رایان ویزا"/>
                    رایان سیاحت
                </h5>

                <button type="button" class="btn-close btn-close-white me-2" data-bs-dismiss="offcanvas"
                        aria-label="Close"></button>
            </div>
            <hr class="d-md-none my-0">

            <div class="offcanvas-body px-md-5">

                <ul class="navbar-nav flex-grow-1 pe-3" id="menu">

                    <li class="nav-item">
                        <a  class="nav-link" href="https://ryan-visa.com/">
                            <img class="logo d-none d-md-flex" src="{{asset('images/logo2.png')}}" alt="لوگو رایان ویزا">
                        </a>

                    </li>
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="{{url('/')}}">خانه</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                           data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            درخواست ویزا
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{url('touristVisa')}}">
                                <img class="img-fluid me-1" src="{{asset('images/visaTypeIcons/tourist.png')}}"
                                     width="32"
                                     height="32" alt="ویزای توریستی">
                                ویزای توریستی</a>
                            <a class="dropdown-item" href="{{url('educationalVisa')}}">
                                <img class="img-fluid me-1" src="{{asset('images/visaTypeIcons/educational.png')}}"
                                     width="32" height="32" alt="ویزای تحصیلی">
                                ویزای تحصیلی</a>
                            <a class="dropdown-item" href="{{url('medicalVisa')}}">
                                <img class="img-fluid me-1" src="{{asset('images/visaTypeIcons/medical.png')}}"
                                     width="32"
                                     height="32" alt="ویزای درمانی">
                                ویزای درمانی</a>
                            <a class="dropdown-item" href="{{url('sportVisa')}}">
                                <img class="img-fluid me-1" src="{{asset('images/visaTypeIcons/sport.png')}}" width="32"
                                     height="32" alt="ویزای ورزشی">
                                ویزای ورزشی</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " aria-current="page" href="{{url('about-us')}}">درباره ما</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " aria-current="page" href="{{url('contact-us')}}">تماس با ما</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" aria-current="page" href="{{url('blog')}}">وبلاگ</a>
                    </li>

                </ul>
                <hr>
                <div class="col-md-2 text-center text-md-end">
                    <a href="tel:05137057663" title="شماره تماس">
                        <img src="{{asset('images/phone-icon2.png')}}" class="img-fluid mt-md-4"
                             alt="شماره تماس رایان ویزا">
                    </a>
                </div>
            </div>
        </div>
    </nav>
</div>


<script>
    document.addEventListener("DOMContentLoaded", function () {
        // Get the current URL path
        let pathName = window.location.pathname;

        // Get all navigation links
        let navLinks = document.querySelectorAll("#menu a.nav-link");

        // Find the matching link and add the 'active' class
        navLinks.forEach(function (link) {
            // Check if the link's href attribute ends with the current pathName
            if (link.getAttribute("href").endsWith(pathName)) {
                link.classList.add("active");
            }
        });
    });
</script>


