<div class="navbarBottom container-fluid row ">
    <nav class="navbar navbar-light bg-light sticky-navbar py-0">

            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="#assessment">تکمیل فرم ارزیابی</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#consultationTime">رزرو وقت مشاوه</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#mainContent">
                        رفتن به بالا
                        <img src="{{asset('images/up-arrow.png')}}" alt="آیکون رفتن به بالا">
                       </a>
                </li>
            </ul>

    </nav>

</div>
