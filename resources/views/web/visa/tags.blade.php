<div class="tagsBody container-fluid">

    <div class="tags container row">
        <div class="title">لینک های مرتبط :</div>

        <div class="countryTags row ">
            @foreach($tags as $tag)
                <div class="item col-md-2"><a class="tagName" href="{{url($tag->getUri())}}">
                        {{$tag->getTitle()}}</a>
                </div>
            @endforeach
        </div>

    </div>
</div>
