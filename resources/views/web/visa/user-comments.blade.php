<div class="userCommentsBody container-fluid">
    <div class="userComments d-flex row ">
        <!-- Button trigger modal -->
        <p class="title col-md-9">ثبت دیدگاه کاربران</p>

        <p class="description">
        شما می توانید با ثبت نظر خود، ما را در ارائه خدمات بهتر، همراهی نموده و همچنین می توانید از این طریق  برای گرفتن وقت مشاوره اقدام نمایید.
        </p>
        <button type="button" class="btn btn-comment w-auto mx-auto" data-bs-toggle="modal" data-bs-target="#comments">
            ثبت دیدگاه
        </button>

        <!-- Modal -->
        <div class="modal fade" id="comments" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
             aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title fs-6" id="title">دیدگاه خود را ثبت کنید.</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">

                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="نام و نام خانوادگی"
                                   aria-label="Sizing example input" aria-describedby="inputGroup-sizing-lg">
                        </div>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="شماره تماس"
                                   aria-label="Sizing example input" aria-describedby="inputGroup-sizing-lg">
                        </div>

                        <div class="input-group">
                            <textarea type="text" class="form-control" placeholder="دیگاه خود را بنوسید ..."
                                      aria-label="Sizing example input"
                                      aria-describedby="inputGroup-sizing-lg"></textarea>
                        </div>


                        <div class="d-flex justify-content-center mt-4 mb-3">
                            <button type="button" class="btn btn-comments">ثبت دیدگاه</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>


