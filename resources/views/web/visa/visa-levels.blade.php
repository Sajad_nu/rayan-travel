<div class="visaLevelsBody container-fluid ">
    <div class="visaLevels container row">

        <p class="title ps-0" id="levels">مراحل اخذ ویزا </p>
        <p class="description ps-0">{{$visa->getProcedures()}} </p>

        <div class="row items mx-auto">

            @foreach($procedures as $key => $procedure)

                <div class="row item">
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{asset('images/numbersIcons/number-'. $key+1 .'.png')}}"
                             class="img-fluid d-flex me-3"
                             alt="مراحل اخذ ویزا">
                        <label class="title mb-0"> {!! $procedure->getTitle() !!}</label>
                    </div>

                    <p class="description mb-0">
                        {!! $procedure->getDescription() !!}
                    </p>
                </div>
                <hr class="my-3">

            @endforeach
        </div>
    </div>
</div>
