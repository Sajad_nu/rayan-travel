@include('web.components.header')
@include('web.components.navbar-bottom')

@include('web.visa.take-visa')

<div class="container-fluid row mx-auto px-0 mb-5 mb-md-0 ">
    <div class="col-md-9 px-0">
        @include('web.visa.visa-documents')

        @include('web.visa.visa-levels')

        @include('web.visa.visa-cost')

        <div class="d-none d-md-flex">
            @include('web.visa.tags')
        </div>
        @include('web.faq')
    </div>
    <div class="col-md-3 mx-auto sidebar d-flex flex-column">
        @include('web.visa.assessment-in-visa')
        @include('web.visa.user-comments')
    </div>
</div>


@include('web.components.footer')
