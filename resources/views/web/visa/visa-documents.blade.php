<div class="container-fluid visaDocumentsBody">
    <div class="visaDocuments container row text-center">

        <p class="title ps-0" id="documents">مدارک لازم برای اخذ ویزا</p>
        <p class="description ps-0">
            {{$visa->getDocuments()}}
{{--            <br>--}}
{{--            <span class="point"></span>--}}
{{--            تلفن اداری : ۶۶-۰۵۱۳۷۰۵۷۶۶۳--}}
{{--            <br>--}}
{{--            <span class="point"></span>--}}
{{--            مدیریت (حسامی) : ۰۹۱۵۳۱۶۳۲۴۶--}}
        </p>

        <div class="row items mx-auto p-2">
            @foreach($documents as $document)

                <div class="row item">
                    <div class="d-flex flex-row">
                        <img src="{{asset('images/arrow-left.png')}}" class="img-fluid d-flex me-3" alt="مدارک لازم اخذ ویزا">
                        <label class="title mb-0">{{$document->getTitle()}}</label>
                    </div>

                    <p class="description mb-0">{{$document->getDescription()}}</p>
                </div>
                <hr class="my-3">

            @endforeach
        </div>
        </div>

</div>
