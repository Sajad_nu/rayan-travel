<div class="takeVisa container-fluid row ms-0">

    <div class="row bg-countries">
        <div class="over">

            <div class="row banner">
                <img src="{{asset($visa->getFileUri())}}" class="img-fluid image ms-0" alt="اخذ {{$visa->getTitle()}}"/>
                <p class="title"> {{$visa->getTitle()}}</p>
                <p class="description">
                    {{$visa->getDescription()}}
                </p>

            </div>
        </div>
    </div>

</div>
