<div class="assessmentInVisa container-fluid row d-flex flex-md-nowrap ms-0" id="assessment">

    <div class="left row mx-auto" >
        <div class="row d-flex mt-md-4 align-items-center mt-md-0">
            <label class="assessmentIcon col-2">
                <svg xmlns="http://www.w3.org/2000/svg" width="48" height="49" viewBox="0 0 48 49" fill="none">
                    <path
                        d="M38 6.5H10C7.8 6.5 6 8.3 6 10.5V38.5C6 40.7 7.8 42.5 10 42.5H38C40.2 42.5 42 40.7 42 38.5V10.5C42 8.3 40.2 6.5 38 6.5ZM38 38.5H10V10.5H38V38.5ZM14 20.5H18V34.5H14V20.5ZM22 14.5H26V34.5H22V14.5ZM30 26.5H34V34.5H30V26.5Z"
                        fill="#00A594"/>
                </svg>
            </label>
            <label class="title col-9">ارزیابی درخواست ویزا</label>
        </div>
        <p class="description">شما از این طریق می توانید قبل از هر اقدامی، از میزان شانس خود برای اخذ ویزا آگاه
            شوید.</p>
        <p class="moreDescription mb-0">پس از تکمیل فرم ارزیابی درخواست ویزا، میزان شانس شما توسط کارشناس ارشد رایان
            ویزا مورد تحلیل و بررسی قرار گرفته و در نهایت مناسب ترین پاسخ برای پرونده شما پیشنهاد و ارسال خواهد شد.</p>

        <div class="row d-flex justify-content-center mx-auto">
            <a href="{{url('assessment')}}">
                برای شروع ارزیابی کلیک کنید
                <svg xmlns="http://www.w3.org/2000/svg" width="32" height="33" viewBox="0 0 32 33" fill="none">
                    <path
                        d="M14.0406 21.1263C14.4311 21.5168 14.4311 22.15 14.0406 22.5405C13.6501 22.931 13.0169 22.931 12.6264 22.5405L7.29306 17.2071C6.90253 16.8166 6.90253 16.1835 7.29306 15.7929L12.6264 10.4596C13.0169 10.0691 13.6501 10.0691 14.0406 10.4596C14.4311 10.8501 14.4311 11.4833 14.0406 11.8738L10.4144 15.5L23.3335 15.5C23.8858 15.5 24.3335 15.9478 24.3335 16.5C24.3335 17.0523 23.8858 17.5 23.3335 17.5H10.4144L14.0406 21.1263Z"
                        fill="white"/>
                </svg>
            </a>
        </div>

    </div>

</div>



