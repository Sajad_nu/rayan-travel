<div class="formAndAddressContactUsBody container-fluid d-flex">
    <div class="formAndAddressContactUs container d-flex row">

        <div class="col-md-5 address">

            <p class="title">آدرس شعب رایان ویزا :</p>

            <div class="item">
                <p class="title d-flex align-items-center">
                    <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 48 48" fill="none">
                        <path
                            d="M24 4.5C16.5469 4.5 10.5 10.2553 10.5 17.3438C10.5 25.5 19.5 38.4253 22.8047 42.8897C22.9419 43.0781 23.1217 43.2315 23.3294 43.3372C23.5371 43.443 23.7669 43.4981 24 43.4981C24.2331 43.4981 24.4629 43.443 24.6706 43.3372C24.8783 43.2315 25.0581 43.0781 25.1953 42.8897C28.5 38.4272 37.5 25.5066 37.5 17.3438C37.5 10.2553 31.4531 4.5 24 4.5Z"
                            stroke="#00A594"
                            stroke-width="4"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                        ></path>
                        <path
                            d="M24 22.5C26.4853 22.5 28.5 20.4853 28.5 18C28.5 15.5147 26.4853 13.5 24 13.5C21.5147 13.5 19.5 15.5147 19.5 18C19.5 20.4853 21.5147 22.5 24 22.5Z"
                            stroke="#00A594"
                            stroke-width="4"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                        ></path>
                    </svg>
                    رایان سیاحت مشهد
                </p>
                <p class="description"> مشهد، میدان جانباز، برج اداری شماره ۲ پاژ، طبقه ۱۴، واحد ۱۴۰۲ و ۱۴۰۳</p>
                <span class="point"></span>
                تلفن اداری : ۶۶-۰۵۱۳۷۰۵۷۶۶۳
                <br>
                <span class="point"></span>
                مدیریت (حسامی) : ۰۹۱۵۳۱۶۳۲۴۶
            </div>
            <hr class="my-4">

            <div class="item">
                <p class="title d-flex align-items-center">
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="32"
                        height="32"
                        viewBox="0 0 48 48"
                        fill="none"
                    >
                        <path
                            d="M24 4.5C16.5469 4.5 10.5 10.2553 10.5 17.3438C10.5 25.5 19.5 38.4253 22.8047 42.8897C22.9419 43.0781 23.1217 43.2315 23.3294 43.3372C23.5371 43.443 23.7669 43.4981 24 43.4981C24.2331 43.4981 24.4629 43.443 24.6706 43.3372C24.8783 43.2315 25.0581 43.0781 25.1953 42.8897C28.5 38.4272 37.5 25.5066 37.5 17.3438C37.5 10.2553 31.4531 4.5 24 4.5Z"
                            stroke="#00A594"
                            stroke-width="4"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                        >
                        </path>
                        <path
                            d="M24 22.5C26.4853 22.5 28.5 20.4853 28.5 18C28.5 15.5147 26.4853 13.5 24 13.5C21.5147 13.5 19.5 15.5147 19.5 18C19.5 20.4853 21.5147 22.5 24 22.5Z"
                            stroke="#00A594" stroke-width="4" stroke-linecap="round" stroke-linejoin="round"></path>
                    </svg>
                    آریا توس تراولز آمریکا
                </p>
                <p class="description"> شرکت آریا توس تراولز در کالیفرنیا </p>
                <span class="point"></span>
                تلفن اداری : ۶۶-۰۵۱۳۷۰۵۷۶۶۳
                <br>
                <span class="point"></span>
                مدیریت (حسامی) : ۰۹۱۵۳۱۶۳۲۴۶
            </div>
            <hr class="my-4">


            <div class="item">
                <p class="title d-flex align-items-center">
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="32"
                        height="32"
                        viewBox="0 0 48 48"
                        fill="none">
                        <path
                            d="M24 4.5C16.5469 4.5 10.5 10.2553 10.5 17.3438C10.5 25.5 19.5 38.4253 22.8047 42.8897C22.9419 43.0781 23.1217 43.2315 23.3294 43.3372C23.5371 43.443 23.7669 43.4981 24 43.4981C24.2331 43.4981 24.4629 43.443 24.6706 43.3372C24.8783 43.2315 25.0581 43.0781 25.1953 42.8897C28.5 38.4272 37.5 25.5066 37.5 17.3438C37.5 10.2553 31.4531 4.5 24 4.5Z"
                            stroke="#00A594"
                            stroke-width="4"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                        ></path>
                        <path
                            d="M24 22.5C26.4853 22.5 28.5 20.4853 28.5 18C28.5 15.5147 26.4853 13.5 24 13.5C21.5147 13.5 19.5 15.5147 19.5 18C19.5 20.4853 21.5147 22.5 24 22.5Z"
                            stroke="#00A594"
                            stroke-width="4"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                        ></path>
                    </svg>
                    حسامی ویزا کانادا
                </p>
                <p class="description">
                    (407)Heddle Crescent Ave, Newmarket, Ontario, Canada, L3X2J2
                </p>
                <span class="point"></span>
                تلفن اداری : 9769-233(437)1+
                <br>
                <span class="point"></span>
                مدیریت (حسامی) : ۰۹۱۵۳۱۶۳۲۴۶
            </div>
        </div>

        <div class="col-md-5 formBody">
            <form class="form w-100">

                <div class="row mx-auto">
                    <p class="title"> مشاوره رایگان با رایان ویزا</p>
                    <p class="description">مشاوران مجرب و حرفه ای رایان ویزا از ابتدا تا انتها در کنار شما هستند.
                        اگر قصد دریافت ویزا دارید، پیام بگذارید و یا با شماره های
                        موجود تماس بگیرید تا مشاوران رایان ویزا در اولین فرصت شما را راهنمایی کنند.</p>
                </div>

                <div class="input-group input-group-lg">
                    <input
                        type="text"
                        class="form-control bg-primary-custom shadow-lg"
                        aria-label="Large"
                        id="fullName"
                        aria-describedby="inputGroup-sizing-sm"
                        placeholder="نام و نام خانوادگی"
                    >
                </div>

                <input type="hidden" id="csrf" value="{{csrf_token()}}">
                <input type="hidden" id="title" value="تماس با ما">

                <div class="input-group input-group-lg">
                    <input
                        type="text"
                        class="form-control bg-light shadow-lg"
                        aria-label="Large"
                        id="phoneNumber"
                        aria-describedby="inputGroup-sizing-sm"
                        placeholder="شماره تماس"
                    >
                </div>


                <p class="name">پیام و یا درخواست خود را با ما در میان بگذارید. </p>
                <div class="input-group input-group-lg">
                <textarea
                    type="text"
                    class="form-control bg-light shadow-lg"
                    aria-label="Large"
                    id="text"
                    aria-describedby="inputGroup-sizing-sm"
                >
                </textarea>
                </div>


                <div class="row btnSend d-flex justify-content-center align-items-center mx-auto">
                    <button
                        type="button"
                        class="btn sendRequest"
                        id="sendContactUsFormButton"
                        onclick="submitContactUsForm()"
                    >
                        ارسال درخواست
                        <svg xmlns="http://www.w3.org/2000/svg"
                             width="32"
                             height="32"
                             viewBox="0 0 32 32"
                             fill="none">
                            <path
                                d="M14.0401 20.6263C14.4306 21.0168 14.4306 21.65 14.0401 22.0405C13.6496 22.431 13.0164 22.431 12.6259 22.0405L7.29257 16.7071C6.90204 16.3166 6.90204 15.6835 7.29257 15.2929L12.6259 9.9596C13.0164 9.56908 13.6496 9.56908 14.0401 9.9596C14.4306 10.3501 14.4306 10.9833 14.0401 11.3738L10.4139 15L23.333 15C23.8853 15 24.333 15.4478 24.333 16C24.333 16.5523 23.8853 17 23.333 17H10.4139L14.0401 20.6263Z"
                                fill="#1a2428"/>
                        </svg>
                    </button>
                </div>

                <div class="row d-flex justify-content-center mt-4">
                    <p id="showResult" class="text-success text-center"></p>
                    <p id="showError" class="text-danger text-center"></p>
                </div>

            </form>
        </div>


    </div>
</div>


