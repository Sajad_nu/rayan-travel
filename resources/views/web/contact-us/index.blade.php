@include('web.components.header')

@include('web.contact-us.contact-us')

@include('web.contact-us.form-and-address')

@include('web.components.footer')

<script type="module" src="{{asset('js/web/contactUsForm.js')}}"></script>
