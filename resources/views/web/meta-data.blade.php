<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5, shrink-to-fit=no">
<meta name="csrf-token" content="{{ csrf_token() }}">

@if(!is_null($metaData->getRobots()))
    <meta name="robots" content="{{$metaData->getRobots()}}"/>
@endif


<title>{{$metaData->getTitle()}}</title>
<meta name="description" content="{{$metaData->getDescription()}}">
<meta name="Keywords" content="{{$metaData->getKeywords()}}">

@if(!is_null($metaData->getIndex()))
    <meta content="index" name="{{$metaData->getIndex()}}">
@endif
