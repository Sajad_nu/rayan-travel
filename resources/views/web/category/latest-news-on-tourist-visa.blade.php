<div class="latestNewsOnTouristVisa container-fluid d-flex row ms-0">

    <p class="description mb-5">
        پر بازدید ترین جاذبه ها
    </p>

    <div class="row d-flex align-items-center justify-content-between mx-auto px-0">


                <div class="col-md-2 mb-4">
                    <div class="swiper mySwiper">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="card p-0">
                                    <img src="{{asset('images/banners/canada/350x350/touristBanner.jpg')}}" class="img-fluid card-img"
                                         alt="جاذبه های گردشگری کانادا">
                                    <div class="card-body p-4 card-img-overlay">
                                        <a href="#" class="text-light">
                                            جاذبه های گردشگری کانادا
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="card p-0">
                                    <img src="{{asset('images/banners/canada/350x350/studyBanner.jpg')}}" class="img-fluid card-img"
                                         alt="دانشگاه های کانادا">
                                    <div class="card-body p-4 card-img-overlay">
                                        <a href="#" class="text-light">
                                            دانشگاه های کانادا
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="card p-0">
                                    <img src="{{asset('images/banners/canada/350x350/medicalBanner.jpg')}}" class="img-fluid card-img"
                                         alt="بیمارستان های کانادا">
                                    <div class="card-body p-4 card-img-overlay">

                                        <a href="#" class="text-light">
                                            بیمارستان های کانادا
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="card p-0">
                                    <img src="{{asset('images/banners/canada/350x350/sportBanner.jpg')}}" class="img-fluid card-img"
                                         alt="استادیوم های ورزشی کانادا">
                                    <div class="card-body p-4 card-img-overlay">
                                        <a href="#" class="text-light">
                                          استادیوم های ورزشی کانادا
                                        </a>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>

                </div>
                <div class="col-md-2 mb-4">
                    <div class="swiper mySwiper">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="card p-0">
                                    <img src="{{asset('images/banners/america/350x350/touristBanner.jpg')}}" class="img-fluid card-img"
                                         alt="جاذبه های گردشگری آمریکا">
                                    <div class="card-body p-4 card-img-overlay">
                                        <a href="#" class="text-light">
                                            جاذبه های گردشگری آمریکا
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="card p-0">
                                    <img src="{{asset('images/banners/america/350x350/studyBanner.jpg')}}" class="img-fluid card-img"
                                         alt="دانشگاه های آمریکا">
                                    <div class="card-body p-4 card-img-overlay">
                                        <a href="#" class="text-light">
                                            دانشگاه های آمریکا
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="card p-0">
                                    <img src="{{asset('images/banners/america/350x350/medicalBanner.jpg')}}" class="img-fluid card-img"
                                         alt="بیمارستان های آمریکا">
                                    <div class="card-body p-4 card-img-overlay">

                                        <a href="#" class="text-light">
                                            بیمارستان های آمریکا
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="card p-0">
                                    <img src="{{asset('images/banners/america/350x350/sportBanner.jpg')}}" class="img-fluid card-img"
                                         alt="استادیوم های ورزشی آمریکا">
                                    <div class="card-body p-4 card-img-overlay">
                                        <a href="#" class="text-light">
                                            استادیوم های ورزشی آمریکا
                                        </a>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>

                </div>

                <div class="col-md-2 mb-4 d-none d-md-flex">
                    <div class="swiper mySwiper">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="card p-0">
                                    <img src="{{asset('images/banners/australia/350x350/touristBanner.jpg')}}" class="img-fluid card-img"
                                         alt="جاذبه های گردشگری استرالیا">
                                    <div class="card-body p-4 card-img-overlay">
                                        <a href="#" class="text-light">
                                            جاذبه های گردشگری استرالیا
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="card p-0">
                                    <img src="{{asset('images/banners/australia/350x350/studyBanner.jpg')}}" class="img-fluid card-img"
                                         alt="دانشگاه های استرالیا">
                                    <div class="card-body p-4 card-img-overlay">
                                        <a href="#" class="text-light">
                                            دانشگاه های استرالیا
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="card p-0">
                                    <img src="{{asset('images/banners/australia/350x350/medicalBanner.jpg')}}" class="img-fluid card-img"
                                         alt="بیمارستان های استرالیا">
                                    <div class="card-body p-4 card-img-overlay">

                                        <a href="#" class="text-light">
                                            بیمارستان های استرالیا
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="card p-0">
                                    <img src="{{asset('images/banners/australia/350x350/sportBanner.jpg')}}" class="img-fluid card-img"
                                         alt="استادیوم های ورزشی استرالیا">
                                    <div class="card-body p-4 card-img-overlay">
                                        <a href="#" class="text-light">
                                            استادیوم های ورزشی استرالیا
                                        </a>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>

                </div>
                <div class="col-md-2 mb-4 d-none d-md-flex">
                    <div class="swiper mySwiper">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="card p-0">
                                    <img src="{{asset('images/banners/england/350x350/touristBanner.jpg')}}" class="img-fluid card-img"
                                         alt="جاذبه های گردشگری انگلستان">
                                    <div class="card-body p-4 card-img-overlay">
                                        <a href="#" class="text-light">
                                            جاذبه های گردشگری انگلستان
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="card p-0">
                                    <img src="{{asset('images/banners/england/350x350/studyBanner.jpg')}}" class="img-fluid card-img"
                                         alt="دانشگاه های انگلستان">
                                    <div class="card-body p-4 card-img-overlay">
                                        <a href="#" class="text-light">
                                            دانشگاه های انگلستان
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="card p-0">
                                    <img src="{{asset('images/banners/england/350x350/medicalBanner.jpg')}}" class="img-fluid card-img"
                                         alt="بیمارستان های انگلستان">
                                    <div class="card-body p-4 card-img-overlay">

                                        <a href="#" class="text-light">
                                            بیمارستان های انگلستان
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="card p-0">
                                    <img src="{{asset('images/banners/england/350x350/sportBanner.jpg')}}" class="img-fluid card-img"
                                         alt="استادیوم های ورزشی انگلستان">
                                    <div class="card-body p-4 card-img-overlay">
                                        <a href="#" class="text-light">
                                            استادیوم های ورزشی انگلستان
                                        </a>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>

                </div>
                <div class="col-md-2 mb-4 d-none d-md-flex">
                    <div class="swiper mySwiper">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="card p-0">
                                    <img src="{{asset('images/banners/schengen/350x350/touristBanner.jpg')}}" class="img-fluid card-img"
                                         alt="جاذبه های گردشگری حوزه شینگن">
                                    <div class="card-body p-4 card-img-overlay">
                                        <a href="#" class="text-light">
                                            جاذبه های گردشگری شینگن
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="card p-0">
                                    <img src="{{asset('images/banners/schengen/350x350/studyBanner.jpg')}}" class="img-fluid card-img"
                                         alt="دانشگاه های شینگن">
                                    <div class="card-body p-4 card-img-overlay">
                                        <a href="#" class="text-light">
                                            دانشگاه های شینگن
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="card p-0">
                                    <img src="{{asset('images/banners/schengen/350x350/medicalBanner.jpg')}}" class="img-fluid card-img"
                                         alt="بیمارستان های شینگن">
                                    <div class="card-body p-4 card-img-overlay">

                                        <a href="#" class="text-light">
                                            بیمارستان های شینگن
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="card p-0">
                                    <img src="{{asset('images/banners/schengen/350x350/sportBanner.jpg')}}" class="img-fluid card-img"
                                         alt="استادیوم های ورزشی شینگن">
                                    <div class="card-body p-4 card-img-overlay">
                                        <a href="#" class="text-light">
                                            استادیوم های ورزشی شینگن
                                        </a>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>

                </div>


            </div>

</div>


<script src="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.js"></script>
<script>
    let swiper = new Swiper(".mySwiper", {
        loop: true,
        centeredSlides: true,
        slidesPerView: 1,
        effect: "fade",
        autoplay: {
            delay: 4000,
            disableOnInteraction: false,
        },
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
    });
</script>
