<div class="visaType container-fluid d-flex justify-content-center row ms-0 card">

    <div class="row d-flex justify-content-center align-items-center">

        <div class="col-md-1 text-center text-md-end"><img src="{{asset('images/visaTypeIcons/tourist.png')}}"
                                                           class="img-fluid" alt="ویزای توریستی"></div>
        <div class="col-md-2 px-0">
            <label class="title">ویزای {{$category->getTitle()}}</label></div>
    </div>

    <div class="row d-flex justify-content-between px-0">
        <p class="description mb-5 mb-md-3 px-0">
            {{$category->getDescription()}}
        </p>

    </div>


</div>

{{--mobile--}}
<svg xmlns="http://www.w3.org/2000/svg" width="375" height="80" viewBox="0 0 375 80" fill="none"
     class="wave-bottom d-md-none position-absolute">
    <path
        d="M431 -1H0C16.5996 12.6443 43.6262 21.173 74.1353 16.5326C92.0259 13.8115 99.3849 20.2302 108.29 27.9973C117.262 35.8226 127.803 45.0164 152.262 47.6131C179.82 50.5391 194.423 42.5117 208.753 34.6339C216.351 30.4573 223.872 26.3232 233.206 23.8861C256.697 17.7529 285.965 28.994 320.011 62.1498C349.366 90.7372 404.024 80.715 431 61.8442V-1Z"
        fill="#1A2428FF"/>
</svg>


{{--desktop--}}
<div class="row  d-none d-md-flex">
    <div class="wave col-md-6 d-flex flex-nowrap position-relative">

        <svg xmlns="http://www.w3.org/2000/svg" width="940" height="176" viewBox="0 0 940 176" fill="none"
             class="wave-bottom position-absolute">
            <path
                d="M940 -1H0C36.2032 28.8154 95.1476 47.452 161.687 37.3119C200.706 31.3658 216.756 45.3919 236.178 62.3644C255.745 79.4642 278.734 99.5543 332.079 105.229C392.183 111.623 424.032 94.0812 455.284 76.8667C471.855 67.7401 488.258 58.7062 508.617 53.3808C559.85 39.9785 623.683 64.5424 697.936 136.994C761.958 199.463 881.167 177.562 940 136.326V-1Z"
                fill="#1A2428FF"/>
        </svg>
    </div>

</div>

