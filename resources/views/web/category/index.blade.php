@include('web.components.header')

@include('web.components.navbar-bottom')

@include('web.category.visa-type')

@include('web.category.target-countries')

@include('web.category.latest-news-on-tourist-visa')

@include('web.faq')

@include('web.components.footer')
