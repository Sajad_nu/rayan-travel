<div class="targetCountries container-fluid d-flex row mx-auto">

    <p class="title"> کشورهای هدف</p>

    <div class="row items">

        @foreach($visas as $visa)
            <div class="col-12 col-md-2 item p-0">
                <a class="text-decoration-none card" href="{{url($visa->getUri())}}">
                    <img src="{{asset($visa->getFileUri())}}" class="img-fluid img-thumbnail card-img" alt="{{$visa->getTitle()}}">
                    <p class="country card-img-overlay">{{$visa->getTitle()}}</p>
                </a>
            </div>
        @endforeach

    </div>

</div>
