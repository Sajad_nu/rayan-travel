<div class="successfulVisasInAboutUs container mx-auto row px-md-0">

    <div class="row successfulVisasNumber px-md-0">
        <p class="title ps-0">تعداد ویزاهای موفق</p>
        <p class="description">اخذ ویزای توریستی، تحصیلی، درمانی، ورزشی برای کشور های امریکا، کانادا، حوزه شنگن،
            انگلستان و استرالیا</p>
        <div class="row items px-0 mx-auto">

            <div class="col-md-2 item">
                <label class="number" data-bs-target="15">0
                    <img src="{{asset('images/icon-plus.png')}}" class="d-inline"/>
                </label>
                <label class="title">سالهای فعالیت </label>
            </div>

            <div class="col-md-2 item">
                <label class="number" data-bs-target="16000">
                    0
                    <img src="{{asset('images/icon-plus.png')}}" class="d-inline"/>
                </label>
                <label class="title">تعداد کل متقاضیان ویزا</label>
            </div>

            <div class="col-md-2 item">
                <label class="number" data-bs-target="10000">0
                    <img src="{{asset('images/icon-plus.png')}}" class="d-inline"/>
                </label>
                <label class="title">تعداد کل ویزاهای موفق</label>
            </div>

            <div class="col-md-2 item me-md-0">
                <label class="number" data-bs-target="20">0
                    <img src="{{asset('images/icon-plus.png')}}" class="d-inline"/>
                </label>
                <label class="title">کشورهای حوزه فعالیت</label>
            </div>
        </div>
    </div>
</div>


<script src="{{ asset('js/web/successfulVisaCounter.js') }}"></script>


