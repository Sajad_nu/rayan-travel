@include('web.components.header')

<div class="aboutUsBody">
    @include('web.about-us.about-us')

    <div class="d-none d-md-flex">
        @include('web.about-us.why-ryan-visa-in-about-us')
    </div>

    @include('web.about-us.experiences-records')

    @include('web.about-us.successful-visas-in-about-us')

    @include('web.about-us.comments-travelers-in-about-us')

{{--    @include('web.about-us.news-letter-in-about-us')--}}

    @include('web.components.footer')
</div>

