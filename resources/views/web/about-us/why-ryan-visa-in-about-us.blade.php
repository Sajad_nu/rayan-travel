<div class="whyRyanVisaInAboutUs container row flex-md-nowrap mx-auto">

    <div class="right col-md-7">
        <p class="title">چرا رایان ویزا ؟!</p>
        <p class="reason">بیش از چند دهه تجربه و سابقه حرفه‌ای !</p>

        <div class="row">
            <div class="col-6 item">
                <label class="icon">
                    <img src="{{asset('images/whyRyanVisaIcons/security.png')}}" class="img-fluid" alt="امنیت و صداقت">
                </label>
                <p class="title mb-0">
                    امنیت و صداقت
                </p>
            </div>
            <div class="col-6 item">
                <label class="icon">
                    <img src="{{asset('images/whyRyanVisaIcons/successfulRecord1.png')}}" class="img-fluid" alt="سابقه موفق عملیاتی">
                </label>
                <p class="title mb-0">
                    سابقه موفق عملیاتی
                </p>
            </div>

        </div>

        <div class="row">

            <div class="col-6 item">
                <label class="icon">
                    <img src="{{asset('images/whyRyanVisaIcons/guarantee.png')}}" class="img-fluid" alt="رضایت مشتریان">
                </label>
                <p class="title mb-0">
                    رضایت مشتریان
                </p>
            </div>
            <div class="col-6 item">
                <label class="icon">
                    <img src="{{asset('images/whyRyanVisaIcons/team.png')}}" class="img-fluid" alt="کادر مجرب و حرفه ای">
                </label>
                <p class="title mb-0">
                    کادر مجرب و حرفه ای
                </p>
            </div>


        </div>

        <div class="row">
            <div class="col-6 item">
                <label class="icon">
                    <img src="{{asset('images/whyRyanVisaIcons/cost.png')}}" class="img-fluid" alt="  هزینه متناسب با خدمات">
                </label>
                <p class="title mb-0">
                    هزینه متناسب با خدمات
                </p>
            </div>
            <div class="col-6 item">
                <label class="icon">
                    <img src="{{asset('images/whyRyanVisaIcons/suport.png')}}" class="img-fluid" alt="شعب پشتیبان ما در کشورهای هدف">
                </label>
                <p class="title mb-0">
                    شعب پشتیبان ما در کشورهای هدف
                </p>
            </div>
        </div>
    </div>
    <div class="left col-md-5 me-md-0">
        <img src="{{asset('images/team.jpg')}}" class="img-fluid" alt="تصویر تیم مشاوران رایان ویزا"/>
    </div>
</div>



