<div class="commentsTravelersInAboutUs container row mx-auto">
    <p class="title">از زبان مسافران</p>
    <p class="description">مسافرانی که به ما اعتماد کرده اند ...</p>

    <div class="swiper mySwiper">

        <div class="swiper-wrapper mb-5 ">
            <div class="swiper-slide">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-4 col-md-2"><img src="{{asset('images/commentsTravelers/men.png')}}"
                                                       class="img-fluid"
                                                       alt="نطر کاربران درباره ویزای استرالیا"></div>
                            <div class="col-8 col-md-10">
                                <p class="card-title fw-bold mb-0"> علی رضوانی</p>
                                <p class="country mb-0">ویزای استرالیا</p>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-md-11">
                                <p class="card-text"> ویزا را از رایان ویزا گرفتم و بسیار راضی بودم. فرآیند ساده
                                    و روانی داشت و اطلاعات لازم برای انجام امور به شفافیت بالایی ارائه می‌شد. به
                                    شدت توصیه می‌کنم.
                                </p>
                            </div>

                            <div class="col-md-1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="20"
                                     viewBox="0 0 25 20"
                                     fill="none">
                                    <g clip-path="url(#clip0_775_5340)">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M24.5613 3.04555C20.9824 4.73754 19.1929 6.72558 19.1929 9.00976C20.7184 9.17896 21.9798 9.77819 22.9772 10.8075C23.9746 11.8368 24.4732 13.0282 24.4732 14.3818C24.4732 15.82 23.9892 17.0325 23.0212 18.0195C22.0531 19.0065 20.8357 19.5 19.3689 19.5C17.7262 19.5 16.3034 18.8585 15.1007 17.5754C13.8979 16.2923 13.2966 14.7343 13.2966 12.9013C13.2966 7.40236 16.4941 3.10197 22.8892 0L24.5613 3.04555ZM11.9312 3.04555C8.32294 4.73754 6.51885 6.72558 6.51885 9.00976C8.07362 9.17896 9.34968 9.77819 10.3471 10.8075C11.3445 11.8368 11.8432 13.0282 11.8432 14.3818C11.8432 15.82 11.3518 17.0325 10.3691 18.0195C9.38635 19.0065 8.16162 19.5 6.69486 19.5C5.05209 19.5 3.63669 18.8585 2.44861 17.5754C1.26053 16.2923 0.666504 14.7343 0.666504 12.9013C0.666504 7.40236 3.84933 3.10197 10.2151 0L11.9312 3.04555Z"
                                              fill="#00A594"/>
                                    </g>
                                    <defs>
                                        <clipPath id="clip0_775_5340">
                                            <rect width="24" height="19.5" fill="white"
                                                  transform="translate(0.666504)"/>
                                        </clipPath>
                                    </defs>
                                </svg>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-4 col-md-2"><img src="{{asset('images/commentsTravelers/women.png')}}"
                                                       class="img-fluid"
                                                       alt="نطر کاربران درباره ویزای انگلستان"></div>
                            <div class="col-8 col-md-10">
                                <p class="card-title fw-bold mb-0">خدادوست</p>
                                <p class="country mb-0">ویزای انگلستان</p>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-md-11">
                                <p class="card-text">

                                    رایان ویزا به عنوان یکی از بهترین ارائه‌دهندگان خدمات ویزا شناخته می‌شود. از
                                    لحظه‌ی تماس اول تا دریافت ویزا، همه‌چیز بسیار سریع و قابل اعتماد بود. این
                                    شرکت توانست نگرانی‌هایم را کاهش دهد و تجربه‌ی من را بهتر کند.</p>
                            </div>

                            <div class="col-md-1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="20"
                                     viewBox="0 0 25 20"
                                     fill="none">
                                    <g clip-path="url(#clip0_775_5340)">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M24.5613 3.04555C20.9824 4.73754 19.1929 6.72558 19.1929 9.00976C20.7184 9.17896 21.9798 9.77819 22.9772 10.8075C23.9746 11.8368 24.4732 13.0282 24.4732 14.3818C24.4732 15.82 23.9892 17.0325 23.0212 18.0195C22.0531 19.0065 20.8357 19.5 19.3689 19.5C17.7262 19.5 16.3034 18.8585 15.1007 17.5754C13.8979 16.2923 13.2966 14.7343 13.2966 12.9013C13.2966 7.40236 16.4941 3.10197 22.8892 0L24.5613 3.04555ZM11.9312 3.04555C8.32294 4.73754 6.51885 6.72558 6.51885 9.00976C8.07362 9.17896 9.34968 9.77819 10.3471 10.8075C11.3445 11.8368 11.8432 13.0282 11.8432 14.3818C11.8432 15.82 11.3518 17.0325 10.3691 18.0195C9.38635 19.0065 8.16162 19.5 6.69486 19.5C5.05209 19.5 3.63669 18.8585 2.44861 17.5754C1.26053 16.2923 0.666504 14.7343 0.666504 12.9013C0.666504 7.40236 3.84933 3.10197 10.2151 0L11.9312 3.04555Z"
                                              fill="#00A594"/>
                                    </g>
                                    <defs>
                                        <clipPath id="clip0_775_5340">
                                            <rect width="24" height="19.5" fill="white"
                                                  transform="translate(0.666504)"/>
                                        </clipPath>
                                    </defs>
                                </svg>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-4 col-md-2"><img src="{{asset('images/commentsTravelers/women.png')}}"
                                                       class="img-fluid"
                                                       alt="نظر کاربران درباره ویزای شینگن"></div>
                            <div class="col-8 col-md-10">
                                <p class="card-title fw-bold mb-0">مینا وطن پور</p>
                                <p class="country mb-0">ویزای کانادا</p>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-md-11">
                                <p class="card-text">

                                    تجربه‌ی من با رایان ویزا بسیار مثبت بود. خدمات مشتریان عالی، پرسنل مودب و
                                    حرفه‌ای و امکانات راحتی که این شرکت فراهم می‌کند، باعث شد که هر بار که به
                                    ویزا
                                    نیاز داشتم، انتخاب اصلی من باشد.
                                </p>
                            </div>

                            <div class="col-md-1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="20"
                                     viewBox="0 0 25 20"
                                     fill="none">
                                    <g clip-path="url(#clip0_775_5340)">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M24.5613 3.04555C20.9824 4.73754 19.1929 6.72558 19.1929 9.00976C20.7184 9.17896 21.9798 9.77819 22.9772 10.8075C23.9746 11.8368 24.4732 13.0282 24.4732 14.3818C24.4732 15.82 23.9892 17.0325 23.0212 18.0195C22.0531 19.0065 20.8357 19.5 19.3689 19.5C17.7262 19.5 16.3034 18.8585 15.1007 17.5754C13.8979 16.2923 13.2966 14.7343 13.2966 12.9013C13.2966 7.40236 16.4941 3.10197 22.8892 0L24.5613 3.04555ZM11.9312 3.04555C8.32294 4.73754 6.51885 6.72558 6.51885 9.00976C8.07362 9.17896 9.34968 9.77819 10.3471 10.8075C11.3445 11.8368 11.8432 13.0282 11.8432 14.3818C11.8432 15.82 11.3518 17.0325 10.3691 18.0195C9.38635 19.0065 8.16162 19.5 6.69486 19.5C5.05209 19.5 3.63669 18.8585 2.44861 17.5754C1.26053 16.2923 0.666504 14.7343 0.666504 12.9013C0.666504 7.40236 3.84933 3.10197 10.2151 0L11.9312 3.04555Z"
                                              fill="#00A594"/>
                                    </g>
                                    <defs>
                                        <clipPath id="clip0_775_5340">
                                            <rect width="24" height="19.5" fill="white"
                                                  transform="translate(0.666504)"/>
                                        </clipPath>
                                    </defs>
                                </svg>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-4 col-md-2"><img src="{{asset('images/commentsTravelers/women.png')}}"
                                                       class="img-fluid"
                                                       alt="نظر کاربران درباره ویزای شینگن"></div>
                            <div class="col-8 col-md-10">
                                <p class="card-title fw-bold mb-0"> سلیمانی</p>
                                <p class="country mb-0">ویزای شینگن</p>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-md-11">
                                <p class="card-text">
                                    جربه من با رایان ویزا بسیار دلنشین بود. از لحظه‌ی مشاوره تا دریافت ویزا،
                                    تمامی مراحل با دقت انجام شد و همواره احساس کردم که در کنار یک تیم حرفه‌ای
                                    هستم.
                                </p>
                            </div>

                            <div class="col-md-1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="20"
                                     viewBox="0 0 25 20"
                                     fill="none">
                                    <g clip-path="url(#clip0_775_5340)">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M24.5613 3.04555C20.9824 4.73754 19.1929 6.72558 19.1929 9.00976C20.7184 9.17896 21.9798 9.77819 22.9772 10.8075C23.9746 11.8368 24.4732 13.0282 24.4732 14.3818C24.4732 15.82 23.9892 17.0325 23.0212 18.0195C22.0531 19.0065 20.8357 19.5 19.3689 19.5C17.7262 19.5 16.3034 18.8585 15.1007 17.5754C13.8979 16.2923 13.2966 14.7343 13.2966 12.9013C13.2966 7.40236 16.4941 3.10197 22.8892 0L24.5613 3.04555ZM11.9312 3.04555C8.32294 4.73754 6.51885 6.72558 6.51885 9.00976C8.07362 9.17896 9.34968 9.77819 10.3471 10.8075C11.3445 11.8368 11.8432 13.0282 11.8432 14.3818C11.8432 15.82 11.3518 17.0325 10.3691 18.0195C9.38635 19.0065 8.16162 19.5 6.69486 19.5C5.05209 19.5 3.63669 18.8585 2.44861 17.5754C1.26053 16.2923 0.666504 14.7343 0.666504 12.9013C0.666504 7.40236 3.84933 3.10197 10.2151 0L11.9312 3.04555Z"
                                              fill="#00A594"/>
                                    </g>
                                    <defs>
                                        <clipPath id="clip0_775_5340">
                                            <rect width="24" height="19.5" fill="white"
                                                  transform="translate(0.666504)"/>
                                        </clipPath>
                                    </defs>
                                </svg>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-4 col-md-2"><img src="{{asset('images/commentsTravelers/women.png')}}"
                                                       class="img-fluid"
                                                       alt="نظر کاربران درباره ویزای شینگن"></div>
                            <div class="col-8 col-md-10">
                                <p class="card-title fw-bold mb-0">میرمحمدی</p>
                                <p class="country mb-0">ویزای شینگن</p>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-md-11">
                                <p class="card-text">
                                    جربه‌ام با رایان ویزا بسیار خوب بود. پرسنل حرفه‌ای و صبوری داشتند و به
                                    سوالات من با دقت پاسخ دادند. سرعت در پردازش ویزا نیز به واقعیت برخوردار بود
                                    و من به راحتی و بدون مشکلاتی ویزای خود را دریافت کردم.

                                </p>
                            </div>

                            <div class="col-md-1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="20"
                                     viewBox="0 0 25 20"
                                     fill="none">
                                    <g clip-path="url(#clip0_775_5340)">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M24.5613 3.04555C20.9824 4.73754 19.1929 6.72558 19.1929 9.00976C20.7184 9.17896 21.9798 9.77819 22.9772 10.8075C23.9746 11.8368 24.4732 13.0282 24.4732 14.3818C24.4732 15.82 23.9892 17.0325 23.0212 18.0195C22.0531 19.0065 20.8357 19.5 19.3689 19.5C17.7262 19.5 16.3034 18.8585 15.1007 17.5754C13.8979 16.2923 13.2966 14.7343 13.2966 12.9013C13.2966 7.40236 16.4941 3.10197 22.8892 0L24.5613 3.04555ZM11.9312 3.04555C8.32294 4.73754 6.51885 6.72558 6.51885 9.00976C8.07362 9.17896 9.34968 9.77819 10.3471 10.8075C11.3445 11.8368 11.8432 13.0282 11.8432 14.3818C11.8432 15.82 11.3518 17.0325 10.3691 18.0195C9.38635 19.0065 8.16162 19.5 6.69486 19.5C5.05209 19.5 3.63669 18.8585 2.44861 17.5754C1.26053 16.2923 0.666504 14.7343 0.666504 12.9013C0.666504 7.40236 3.84933 3.10197 10.2151 0L11.9312 3.04555Z"
                                              fill="#00A594"/>
                                    </g>
                                    <defs>
                                        <clipPath id="clip0_775_5340">
                                            <rect width="24" height="19.5" fill="white"
                                                  transform="translate(0.666504)"/>
                                        </clipPath>
                                    </defs>
                                </svg>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-4 col-md-2"><img src="{{asset('images/commentsTravelers/women.png')}}"
                                                       class="img-fluid"
                                                       alt="نطر کاربران درباره ویزای انگلستان"></div>
                            <div class="col-8 col-md-10">
                                <p class="card-title fw-bold mb-0">خدادوست</p>
                                <p class="country mb-0">ویزای انگلستان</p>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-md-11">
                                <p class="card-text">

                                    رایان ویزا به عنوان یکی از بهترین ارائه‌دهندگان خدمات ویزا شناخته می‌شود. از
                                    لحظه‌ی تماس اول تا دریافت ویزا، همه‌چیز بسیار سریع و قابل اعتماد بود. این
                                    شرکت توانست نگرانی‌هایم را کاهش دهد و تجربه‌ی من را بهتر کند.</p>
                            </div>

                            <div class="col-md-1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="20"
                                     viewBox="0 0 25 20"
                                     fill="none">
                                    <g clip-path="url(#clip0_775_5340)">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M24.5613 3.04555C20.9824 4.73754 19.1929 6.72558 19.1929 9.00976C20.7184 9.17896 21.9798 9.77819 22.9772 10.8075C23.9746 11.8368 24.4732 13.0282 24.4732 14.3818C24.4732 15.82 23.9892 17.0325 23.0212 18.0195C22.0531 19.0065 20.8357 19.5 19.3689 19.5C17.7262 19.5 16.3034 18.8585 15.1007 17.5754C13.8979 16.2923 13.2966 14.7343 13.2966 12.9013C13.2966 7.40236 16.4941 3.10197 22.8892 0L24.5613 3.04555ZM11.9312 3.04555C8.32294 4.73754 6.51885 6.72558 6.51885 9.00976C8.07362 9.17896 9.34968 9.77819 10.3471 10.8075C11.3445 11.8368 11.8432 13.0282 11.8432 14.3818C11.8432 15.82 11.3518 17.0325 10.3691 18.0195C9.38635 19.0065 8.16162 19.5 6.69486 19.5C5.05209 19.5 3.63669 18.8585 2.44861 17.5754C1.26053 16.2923 0.666504 14.7343 0.666504 12.9013C0.666504 7.40236 3.84933 3.10197 10.2151 0L11.9312 3.04555Z"
                                              fill="#00A594"/>
                                    </g>
                                    <defs>
                                        <clipPath id="clip0_775_5340">
                                            <rect width="24" height="19.5" fill="white"
                                                  transform="translate(0.666504)"/>
                                        </clipPath>
                                    </defs>
                                </svg>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>

        <div class="swiper-pagination"></div>


    </div>


</div>


<script src="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.js"></script>
<script>
    let swiper = new Swiper(".mySwiper", {
        loop: true,
        centeredSlides: true,
        spaceBetween: 30,
        autoplay: {
            delay: 2500,
            disableOnInteraction: false,
        },
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
        breakpoints: {
            768: {
                slidesPerView: 1,
                effect: "fade",

            },
            769: {
                slidesPerView: 3,

            }}
        ,
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
            },
        });
</script>
