<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="rtl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ryan-visa</title>

    @vite('resources/js/web/vue/app.js')
</head>

<body>
<div id="app">
    @verbatim
        <received-message></received-message>
    @endverbatim
</div>
</body>
</html>
