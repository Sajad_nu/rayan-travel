<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="rtl">
<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta name="x-apple-disable-message-reformatting">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="telephone=no" name="format-detection">
    <title>New Template</title>

    <link href="https://cdn.jsdelivr.net/gh/rastikerdar/vazirmatn@v33.003/Vazirmatn-font-face.css" rel="stylesheet"
          type="text/css"/>
</head>


<body style="width:100%; height:100%; padding:0; margin:0; font-family: Vazirmatn, system-ui;">

<table border="1" align="center">

    <tr style="background-color:#E0E1FD; padding: 15px;text-align: center;font-weight: bold;font-size:15pt!important;color: #0d0d0d;height: 65px;line-height:3">
        <td>سوال</td>
        <td>پاسخ</td>
        <td> توضیحات</td>
    </tr>

    @foreach($paramsValues as $paramsValue)

        <tr style="font-size:12pt!important;font-weight: bold;text-align:center;vertical-align:middle ;color:#44465F;direction: rtl">

            <td style="padding:15px;width:500px">
                {{$paramsValue['title']}}

            </td>

            <td style="padding:15px;width: 300px">
                @foreach($paramsValue['values'] as $value)

                    {{ $value }}
                    <br>
                @endforeach
            </td>


            @if(isset($paramsValue['items']))

                <td style="padding:15px;width: 400px;">
                    @foreach($paramsValue['items'] as $paramsItem)

                        {{ $paramsItem['itemKey'] . ' : '.  $paramsItem['itemValue']}}
                        <br>

                    @endforeach
                </td>
            @else
                <td style="padding:15px;width: 400px"></td>
            @endif

        </tr>
    @endforeach
</table>

<div
    style="margin: 50px auto;text-align: center">

    <button style="background : #bb2d3b ; padding : 10px 25px; margin : 10px ; border-radius : 5px">
        <a href="{{url('assessment/record-feedback?hash_id='. $hashId . '&rank=poor')}}"
           style="cursor : pointer; text-decoration: none;font-family: Vazirmatn, system-ui; font-size :12pt; font-weight : bold;color :#fff;">ضعیف</a>
    </button>
    <button style="background : #ffca2c ; padding : 10px 25px; margin : 10px ; border-radius : 5px">
        <a href="{{url('/assessment/record-feedback?hash_id='. $hashId . '&rank=average')}}"
           style="cursor : pointer;text-decoration: none;font-family: Vazirmatn, system-ui; font-size :12pt; font-weight : bold;color :#fff;">متوسط</a>
    </button>
    <button style="background : #0d6efd ; padding : 10px 25px; margin : 10px ; border-radius : 5px">
        <a href="{{url('/assessment/record-feedback?hash_id='. $hashId . '&rank=good')}}"
           style="cursor : pointer;text-decoration: none;font-family: Vazirmatn, system-ui; font-size :12pt; font-weight : bold;color :#fff;">خوب</a>
    </button>
    <button style="background : #198754 ; padding : 10px 25px; margin : 10px ; border-radius : 5px;">
        <a href="{{url('/assessment/record-feedback?hash_id='. $hashId . '&rank=excellent')}}"
           style="cursor : pointer;text-decoration: none;font-family: Vazirmatn, system-ui; font-size :12pt; font-weight : bold;color :#fff;">خیلی
            خوب</a>
    </button>

</div>


</body>
</html>
