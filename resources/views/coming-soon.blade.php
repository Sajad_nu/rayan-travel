<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Coming Soon - Rayan Visa</title>
    <link rel="icon" type="image/x-icon" href="{{ url('/coming-soon-styles/assets/favicon.ico')}}" />
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v6.3.0/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link rel="preconnect" href="https://fonts.gstatic.com" />
    <link href="https://fonts.googleapis.com/css2?family=Tinos:ital,wght@0,400;0,700;1,400;1,700&amp;display=swap" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=DM+Sans:ital,wght@0,400;0,500;0,700;1,400;1,500;1,700&amp;display=swap" rel="stylesheet" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="{{ url('/coming-soon-styles/css/styles.css') }}" rel="stylesheet" />
</head>
<body>
<!-- Background Video-->
<video class="bg-video" playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop"><source src="{{ url('/coming-soon-styles/assets/mp4/bg.mp4')}}" type="video/mp4" /></video>
<!-- Masthead-->
<div class="masthead">
    <div class="masthead-content text-white text-center">
        <div class="container-fluid px-4 px-lg-0 text-secondary">
            <img src="{{ url('/coming-soon-styles/assets/img/logo-prev_ui.png')}}" class="img-fluid">
            <h1 class="fst-italic lh-1 mb-4">Our Website is Coming Soon ...</h1>
            <h3  class="fst-italic mb-3">Click to see our
                <a href="{{url('blog')}}">Weblog</a>
            </h3>
        </div>
    </div>
</div>
<!-- Social Icons-->
<!-- For more icon options, visit https://fontawesome.com/icons?d=gallery&p=2&s=brands-->
<div class="social-icons">
    <div class="d-flex flex-row flex-lg-column justify-content-center align-items-center h-100 mt-3 mt-lg-0">
        <a class="btn btn-dark m-3" href="https://t.me/ryansiyahat"><i class="fab fa-telegram"></i></a>
        <a class="btn btn-dark m-3" href="https://instagram.com/ryan.siyahat.official?igshid=YzcxN2Q2NzY0OA=="><i class="fab fa-instagram"></i></a>
    </div>
</div>
<!-- Bootstrap core JS-->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
<!-- Core theme JS-->
<script src="{{ url('/js/bootstrap.js') }}"></script>
<!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
<!-- * *                               SB Forms JS                               * *-->
<!-- * * Activate your form at https://startbootstrap.com/solution/contact-forms * *-->
<!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
<script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
</body>
</html>
