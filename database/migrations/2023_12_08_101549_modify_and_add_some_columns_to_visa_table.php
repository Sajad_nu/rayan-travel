<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('visa', function (Blueprint $table) {
            $table->unsignedBigInteger('category_id')->after('location_id');

            $table->foreign('location_id')->references('id')->on('locations');
            $table->foreign('category_id')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('visa', function (Blueprint $table) {
            $table->dropForeign('category_id');
            $table->dropColumn('category_id');
            $table->dropColumn('location_id');
        });
    }
};
