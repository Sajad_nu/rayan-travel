<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('answers_assessment', function (Blueprint $table) {

            $table->unsignedBigInteger('user_assessment_id')->after('id')->nullable();

            $table->foreign('user_assessment_id')->references('id')->on('users_assessment');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('answers_assessment', function (Blueprint $table) {

            $table->dropColumn('user_assessment_id');

            $table->dropForeign('user_assessment_id');
        });
    }
};
