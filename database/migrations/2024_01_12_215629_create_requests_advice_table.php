<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('requests_advice', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('advice_time_id')->nullable();
            $table->string('phone_number');
            $table->string('full_name')->nullable();
            $table->enum('type', ['in_person', 'online', 'phone']);
            $table->string('tracking_code', 128);
            $table->timestamp('send_to_expert_at')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();

            $table->foreign('advice_time_id')->references('id')->on('advice_times');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('requests_advice');
    }
};
