<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('files', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('category_id')->nullable();
            $table->unsignedBigInteger('visa_id')->nullable();
            $table->string('name', 128);
            $table->string('description', 64)->nullable();
            $table->string('hash', 32)->nullable();
            $table->string('type', 16)->nullable();
            $table->string('path', 256)->nullable();
            $table->integer('priority')->default(0);
            $table->boolean('published')->default(false);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
            $table->timestamp('deleted_at')->nullable();


            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('visa_id')->references('id')->on('visa');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('files');
    }
};
