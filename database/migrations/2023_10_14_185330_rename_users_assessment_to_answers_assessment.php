<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('answers_assessment', function (Blueprint $table) {

            Schema::table('users_assessment', function (Blueprint $table) {
                $table->dropForeign(['question_id']);
            });

            Schema::rename('users_assessment', 'answers_assessment');

            Schema::table('answers_assessment', function (Blueprint $table) {
                $table->foreign('question_id')->references('id')->on('assessment_questions');
            });
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('answers_assessment', function (Blueprint $table) {
            //
        });
    }
};
