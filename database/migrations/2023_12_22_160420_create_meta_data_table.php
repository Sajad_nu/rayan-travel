<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('meta_data', function (Blueprint $table) {
            $table->id();
            $table->string('uri');
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->string('locale')->nullable();
            $table->string('canonical_url')->nullable();
            $table->string('index')->nullable();
            $table->string('robots')->nullable();
            $table->string('type')->nullable();
            $table->string('author')->nullable();
            $table->text('keywords')->nullable();
            $table->string('image')->nullable();
            $table->boolean('disabled')->default(0);
            $table->timestamp('published_time')->nullable();
            $table->timestamp('modified_time')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('meta_data');
    }
};
