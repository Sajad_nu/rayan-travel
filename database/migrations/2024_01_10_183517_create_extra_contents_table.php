<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('extra_contents', function (Blueprint $table) {
            $table->id();
            $table->text('uri');
            $table->enum('type', ['faq', 'content']);
            $table->string('title', 256);
            $table->text('description');
            $table->unsignedSmallInteger('priority')->default(0);
            $table->boolean('published')->default(false);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('extra_contents');
    }
};
