<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users_assessment', function (Blueprint $table) {
            $table->id();
            $table->string('hash_id');
            $table->unsignedBigInteger('tracking_code');
            $table->unsignedBigInteger('phone_number')->nullable();
            $table->string('send_result_to')->nullable();
            $table->enum('rank', ['poor', 'average', 'good', 'excellent'])->nullable();
            $table->timestamp('review_at')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users_assessment');
    }
};
