<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('visa', function (Blueprint $table) {
            $table->text('documents')->after('description')->nullable();
            $table->text('procedures')->after('fee')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('visa', function (Blueprint $table) {
            $table->dropColumn('documents');
            $table->dropColumn('procedures');
        });
    }
};
