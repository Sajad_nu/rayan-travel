<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->enum('type', [
                'pilgrimage', 'diplomatic', 'tourist', 'educational',
                'business', 'transit', 'media', 'investment', 'medical',
                'marriage', 'on_arrival', 'immigrant', 'sport'])->nullable();

            $table->string('uri')->nullable();
            $table->text('description')->nullable();
            $table->boolean('disabled')->default(0);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('categoty');
    }
};
