<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('users_assessment', function (Blueprint $table) {
            $table->string('tracking_code', 128)->change();
            DB::statement("ALTER TABLE users_assessment MODIFY phone_number Varchar(60)");

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
