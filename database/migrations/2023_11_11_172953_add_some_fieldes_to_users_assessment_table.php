<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('users_assessment', function (Blueprint $table) {
            $table->timestamp('send_at')->nullable()->after('review_at')->comment('ارسال شده به کارشناس در ');
            $table->timestamp('send_result_at')->nullable()->after('send_at')->comment('ارسال نتیجه ارزیابی به کاربر در ');
            $table->timestamp('send_confirm_at')->nullable()->after('send_result_at')->comment('تأییدیه ارسال به کارشناس در ');
            $table->renameColumn('review_at', 'rank_at');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('users_assessment', function (Blueprint $table) {
            $table->dropColumn('send_at');
            $table->dropColumn('send_result_at');
            $table->dropColumn('send_confirm_at');
        });
    }
};
